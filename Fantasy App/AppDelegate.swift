    //
//  AppDelegate.swift
//  Fantasy App
//
//  Created by Chad on 4/15/18.
//  Copyright © 2018 Chad. All rights reserved.
//

import UIKit
import AWSMobileClient
import AWSCore
import AWSDynamoDB
import AWSCognitoIdentityProvider

let userPoolID = "FantasyAppUserPool"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    class func defaultUserPool() -> AWSCognitoIdentityUserPool {
        return AWSCognitoIdentityUserPool(forKey: userPoolID)
    }
    
    var window: UIWindow?
    
    var loginViewController: LoginController?
    var resetPasswordViewController: ResetPasswordVC?
    var forgotPasswordViewController: ForgotPasswordVC?
    var navigationController: UINavigationController?
    var cognitoConfig:CognitoConfig?
    let loginLayout = UICollectionViewFlowLayout()

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        //Set up layout and window for UI
        setupLayoutandWindow()
        
        // set up logging for AWS and Cognito
        AWSDDLog.sharedInstance.logLevel = .verbose
        AWSDDLog.add(AWSDDTTYLogger.sharedInstance)
        
        // set up Cognito config
        self.cognitoConfig = CognitoConfig()
        
        // set up Cognito
        setupCognitoUserPool()
        
        // Create AWSMobileClient to connect with AWS
        AWSDDLog.add(AWSDDTTYLogger.sharedInstance)
        AWSDDLog.sharedInstance.logLevel = .info
        
        // Initialize the Amazon Cognito credentials provider
        
        let credentialsProvider = AWSCognitoCredentialsProvider(regionType:.USEast2,
                                                                identityPoolId:"us-east-2:0ab4ce73-ac10-4518-87ff-d1e784b37c16")
        
        let configuration = AWSServiceConfiguration(region:.USEast2, credentialsProvider:credentialsProvider)
        
        AWSServiceManager.default().defaultServiceConfiguration = configuration
                
        return AWSMobileClient.sharedInstance().interceptApplication(
            application,
            didFinishLaunchingWithOptions: launchOptions)
        
    }
    
    func setupLayoutandWindow() {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.makeKeyAndVisible()
        self.window?.backgroundColor = .white
        
        let layout = UICollectionViewFlowLayout()
        let initialViewController = LoginController(collectionViewLayout: layout)
        let navigationController = UINavigationController(rootViewController: initialViewController)
        self.window?.rootViewController = MainNavigationController()
        //UIApplication.shared.statusBarStyle = .lightContent
        UINavigationBar.appearance().tintColor = .white
    }
    
    //Setup Cognito User Pool
    func setupCognitoUserPool() {
        let clientId:String = self.cognitoConfig!.getClientId()
        let poolId:String = self.cognitoConfig!.getPoolId()
        let clientSecret:String = self.cognitoConfig!.getClientSecret()
        let region:AWSRegionType = self.cognitoConfig!.getRegion()
        
        let serviceConfiguration:AWSServiceConfiguration = AWSServiceConfiguration(region: region, credentialsProvider: nil)
        let cognitoConfiguration:AWSCognitoIdentityUserPoolConfiguration = AWSCognitoIdentityUserPoolConfiguration(clientId: clientId, clientSecret: clientSecret, poolId: poolId)
        AWSCognitoIdentityUserPool.register(with: serviceConfiguration, userPoolConfiguration: cognitoConfiguration, forKey: userPoolID)
        let pool:AWSCognitoIdentityUserPool = AppDelegate.defaultUserPool()
        pool.delegate = self
//        pool.currentUser()?.getSession()
        print(pool.currentUser()?.isSignedIn)
    }
    
    // Add a AWSMobileClient call in application:open url
    func application(_ application: UIApplication, open url: URL,
                     sourceApplication: String?, annotation: Any) -> Bool {
        
        return AWSMobileClient.sharedInstance().interceptApplication(
            application, open: url,
            sourceApplication: sourceApplication,
            annotation: annotation)
        
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}

extension AppDelegate: AWSCognitoIdentityInteractiveAuthenticationDelegate {

    // This method is called when we need to log into the application.
    // It will grab the view controller from the storyboard and present it.
    func startPasswordAuthentication() -> AWSCognitoIdentityPasswordAuthentication {
        if (self.navigationController == nil) {
            let layout = UICollectionViewFlowLayout()
            let initialViewController = LoginController(collectionViewLayout: layout)
            let navigationController = UINavigationController(rootViewController: initialViewController)
            self.navigationController = navigationController as? UINavigationController
        }
        
        if (self.loginViewController == nil) {
            self.loginViewController = self.navigationController?.viewControllers[0] as? LoginController
        }
        
        DispatchQueue.main.async {
            self.navigationController!.popToRootViewController(animated: true)
            if (!self.navigationController!.isViewLoaded
                || self.navigationController!.view.window == nil) {
                self.window?.rootViewController?.present(self.navigationController!,
                                                         animated: true,
                                                         completion: nil)
            }
            
        }
        return self.loginViewController!
    }
//
//    // This method is called when we need to reset a password.
//    // It will grab the view controller from the storyboard and present it.
//    func startNewPasswordRequired() -> AWSCognitoIdentityNewPasswordRequired {
//        if (self.resetPasswordViewController == nil) {
//            self.resetPasswordViewController = self.storyboard?.instantiateViewController(withIdentifier: "ResetPasswordController") as? ResetPasswordViewController
//        }
//
//        DispatchQueue.main.async {
//            if(self.resetPasswordViewController!.isViewLoaded || self.resetPasswordViewController!.view.window == nil) {
//                self.navigationController?.present(self.resetPasswordViewController!, animated: true, completion: nil)
//            }
//        }
//
//        return self.resetPasswordViewController!
//    }

}


