//
//  MainNavigationController.swift
//  Fantasy App
//
//  Created by Chad on 8/14/18.
//  Copyright © 2018 Chad. All rights reserved.
//

import UIKit

class MainNavigationController: UINavigationController {
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        self.isNavigationBarHidden = true
    
        if isLoggedIn() {
            let homeController = CustomTabBarController()
            viewControllers = [homeController]
        } else {
            perform(#selector(showLoginController), with: nil, afterDelay: 0.01)
        }
    }
    
    private func isLoggedIn() -> Bool {
        return UserDefaults.standard.bool(forKey: "isLoggedIn")
    }
    
    @objc func showLoginController() {
        AppDelegate.defaultUserPool().currentUser()?.getSession()
//        let layout = UICollectionViewFlowLayout()
//        let loginController = LoginController(collectionViewLayout: layout)
//        present(loginController, animated: true, completion: {
//            //to be updated
//        })
    }
}
