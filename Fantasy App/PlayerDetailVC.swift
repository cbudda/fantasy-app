//
//  PlayerDetailVC.swift
//  
//
//  Created by Chad on 5/1/18.
//

import Foundation
import UIKit
import SwiftyJSON
import AWSLambda

class PlayerDetailViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate {
    
    
    @IBOutlet weak var playerName: UILabel!
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var school: UILabel!
    @IBOutlet weak var gp: UILabel!
    @IBOutlet weak var ppg: UILabel!
    @IBOutlet weak var apg: UILabel!
    @IBOutlet weak var rpg: UILabel!
    @IBOutlet weak var ownerDraft: UILabel!
    @IBOutlet weak var topView1: UIView!
    @IBOutlet weak var topView2: UIView!

    @IBOutlet weak var detailPlayerTable: UITableView!
    
    var teamHexColor = String()
    
    lazy var navbarHeight = CGFloat((navigationController?.navigationBar.frame.size.height)!)
    let statusbarHeight = CGFloat(UIApplication.shared.statusBarFrame.size.height)
    var playerDetailRefreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = ""
        
        NotificationCenter.default.addObserver(self, selector: #selector(loadList), name: NSNotification.Name(rawValue: "load"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(endRefresh), name: NSNotification.Name(rawValue: "endRefresh"), object: nil)
        
        detailPlayerTable.delegate = self
        detailPlayerTable.dataSource = self
        
        detailPlayerTable.allowsSelection = false
        
        detailPlayerTable.tableFooterView = UIView()
        
        self.navigationController?.delegate = self
        addGesture()
        
        playerDetailRefreshControl.addTarget(self, action: #selector(requestData), for: .valueChanged)
        detailPlayerTable.refreshControl = playerDetailRefreshControl
        
    }
    
    @objc func endRefresh(){
        //end refresh
        self.playerDetailRefreshControl.endRefreshing()
    }
    
    @objc
    func requestData() {
        TournStats.detailedViewQueryStats()
    }
    
    @objc func loadList(){
        //load data here
        UIView.transition(with: detailPlayerTable, duration: 0.5, options: .transitionCrossDissolve, animations: {self.detailPlayerTable.reloadData()}, completion: nil)
        self.playerDetailRefreshControl.endRefreshing()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        TournStats.detailedViewQueryStats()
        let teamID = SendtoPlayerDetailView.schoolID
        let hexColorIndex = BackgroundColors.backgroundColors.index(where: {($0.teamID == teamID)}) as! Int
        //print(hexColorIndex)
        let hexColor = BackgroundColors.backgroundColors.map {$0.color}[hexColorIndex]
        teamHexColor = hexColor
        
        let url = URL(string: SendtoPlayerDetailView.schoolLogo)
        
        playerName.text = SendtoPlayerDetailView.playerName
        school.text = SendtoPlayerDetailView.schoolName
        ownerDraft.text = "\(SendtoPlayerDetailView.owner) - Draft \(SendtoPlayerDetailView.draft)"
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor().colorFromHex(hexColor)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
            
        let navView = UIView()
        view.addSubview(navView)
        navView.backgroundColor = UIColor().colorFromHex(hexColor)
        navView.translatesAutoresizingMaskIntoConstraints = false
        navView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        navView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        navView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        navView.heightAnchor.constraint(equalToConstant: navbarHeight + statusbarHeight).isActive = true
        
        topView1.backgroundColor = UIColor().colorFromHex(hexColor)
        topView2.backgroundColor = UIColor().colorFromHex(hexColor)
        
        logo.kf.setImage(with: url)
        
//        addRefresh()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
//    func addRefresh() {
//        playerDetailRefreshControl.addTarget(self, action: #selector(requestData), for: .valueChanged)
//        detailPlayerTable.refreshControl = playerDetailRefreshControl
//        print("added refresh")
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ModelController.detailedViewPlayerTableValues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = detailPlayerTable.dequeueReusableCell(withIdentifier: "DetailPlayerTableViewCell") as! DetailPlayerTableViewCell
        let model = ModelController.detailedViewPlayerTableValues
        let teamID = SendtoPlayerDetailView.schoolID
        let oppoID = model.map {$0.oppoID}[indexPath.row]
        
        let teamURL = URL(string: "http://a.espncdn.com/combiner/i?img=/i/teamlogos/ncaa/500/\(teamID).png&h=150&w=150")
        let oppoURL = URL(string: "http://a.espncdn.com/combiner/i?img=/i/teamlogos/ncaa/500/\(oppoID).png&h=150&w=150")
        
        let teamScore = model.map {$0.score}[indexPath.row]
        let opponentScore = model.map {$0.oppoScore}[indexPath.row]
        let gameStatus = model.map {$0.gameTime}[indexPath.row]
        
        if teamScore > opponentScore && gameStatus.contains("Final") {
            cell.score.font = UIFont.systemFont(ofSize: 17, weight: .medium)
            cell.score.textColor = UIColor.black
            cell.oppoScore.font = UIFont.systemFont(ofSize: 17, weight: .regular)
            cell.oppoScore.textColor = UIColor.darkGray
        } else if teamScore < opponentScore && gameStatus.contains("Final") {
            cell.oppoScore.font = UIFont.systemFont(ofSize: 17, weight: .medium)
            cell.oppoScore.textColor = UIColor.black
            cell.score.font = UIFont.systemFont(ofSize: 17, weight: .regular)
            cell.score.textColor = UIColor.darkGray
        }
        cell.roundView.backgroundColor = UIColor().colorFromHex(teamHexColor)
        
        cell.round.text = "\(model.map {$0.round}[indexPath.row])"
        cell.min.text = "\(model.map {$0.min}[indexPath.row])"
        cell.fg.text = "\(model.map {$0.fg}[indexPath.row])"
        cell.ast.text = "\(model.map {$0.ast}[indexPath.row])"
        cell.reb.text = "\(model.map {$0.reb}[indexPath.row])"
        cell.to.text = "\(model.map {$0.to}[indexPath.row])"
        cell.pts.text = "\(model.map {$0.pts}[indexPath.row])"
        cell.fantasyPts.text = "\(model.map {$0.total}[indexPath.row])"
        
        cell.seed.text = "\(model.map {$0.seed}[indexPath.row])"
        cell.teamLogo.kf.setImage(with: teamURL)
        cell.score.text = "\(teamScore)"
        cell.oppoSeed.text = "\(model.map {$0.oppoSeed}[indexPath.row])"
        cell.oppLogo.kf.setImage(with: oppoURL)
        cell.oppoScore.text = "\(opponentScore)"
        cell.gameTime.text = "\(gameStatus)"
        
        
        let gamesPlayed = Float(model.map {$0.min}.count)
        let pointsArr = model.map {$0.pts}
        
        let totalPts = Float(pointsArr.reduce(0, {x, y in
            x + y
        }))
        
        let astArr = model.map {$0.ast}
        let totalAst = Float(astArr.reduce(0, {x, y in
            x + y
        }))
        
        let rebArr = model.map {$0.reb}
        let totalReb = Float(rebArr.reduce(0, {x, y in
            x + y
        }))
        
        if gamesPlayed != 0 {
            gp.text = String(format: "%.0f", gamesPlayed)
            ppg.text = String(format: "%.1f", Float(totalPts / gamesPlayed))
            //print(Float(totalPts)/Float(gamesPlayed))
            apg.text = String(format: "%.1f", Float(totalAst / gamesPlayed))
            rpg.text = String(format: "%.1f", Float(totalReb / gamesPlayed))
        } else {
            gp.text = "0"
            ppg.text = "0.0"
            //print(Float(totalPts)/Float(gamesPlayed))
            apg.text = "0.0"
            rpg.text = "0.0"
        }
        
        return cell
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    
    
    
    
    //IMPLEMENT CUSTOM POP ANIMATION ----------------------------------------------------------------------------------------
    
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!
    
    func addGesture() {
        
        guard (navigationController?.viewControllers.count)! > 1 else {
            return
        }
        
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(PlayerDetailViewController.handlePanGesture(_:)))
        self.view.addGestureRecognizer(panGestureRecognizer)
    }
    
    @objc func handlePanGesture(_ panGesture: UIPanGestureRecognizer) {
        
        let percent = max(panGesture.translation(in: view).x, 0) / view.frame.width
        
        switch panGesture.state {
            
        case .began:
            navigationController?.delegate = self
            _ = navigationController?.popViewController(animated: true)
            
        case .changed:
            if let percentDrivenInteractiveTransition = percentDrivenInteractiveTransition {
                percentDrivenInteractiveTransition.update(percent)
            }
            
        case .ended:
            let velocity = panGesture.velocity(in: view).x
            
            // Continue if drag more than 50% of screen width or velocity is higher than 1000
            if percent > 0.5 || velocity > 1000 {
                percentDrivenInteractiveTransition.finish()
            } else {
                percentDrivenInteractiveTransition.cancel()
            }
            
        case .cancelled, .failed:
            percentDrivenInteractiveTransition.cancel()
            
        default:
            break
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}


extension PlayerDetailViewController: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationControllerOperation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return SlideAnimatedTransitioning()
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        navigationController.delegate = nil
        
        if panGestureRecognizer.state == .began {
            percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            percentDrivenInteractiveTransition = nil
        }
        
        return percentDrivenInteractiveTransition
    }

// END OF POP ANIMATION ---------------------------------------------------------------------------------------
    
    
}
