//
//  JoinLeagueVC.swift
//  Fantasy App
//
//  Created by Chad on 7/28/18.
//  Copyright © 2018 Chad. All rights reserved.
//

import UIKit

class JoinLeagueVC: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    private let headerId = "JoinLeagueHeader"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.delegate = self
        collectionView?.dataSource = self
        collectionView?.showsVerticalScrollIndicator = false
        setupCollectionView()
        configureNavigationBar()
    }
    
    func setupCollectionView() {
        collectionView?.backgroundColor = .white
        collectionView?.register(CustomMyLeaguesHeader.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: headerId)
        collectionView?.backgroundColor = .white
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        return CGSize(width: view.frame.width, height: 88)
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: headerId, for: indexPath) as! CustomMyLeaguesHeader
        header.backgroundColor = .white
        header.headerLabel.text = "Join League"
        
        return header
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        configureNavigationBar()
    }
    
    let statusBarView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.frame = UIApplication.shared.statusBarFrame
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    func configureNavigationBar() {
        view.addSubview(statusBarView)
        navigationController?.isNavigationBarHidden = true
        UIApplication.shared.statusBarStyle = .default
    }
}
