//
//  CreateLeagueCell.swift
//  Fantasy App
//
//  Created by Chad on 8/19/18.
//  Copyright © 2018 Chad. All rights reserved.
//

import UIKit

class CreateLeagueCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super .init(frame: frame)
        setupViews()
    }
    
    let leagueImage: UIImageView = {
        let image = UIImageView()
        image.frame = CGRect(x: 0, y: 0, width: 60, height: 60)
        image.contentMode = UIViewContentMode.scaleAspectFit
        image.translatesAutoresizingMaskIntoConstraints = false
        image.clipsToBounds = true
        image.layer.borderWidth = 1.5
        image.layer.borderColor = lightGray.cgColor
        image.layer.cornerRadius = image.frame.height / 2
        return image
    }()
    
    let leagueName: UITextField = {
        let tf = UITextField()
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.clipsToBounds = true
        tf.autocorrectionType = .no
        tf.autocapitalizationType = .none
        tf.spellCheckingType = .no
        tf.keyboardType = .asciiCapable
        tf.tintColor = bluePrimary
        tf.placeholder = "Name your league"
        tf.textAlignment = .center
        tf.contentVerticalAlignment = .center
        tf.setBottomBorder(color: lightGray)
        return tf
    }()
    
    func setupViews() {
        addSubview(leagueImage)
        addSubview(leagueName)

        leagueImage.topAnchor.constraint(equalTo: self.topAnchor, constant: 12).isActive = true
        leagueImage.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        leagueImage.widthAnchor.constraint(equalToConstant: 60).isActive = true
        leagueImage.heightAnchor.constraint(equalToConstant: 60).isActive = true
        leagueImage.image = UIImage(named: "CameraIcon")
        
        leagueName.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 61).isActive = true
        leagueName.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -61).isActive = true
        leagueName.topAnchor.constraint(equalTo: leagueImage.bottomAnchor, constant: 6).isActive = true
        leagueName.heightAnchor.constraint(equalToConstant: 34).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

