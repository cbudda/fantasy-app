//
//  CustomSubHeaders.swift
//  Fantasy App
//
//  Created by Chad on 8/23/18.
//  Copyright © 2018 Chad. All rights reserved.
//

import UIKit

class CustomSubHeaders: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupViews()
    }
    
    let headerLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let separatorLine: UIView = {
        let view = UIView()
        view.backgroundColor = lightGray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    func setupViews() {
        
        addSubview(headerLabel)
        headerLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 10).isActive = true
        headerLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 14).isActive = true
        headerLabel.heightAnchor.constraint(equalToConstant: 22).isActive = true
        
        addSubview(separatorLine)
        separatorLine.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        separatorLine.leftAnchor.constraint(equalTo: headerLabel.leftAnchor).isActive = true
        separatorLine.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -14).isActive = true
        separatorLine.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
