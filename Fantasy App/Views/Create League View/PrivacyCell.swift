//
//  PrivacyCell.swift
//  Fantasy App
//
//  Created by Chad on 8/22/18.
//  Copyright © 2018 Chad. All rights reserved.
//

import UIKit

class CreatePrivacyCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super .init(frame: frame)
        setupViews()
    }
    
    let publicImage: UIImageView = {
        let image = UIImageView()
        image.frame = CGRect(x: 0, y: 0, width: 27.5, height: 27.5)
        image.contentMode = UIViewContentMode.scaleAspectFit
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    let privacyHeader: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Privacy"
        return label
    }()
    
    let publicLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 18, weight: .regular)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.highlightedTextColor = .black
        label.textColor = .lightGray
        return label
    }()
    
    let publicInformation: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.highlightedTextColor = grayText
        label.textColor = .lightGray
        return label
    }()
    
    let privacySeparator: UIView = {
        let view = UIView()
        view.backgroundColor = lightGray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let publicPrivateSeparator: UIView = {
        let view = UIView()
        view.backgroundColor = lightGray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    func setupViews() {
        addSubview(publicImage)
        addSubview(publicLabel)
        addSubview(publicInformation)
        addSubview(publicPrivateSeparator)
        
        publicPrivateSeparator.topAnchor.constraint(equalTo: publicInformation.bottomAnchor, constant: 12).isActive = true
        publicPrivateSeparator.leftAnchor.constraint(equalTo: publicLabel.leftAnchor).isActive = true
        publicPrivateSeparator.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -12).isActive = true
        publicPrivateSeparator.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
        
        publicImage.centerYAnchor.constraint(equalTo: publicLabel.bottomAnchor).isActive = true
        publicImage.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 12).isActive = true
        publicImage.widthAnchor.constraint(equalToConstant: 27.5).isActive = true
        publicImage.heightAnchor.constraint(equalToConstant: 27.5).isActive = true
        
        publicLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 12).isActive = true
        publicLabel.leftAnchor.constraint(equalTo: publicImage.rightAnchor, constant: 12).isActive = true
        publicLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -12).isActive = true
        
        publicInformation.topAnchor.constraint(equalTo: publicLabel.bottomAnchor).isActive = true
        publicInformation.leftAnchor.constraint(equalTo: publicLabel.leftAnchor).isActive = true
        publicInformation.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -12).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

