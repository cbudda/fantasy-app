//
//  CreateLeagueFooter.swift
//  Fantasy App
//
//  Created by Chad on 8/23/18.
//  Copyright © 2018 Chad. All rights reserved.
//

import UIKit

class CreateLeagueFooter: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupViews()
    }
    
    let createButton: UIButton = {
        let button = UIButton()
        button.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        button.setTitle("Create League", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = bluePrimary
        button.layer.cornerRadius = 4
        button.clipsToBounds = true
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    func setupViews() {
        addSubview(createButton)
        createButton.topAnchor.constraint(equalTo: self.topAnchor, constant: 8).isActive = true
        createButton.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 12).isActive = true
        createButton.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -12).isActive = true
        createButton.heightAnchor.constraint(equalToConstant: 45).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

