//
//  CreateLeagueVC.swift
//  Fantasy App
//
//  Created by Chad on 7/28/18.
//  Copyright © 2018 Chad. All rights reserved.
//

class CreateLeagueVC: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    private let headerId = "CreateLeagueHeader"
    private let subHeaderId = "SubCreateLeagueHeader"
    private let createCellId = "CreateLeaguesCell"
    private let createPrivacyCellId = "CreatePrivacyCellId"
    private let createSettingsCellId = "CreateSettingsCellId"
    private let leagueModeCellId = "leagueModeCellId"
    private let createFooterId = "createFooterId"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.delegate = self
        collectionView?.dataSource = self
        collectionView?.showsVerticalScrollIndicator = false
        setupCollectionView()
        configureNavigationBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        collectionView?.selectItem(at: IndexPath(item: 0, section: 1), animated: false, scrollPosition: [])
        collectionView?.selectItem(at: IndexPath(item: 0, section: 2), animated: false, scrollPosition: [])
        collectionView?.selectItem(at: IndexPath(item: 2, section: 3), animated: false, scrollPosition: [])
    }
    
    func setupCollectionView() {
        collectionView?.backgroundColor = .white
        collectionView?.register(CustomMyLeaguesHeader.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: headerId)
        collectionView?.register(CustomSubHeaders.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: subHeaderId)
        collectionView?.register(CreateLeagueFooter.self, forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: createFooterId)
        collectionView?.register(CreateLeagueCell.self, forCellWithReuseIdentifier: createCellId)
        collectionView?.register(CreatePrivacyCell.self, forCellWithReuseIdentifier: createPrivacyCellId)
        collectionView?.register(CreatePrivacyCell.self, forCellWithReuseIdentifier: leagueModeCellId)
        collectionView?.register(CreateSettingsCell.self, forCellWithReuseIdentifier: createSettingsCellId)
        collectionView?.alwaysBounceVertical = true
        collectionView?.backgroundColor = .white
        collectionView?.allowsMultipleSelection = true
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == 3 {
            self.collectionView?.selectItem(at: indexPath, animated: false, scrollPosition: [])
        } else {
            let selectedRows = self.collectionView?.indexPathsForSelectedItems
            for selectedRow: IndexPath in selectedRows! {
                if (selectedRow.section == indexPath.section) && (selectedRow.row != indexPath.row) {
                    self.collectionView?.deselectItem(at: selectedRow, animated: false)
                }
            }
        }
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if indexPath.section == 3 {
            self.collectionView?.deselectItem(at: indexPath, animated: false)
        } else {
            self.collectionView?.selectItem(at: indexPath, animated: false, scrollPosition: [])
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else if section == 1 || section == 2 {
            return 2
        } else {
            return 3
        }
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 6, 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: createCellId, for: indexPath) as! CreateLeagueCell
        return cell
        } else if indexPath.section == 1 {
            let privacyCell = collectionView.dequeueReusableCell(withReuseIdentifier: createPrivacyCellId, for: indexPath) as! CreatePrivacyCell
            
            if indexPath.row == 0 {
                privacyCell.publicImage.highlightedImage = UIImage(named: "NewWorldWideColor")
                privacyCell.publicLabel.text = "Public"
                privacyCell.publicInformation.text = "Anyone can join the league"
                privacyCell.publicImage.image = UIImage(named: "WorldWideGray-1")
            } else {
                privacyCell.publicImage.highlightedImage = UIImage(named: "lockColor")
                privacyCell.publicLabel.text = "Private"
                privacyCell.publicInformation.text = "Requires a password to join the league"
                privacyCell.publicImage.image = UIImage(named: "lockGray")
            }
            return privacyCell
        } else if indexPath.section == 2 {
            let leagueModeCell = collectionView.dequeueReusableCell(withReuseIdentifier: leagueModeCellId, for: indexPath) as! CreatePrivacyCell
            let leagueModes = ["Second Chance", "Classic"]
            let leagueDescription = ["Second draft after the first weekend of games","Single draft elimination tournament"]
            let selectedLeagueImage = ["SecondChanceIcon", "ClassicIcon"]
            let unselectedLeagueImage = ["SecondChanceIconGray", "ClassicIconGray"]
            
            leagueModeCell.publicLabel.text = leagueModes[indexPath.row]
            leagueModeCell.publicInformation.text = leagueDescription[indexPath.row]
            leagueModeCell.publicImage.highlightedImage = UIImage(named: selectedLeagueImage[indexPath.row])
            leagueModeCell.publicImage.image = UIImage(named: unselectedLeagueImage[indexPath.row])
            return leagueModeCell
        } else {
            if indexPath.row == 2 {
                let multiplierCell = collectionView.dequeueReusableCell(withReuseIdentifier: leagueModeCellId, for: indexPath) as! CreatePrivacyCell
                multiplierCell.publicLabel.text = "Point Multipliers"
                multiplierCell.publicInformation.text = "Fantasy point mulitpliers for each round"
                multiplierCell.publicImage.highlightedImage = UIImage(named: "CheckMark")
                multiplierCell.publicImage.image = UIImage(named: "CheckMarkGray")
                return multiplierCell
            } else {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: createSettingsCellId, for: indexPath) as! CreateSettingsCell
                let labels = ["Number of Teams", "Team Size"]
                let subLabels = ["Number of teams in the league", "Number of players on each team"]
                cell.sizeLabel.text = "12"
                cell.publicLabel.text = labels[indexPath.row]
                cell.publicInformation.text = subLabels[indexPath.row]
                return cell
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section == 0 {
            return CGSize(width: collectionView.frame.width, height: 113)
        } else {
            return CGSize(width: collectionView.frame.width, height: 64)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if section == 0 {
            return CGSize(width: view.frame.width, height: 88)
        } else {
            return CGSize(width: view.frame.width, height: 40.5)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        if section == 3 {
            return CGSize(width: view.frame.width, height: 88)
        } else {
            return CGSize(width: view.frame.width, height: 0)
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionElementKindSectionHeader {
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: headerId, for: indexPath) as! CustomMyLeaguesHeader
            header.backgroundColor = .white
        
            if indexPath.section == 0 {
                header.headerLabel.text = "Create League"
                return header
            } else {
                let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: subHeaderId, for: indexPath) as! CustomSubHeaders
                if indexPath.section == 1 {
                    header.headerLabel.text = "Privacy"
                } else if indexPath.section == 2 {
                    header.headerLabel.text = "League Mode"
                } else if indexPath.section == 3 {
                    header.headerLabel.text = "Settings"
                }
                return header
            }
        } else {
            let footer = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionFooter, withReuseIdentifier: createFooterId, for: indexPath) as! CreateLeagueFooter
            footer.backgroundColor = .white
            return footer
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        cell?.backgroundColor = lightGray
    }
    
    override func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        cell?.backgroundColor = .white
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        configureNavigationBar()
    }
    
    let statusBarView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.frame = UIApplication.shared.statusBarFrame
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    func configureNavigationBar() {
        view.addSubview(statusBarView)
        navigationController?.isNavigationBarHidden = true
        UIApplication.shared.statusBarStyle = .default
    }
}
