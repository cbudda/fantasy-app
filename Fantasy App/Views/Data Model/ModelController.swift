//
//  ModelController.swift
//  Fantasy App
//
//  Created by Chad on 6/9/18.
//  Copyright © 2018 Chad. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Players {
    
    let playerID: Int
    let draft: Int
    let games: Int
    let image: String
    let name: String
    let owner: String
    let points: Int
    let team: String
    let teamID: Int
    let wasDraft: Int
    
}

class ModelController {
    
    static var owners = [String]()
    
    static var ownerPointsArray = [(
        name: String,
        teamName: String,
        games: Int,
        points: Int,
        players: Int
        )]()
    
    static var draftedPlayerStats = [(
        name: String,
        team: String,
        games: Int,
        owner: String,
        points: Int,
        teamID: Int,
        playerID: Int,
        draft: Int
        )]()
    
    static var detailedViewPlayerTableValues = [(
        round: String,
        oppoID: Int,
        score: Int,
        oppoScore: Int,
        min: Int,
        pts: Int,
        ast: Int,
        reb: Int,
        total: Int,
        roundNum: Int,
        seed: Int,
        oppoSeed: Int,
        fg: String,
        to: Int,
        gameTime: String
        )]()
    
    static var getOwnerPlayerGames = JSON()
    static var getOwnerSummary = JSON()
    static var getOwnerPlayerSummary = JSON()
    static var getOwnerSortedOrder = [String]()
    
    static var playerStatsJSON = JSON()
    
}
