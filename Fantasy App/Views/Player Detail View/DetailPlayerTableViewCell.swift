//
//  DetailPlayerTableViewCell.swift
//  Fantasy App
//
//  Created by Chad on 5/1/18.
//  Copyright © 2018 Chad. All rights reserved.
//

import UIKit

class DetailPlayerTableViewCell: UITableViewCell {

    @IBOutlet weak var round: UILabel!
    @IBOutlet weak var teamLogo: UIImageView!
    @IBOutlet weak var seed: UILabel!
    @IBOutlet weak var oppLogo: UIImageView!
    @IBOutlet weak var oppoSeed: UILabel!
    @IBOutlet weak var score: UILabel!
    @IBOutlet weak var oppoScore: UILabel!
    @IBOutlet weak var min: UILabel!
    @IBOutlet weak var pts: UILabel!
    @IBOutlet weak var ast: UILabel!
    @IBOutlet weak var reb: UILabel!
    @IBOutlet weak var fantasyPts: UILabel!
    @IBOutlet weak var gameTime: UILabel!
    @IBOutlet weak var fg: UILabel!
    @IBOutlet weak var to: UILabel!
    @IBOutlet weak var roundView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
