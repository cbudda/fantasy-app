//
//  MenuVC.swift
//  Fantasy App
//
//  Created by Chad on 7/28/18.
//  Copyright © 2018 Chad. All rights reserved.
//

import UIKit
import AWSCognitoIdentityProvider

class MenuVC: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    private let headerId = "MenuHeader"
    private let menuCellId = "menuCellId"
    var user: AWSCognitoIdentityUser?
    var userAttributes: [AWSCognitoIdentityProviderAttributeType]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.delegate = self
        collectionView?.dataSource = self
        collectionView?.showsVerticalScrollIndicator = false
        setupCollectionView()
        configureNavigationBar()
        fetchUserAttributes()
    }
    
    func fetchUserAttributes() {
        user = AppDelegate.defaultUserPool().currentUser()
        user?.getDetails().continueOnSuccessWith(block: { (task) -> Any? in
            guard task.result != nil else {
                return nil
            }
            self.userAttributes = task.result?.userAttributes
            self.userAttributes?.forEach({ (attribute) in
                print(attribute.name! + ": " + attribute.value!)
            })
            DispatchQueue.main.async {
                print("done")
            }
            return nil
        })
    }
    
    func setupCollectionView() {
        collectionView?.backgroundColor = .white
        collectionView?.register(CustomMyLeaguesHeader.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: headerId)
        collectionView?.register(WordCell.self, forCellWithReuseIdentifier: menuCellId)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        return CGSize(width: view.frame.width, height: 88)
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: headerId, for: indexPath) as! CustomMyLeaguesHeader
        header.backgroundColor = .white
        header.headerLabel.text = "Settings"
        
        return header
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        configureNavigationBar()
    }
    
    let statusBarView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.frame = UIApplication.shared.statusBarFrame
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    func configureNavigationBar() {
        view.addSubview(statusBarView)
        navigationController?.isNavigationBarHidden = true
        UIApplication.shared.statusBarStyle = .default
    }
    
    func handleSignOut() {
        UserDefaults.standard.set(false, forKey: "isLoggedIn")
        UserDefaults.standard.synchronize()
        user?.signOut()
        self.fetchUserAttributes()
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        handleSignOut()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(10, 0, 10, 0)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: menuCellId, for: indexPath) as! WordCell
        cell.backgroundColor = .clear
        cell.wordLabel.text = "Log out"
        cell.labelImage.image = UIImage(named: "logout")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 50)
    }
}
