//
//  VerifyVC.swift
//  Fantasy App
//
//  Created by Chad on 8/7/18.
//  Copyright © 2018 Chad. All rights reserved.
//

import UIKit
import AWSCognitoIdentityProvider

class VerifyCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super .init(frame: frame)
        setupViews()
    }
    
    let image: UIImageView = {
        let image = UIImageView()
        image.frame = CGRect(x: 0, y: 0, width: 200, height: 35)
        image.contentMode = UIViewContentMode.scaleAspectFit
        image.translatesAutoresizingMaskIntoConstraints = false
        image.clipsToBounds = true
        return image
    }()
    
    let welcomeLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "We're almost there!"
        return label
    }()
    
    let loginInstructions: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 15)
        label.textColor = grayText
        label.translatesAutoresizingMaskIntoConstraints = false
//        label.text = "Please enter the verification code that was \nsent to the email you provided"
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    let verifyPh: UILabel = {
        let label = UILabel()
        label.text = "Verification code"
        label.font = UIFont.systemFont(ofSize: 17)
        label.textColor = placeholderGray
        label.translatesAutoresizingMaskIntoConstraints = false
        label.sizeToFit()
        return label
    }()
    
    let verificationCodeTextField: CustomTextField = {
        let tf = CustomTextField()
        tf.bottomPaddingPoints(0)
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.layer.borderWidth = 1
        tf.layer.borderColor = placeholderGray.cgColor
        tf.layer.cornerRadius = 4
        tf.clipsToBounds = true
        tf.keyboardType = .numberPad
        return tf
    }()
    
    let verifyButton: UIButton = {
        let button = UIButton()
        button.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        button.setTitle("Verify email address", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = bluePrimary
        button.layer.cornerRadius = 4
        button.clipsToBounds = true
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let resendButton: UIButton = {
        let button = UIButton()
        button.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        button.setTitle("Resend confirmation code", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = bluePrimary
        button.layer.cornerRadius = 4
        button.clipsToBounds = true
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let separatorLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = .gray
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "or"
        return label
    }()
    
    let leftSeparatorView: UIView = {
        let view = UIView()
        view.backgroundColor = lightGray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let rightSeparatorView: UIView = {
        let view = UIView()
        view.backgroundColor = lightGray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var verifyPhCenter: NSLayoutConstraint?
    var verifyPhTop: NSLayoutConstraint?
    
    func setupViews() {
        addSubview(image)
        image.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        image.topAnchor.constraint(equalTo: self.topAnchor, constant: 125 - UIApplication.shared.statusBarFrame.height).isActive = true
        image.heightAnchor.constraint(equalToConstant: 35).isActive = true
        image.image = UIImage(named: "instacart")
        
        addSubview(welcomeLabel)
        welcomeLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        welcomeLabel.topAnchor.constraint(equalTo: image.bottomAnchor, constant: 16).isActive = true
        
        addSubview(loginInstructions)
        loginInstructions.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        loginInstructions.topAnchor.constraint(equalTo: welcomeLabel.bottomAnchor, constant: 6).isActive = true
        
        addSubview(verificationCodeTextField)
        verificationCodeTextField.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16).isActive = true
        verificationCodeTextField.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16).isActive = true
        verificationCodeTextField.topAnchor.constraint(equalTo: loginInstructions.bottomAnchor, constant: 12).isActive = true
        verificationCodeTextField.heightAnchor.constraint(equalToConstant: 54).isActive = true
        
        addSubview(verifyPh)
        verifyPh.leftAnchor.constraint(equalTo: verificationCodeTextField.leftAnchor, constant: 8).isActive = true
        verifyPhCenter = verifyPh.centerYAnchor.constraint(equalTo: verificationCodeTextField.centerYAnchor)
        verifyPhCenter?.isActive = true
        verifyPhTop = verifyPh.topAnchor.constraint(equalTo: verificationCodeTextField.topAnchor, constant: 6)
        verifyPhTop?.isActive = false
        
        addSubview(verifyButton)
        verifyButton.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16).isActive = true
        verifyButton.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16).isActive = true
        verifyButton.topAnchor.constraint(equalTo: verificationCodeTextField.bottomAnchor, constant: 12).isActive = true
        verifyButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        addSubview(separatorLabel)
        separatorLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        separatorLabel.topAnchor.constraint(equalTo: verifyButton.bottomAnchor, constant: 12).isActive = true
        
        addSubview(leftSeparatorView)
        leftSeparatorView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 64).isActive = true
        leftSeparatorView.rightAnchor.constraint(equalTo: separatorLabel.leftAnchor, constant: -12).isActive = true
        leftSeparatorView.centerYAnchor.constraint(equalTo: separatorLabel.centerYAnchor).isActive = true
        leftSeparatorView.heightAnchor.constraint(equalToConstant: 0.25).isActive = true
        
        addSubview(rightSeparatorView)
        rightSeparatorView.leftAnchor.constraint(equalTo: separatorLabel.rightAnchor, constant: 12).isActive = true
        rightSeparatorView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -64).isActive = true
        rightSeparatorView.centerYAnchor.constraint(equalTo: separatorLabel.centerYAnchor).isActive = true
        rightSeparatorView.heightAnchor.constraint(equalToConstant: 0.25).isActive = true
        
        addSubview(resendButton)
        resendButton.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16).isActive = true
        resendButton.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16).isActive = true
        resendButton.topAnchor.constraint(equalTo: separatorLabel.bottomAnchor, constant: 12).isActive = true
        resendButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class VerifyController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    private let verifyCellId = "verifyCellId"
    var user: AWSCognitoIdentityUser?
    var codeDeliveryDetails:AWSCognitoIdentityProviderCodeDeliveryDetailsType?
    var cell: VerifyCell?
    var u: String?
    var p: String?
    
    let whiteView: UIView = {
        let v = UIView()
        v.backgroundColor = .white
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.delegate = self
        collectionView?.dataSource = self
        collectionView?.showsVerticalScrollIndicator = false
        collectionView?.isScrollEnabled = true
        collectionView?.alwaysBounceVertical = true
        setupCollectionView()
        configureNavigationBar()
        setupHideKeyboardOnTap()
    }
    
    func setupCollectionView() {
        collectionView?.backgroundColor = .white
        collectionView?.register(VerifyCell.self, forCellWithReuseIdentifier: verifyCellId)
        
        view.insertSubview(whiteView, belowSubview: collectionView!)
        whiteView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        configureNavigationBar()
        populateCodeDeliveryDetails()
        setupPlaceholderText()
    }
    
    func populateCodeDeliveryDetails() {
        let indexPath = IndexPath(item: 0, section: 0);
        if let cell = collectionView?.cellForItem(at: indexPath) as? VerifyCell
        {
            print(codeDeliveryDetails?.destination)
            print("Medium \(codeDeliveryDetails?.deliveryMedium)")
            print("AWSCOG \(AWSCognitoIdentityProviderDeliveryMediumType.email)")
            let isEmail = (codeDeliveryDetails?.deliveryMedium == AWSCognitoIdentityProviderDeliveryMediumType.email)
            print(isEmail)
            cell.verifyButton.setTitle(isEmail ? "Verify Email Address" : "Verify Phone Number", for: .normal)
            let medium = isEmail ? "your email address" : "your phone number"
            let destination = codeDeliveryDetails!.destination!
            cell.loginInstructions.text = "Please enter the code that was \nsent to \(medium) at \(destination)"
            cell.layoutIfNeeded()
        }
    }
    
    func resetConfirmation(message:String? = "") {
        let indexPath = IndexPath(item: 0, section: 0);
        if let cell = collectionView?.cellForItem(at: indexPath) as? VerifyCell
        {
            cell.verificationCodeTextField.text = ""
            let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Retry", style: .default, handler: nil))
            self.present(alert, animated: true, completion:nil)
        }
    }
    
    func setupPlaceholderText() {
        let indexPath = IndexPath(item: 0, section: 0)
        if let cell = collectionView?.cellForItem(at: indexPath) as? VerifyCell
        {
            cell.verifyPhCenter?.isActive = true
            cell.verifyPhTop?.isActive = false
            cell.verifyPh.textColor = placeholderGray
            cell.verifyPh.font = UIFont.systemFont(ofSize: 17)
            cell.verificationCodeTextField.contentVerticalAlignment = .center
            cell.verificationCodeTextField.bottomPaddingPoints(0)
        }
    }
    
    @objc func inputDidChange(_ sender:AnyObject) {
        let indexPath = IndexPath(item: 0, section: 0);
        if let cell = collectionView?.cellForItem(at: indexPath) as? VerifyCell
        {
            if (cell.verificationCodeTextField.text?.count)! > 0 && cell.verificationCodeTextField.isEditing {
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    cell.verifyPhCenter?.isActive = false
                    cell.verifyPhTop?.isActive = true
                    cell.verifyPh.textColor = bluePrimary
                    cell.verifyPh.font = UIFont.systemFont(ofSize: 14)
                    cell.layoutIfNeeded()
                })
                cell.verificationCodeTextField.contentVerticalAlignment = .bottom
                cell.verificationCodeTextField.bottomPaddingPoints(6)
                
            } else if (cell.verificationCodeTextField.text?.count)! == 0{
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    cell.verifyPhCenter?.isActive = true
                    cell.verifyPhTop?.isActive = false
                    cell.verifyPh.textColor = placeholderGray
                    cell.verifyPh.font = UIFont.systemFont(ofSize: 17)
                    cell.layoutIfNeeded()
                })
                cell.verificationCodeTextField.contentVerticalAlignment = .center
                cell.verificationCodeTextField.bottomPaddingPoints(0)
            }
            
            if((cell.verificationCodeTextField.text?.count)! < 6) {
                cell.verifyButton.isEnabled = false
                cell.verifyButton.alpha = 0.75
                return
            }
            cell.verifyButton.isEnabled = true
            cell.verifyButton.alpha = 1
        }
    }
    
    @objc func didBeginEdit() {
        let indexPath = IndexPath(item: 0, section: 0);
        if let cell = collectionView?.cellForItem(at: indexPath) as? VerifyCell
        {
            cell.verificationCodeTextField.layer.borderColor = bluePrimary.cgColor
            if (cell.verificationCodeTextField.text?.count)! > 0 {
                cell.verifyPh.textColor = bluePrimary
            }
        }
    }
    
    @objc func didEndEdit() {
        let indexPath = IndexPath(item: 0, section: 0);
        if let cell = collectionView?.cellForItem(at: indexPath) as? VerifyCell
        {
            cell.verificationCodeTextField.layer.borderColor = placeholderGray.cgColor
            cell.verifyPh.textColor = placeholderGray
        }
    }
    
    func configureNavigationBar() {
        navigationController?.isNavigationBarHidden = true
        UIApplication.shared.statusBarStyle = .default
    }
    
    func setupHideKeyboardOnTap() {
        self.view.addGestureRecognizer(self.endEditingRecognizer())
        self.navigationController?.navigationBar.addGestureRecognizer(self.endEditingRecognizer())
    }
    
    /// Dismisses the keyboard from self.view
    private func endEditingRecognizer() -> UIGestureRecognizer {
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(self.view.endEditing(_:)))
        tap.cancelsTouchesInView = false
        return tap
    }
    
    @objc func verifyPressed() {
        print("Start verification process")
        let indexPath = IndexPath(item: 0, section: 0)
        if let cell = collectionView?.cellForItem(at: indexPath) as? VerifyCell
        {
            self.user?.confirmSignUp(cell.verificationCodeTextField.text!)
                .continueWith(block: { (response) -> Any? in
                    if response.error != nil {
                        self.resetConfirmation(message: (response.error! as NSError).userInfo["message"] as? String)
                    } else {
                        DispatchQueue.main.async {
                            
                            let cognitoUser = AppDelegate.defaultUserPool().getUser(self.u!)
                            cognitoUser.getSession(self.u!, password: self.p!, validationData: nil).continueWith(block: {(_ task: AWSTask<AWSCognitoIdentityUserSession>) -> Any in
                                return print(AppDelegate.defaultUserPool().currentUser()?.isSignedIn)
                            })
                            
                            let rootViewController = UIApplication.shared.keyWindow?.rootViewController
                            guard let mainNavigationController = rootViewController as? MainNavigationController else { return }
                            
                            mainNavigationController.viewControllers = [CustomTabBarController()]
                            
                            UserDefaults.standard.set(true, forKey: "isLoggedIn")
                            UserDefaults.standard.synchronize()
                            
                            self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
                        }
                    }
                    return nil
                })
        }
    }
    
    @objc func resendVerificationFunc() {
        print("Resend confirmation code")
        self.user?.resendConfirmationCode()
        .continueWith(block: { (respone) -> Any? in
            let alert = UIAlertController(title: "Resent", message: "The confirmation code has been resent.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion:nil)
            return nil
        })
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 10, 0)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: verifyCellId, for: indexPath) as! VerifyCell
        cell.backgroundColor = .clear
        let isEmail = (codeDeliveryDetails?.deliveryMedium == AWSCognitoIdentityProviderDeliveryMediumType.email)
        print(isEmail)
        cell.verifyButton.setTitle(isEmail ? "Verify Email Address" : "Verify Phone Number", for: .normal)
        let medium = isEmail ? "your email address" : "your phone number"
        let destination = codeDeliveryDetails!.destination!
        cell.loginInstructions.text = "Please enter the code that was sent \nto \(medium) at \(destination)"
        
        cell.verifyButton.addTarget(self, action: #selector(self.verifyPressed), for: .touchUpInside)
        cell.resendButton.addTarget(self, action: #selector(self.resendVerificationFunc), for: .touchUpInside)
        
        cell.verifyButton.isEnabled = false
        cell.verifyButton.alpha = 0.75
        cell.verificationCodeTextField.addTarget(self, action: #selector(inputDidChange(_:)), for: .editingChanged)
        
        cell.verificationCodeTextField.addTarget(self, action: #selector(self.didBeginEdit), for: .editingDidBegin)
        cell.verificationCodeTextField.addTarget(self, action: #selector(self.didEndEdit), for: .editingDidEnd)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 600)
    }
}
