//
//  CustomTextField.swift
//  Fantasy App
//
//  Created by Chad on 8/15/18.
//  Copyright © 2018 Chad. All rights reserved.
//

import UIKit

class CustomTextField: UITextField, UITextFieldDelegate{
    
    let border = CALayer()
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        delegate = self
//        createBorder(color: CGColor)
    }
    required override init(frame: CGRect) {
        super.init(frame: frame)
        delegate = self
//        createBorder(color: CGColor)
    }
    
    func createBorder(color: CGColor){
        let width = CGFloat(2.0)
        border.borderColor = color
        border.frame = CGRect(x: 0, y: self.frame.size.height-width, width: self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        clearButtonMode = .whileEditing
        movePlaceholderUp()
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        movePlaceholderDown()
    }
    
    func movePlaceholderUp(){
        border.borderColor = UIColor(red:0.87, green:0.30, blue:0.32, alpha:1.0).cgColor
    }
    func movePlaceholderDown(){
        border.borderColor = UIColor(red:0.60, green:0.60, blue:0.60, alpha:1.0).cgColor
    }
    
    var padding = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 0)
    
    func bottomPaddingPoints(_ amount:CGFloat){
        padding = UIEdgeInsets(top: 0, left: 8, bottom: amount, right: 0)
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
}
