//
//  LoginController.swift
//  Fantasy App
//
//  Created by Chad on 8/6/18.
//  Copyright © 2018 Chad. All rights reserved.
//

import UIKit
import AWSCognitoIdentityProvider

class LoginCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super .init(frame: frame)
        setupViews()
    }
    
    let loginVC = LoginController()
    
    let image: UIImageView = {
        let image = UIImageView()
        image.frame = CGRect(x: 0, y: 0, width: 200, height: 35)
        image.contentMode = UIViewContentMode.scaleAspectFit
        image.translatesAutoresizingMaskIntoConstraints = false
        image.clipsToBounds = true
        return image
    }()
    
    let welcomeLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Welcome Back!"
        return label
    }()
    
    let loginInstructions: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 15)
        label.textColor = grayText
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Log in with your email and password."
        return label
    }()
    
    let emailPhText: UILabel = {
        let label = UILabel()
        label.text = "Enter your email"
        label.font = UIFont.systemFont(ofSize: 17)
        label.textColor = placeholderGray
        label.translatesAutoresizingMaskIntoConstraints = false
        label.sizeToFit()
        return label
    }()
    
    let passwordPhText: UILabel = {
        let label = UILabel()
        label.text = "Enter your password"
        label.font = UIFont.systemFont(ofSize: 17)
        label.textColor = placeholderGray
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let emailTextField: CustomTextField = {
        let tf = CustomTextField()
        tf.bottomPaddingPoints(0)
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.layer.borderWidth = 1
        tf.layer.borderColor = placeholderGray.cgColor
        tf.layer.cornerRadius = 4
        tf.clipsToBounds = true
        tf.autocorrectionType = .no
        tf.autocapitalizationType = .none
        tf.spellCheckingType = .no
        tf.keyboardType = .asciiCapable
        tf.tintColor = bluePrimary
        return tf
    }()
    
    let passwordTextField: CustomTextField = {
        let tf = CustomTextField()
        tf.bottomPaddingPoints(0)
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.layer.borderWidth = 1
        tf.layer.borderColor = placeholderGray.cgColor
        tf.layer.cornerRadius = 4
        tf.clipsToBounds = true
        tf.autocorrectionType = .no
        tf.autocapitalizationType = .none
        tf.spellCheckingType = .no
        tf.isSecureTextEntry = true
        tf.tintColor = bluePrimary
        return tf
    }()
    
    let loginButton: UIButton = {
        let button = UIButton()
        button.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        button.setTitle("Log in", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = bluePrimary
        button.layer.cornerRadius = 4
        button.clipsToBounds = true
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let facebookButton: UIButton = {
        let button = UIButton()
        button.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        button.setTitle("Continue with Facebook", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = UIColor(red: 67/255, green: 105/255, blue: 176/255, alpha: 1)
        button.layer.cornerRadius = 4
        button.clipsToBounds = true
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let separatorLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12, weight: .light)
        label.textColor = .gray
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "or"
        return label
    }()
    
    let leftSeparatorView: UIView = {
        let view = UIView()
        view.backgroundColor = lightGray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let rightSeparatorView: UIView = {
        let view = UIView()
        view.backgroundColor = lightGray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let signupButton: UIButton = {
        let button = UIButton()
        let s1 = "Don't have an account? "
        let s1Attributes : [NSAttributedStringKey : Any] = [NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue) : UIFont.systemFont(ofSize: 14, weight: .regular), NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue) : UIColor.gray]
        let attributedString = NSMutableAttributedString(string: s1, attributes: s1Attributes)
        
        let s2 = "Sign up"
        let s2Attributes : [NSAttributedStringKey : Any] = [NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue) : UIFont.systemFont(ofSize: 14, weight: .semibold), NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue) : bluePrimary]
        let s2String = NSMutableAttributedString(string: s2, attributes: s2Attributes)
        attributedString.append(s2String)
        button.setAttributedTitle(attributedString, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
//        button.addTarget(self.loginVC, action: #selector(self.loginVC.signupFunc), for: .touchUpInside)
        return button
    }()
    
    let resetButton: UIButton = {
        let button = UIButton()
        let s1 = "Forgot password? "
        let s1Attributes : [NSAttributedStringKey : Any] = [NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue) : UIFont.systemFont(ofSize: 14, weight: .regular), NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue) : UIColor.gray]
        let attributedString = NSMutableAttributedString(string: s1, attributes: s1Attributes)
        
        let s2 = "Reset it"
        let s2Attributes : [NSAttributedStringKey : Any] = [NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue) : UIFont.systemFont(ofSize: 14, weight: .semibold), NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue) : bluePrimary]
        let s2String = NSMutableAttributedString(string: s2, attributes: s2Attributes)
        attributedString.append(s2String)
        button.setAttributedTitle(attributedString, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let bottomSeparatorView: UIView = {
        let view = UIView()
        view.backgroundColor = lightGray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var emailPhCenterAnchor: NSLayoutConstraint?
    var emailPhTopAnchor: NSLayoutConstraint?
    var passwordPhCenterAnchor: NSLayoutConstraint?
    var passwordPhTopAnchor: NSLayoutConstraint?
    
    func setupViews() {
        addSubview(image)
        image.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        image.topAnchor.constraint(equalTo: self.topAnchor, constant: 125 - UIApplication.shared.statusBarFrame.height).isActive = true
        image.heightAnchor.constraint(equalToConstant: 80).isActive = true
        image.image = UIImage(named: "Logo")
        
        addSubview(welcomeLabel)
        welcomeLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        welcomeLabel.topAnchor.constraint(equalTo: image.bottomAnchor, constant: 16).isActive = true
        
        addSubview(loginInstructions)
        loginInstructions.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        loginInstructions.topAnchor.constraint(equalTo: welcomeLabel.bottomAnchor, constant: 6).isActive = true
        
        addSubview(emailTextField)
        emailTextField.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16).isActive = true
        emailTextField.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16).isActive = true
        emailTextField.topAnchor.constraint(equalTo: loginInstructions.bottomAnchor, constant: 12).isActive = true
        emailTextField.heightAnchor.constraint(equalToConstant: 54).isActive = true
        
        addSubview(emailPhText)
        emailPhText.leftAnchor.constraint(equalTo: emailTextField.leftAnchor, constant: 8).isActive = true
        emailPhCenterAnchor = emailPhText.centerYAnchor.constraint(equalTo: emailTextField.centerYAnchor)
        emailPhCenterAnchor?.isActive = true
        emailPhTopAnchor = emailPhText.topAnchor.constraint(equalTo: emailTextField.topAnchor, constant: 6)
        emailPhTopAnchor?.isActive = false
        
        addSubview(passwordTextField)
        passwordTextField.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16).isActive = true
        passwordTextField.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16).isActive = true
        passwordTextField.topAnchor.constraint(equalTo: emailTextField.bottomAnchor, constant: 6).isActive = true
        passwordTextField.heightAnchor.constraint(equalToConstant: 54).isActive = true
        
        addSubview(passwordPhText)
        passwordPhText.leftAnchor.constraint(equalTo: passwordTextField.leftAnchor, constant: 8).isActive = true
        passwordPhCenterAnchor = passwordPhText.centerYAnchor.constraint(equalTo: passwordTextField.centerYAnchor)
        passwordPhCenterAnchor?.isActive = true
        passwordPhTopAnchor = passwordPhText.topAnchor.constraint(equalTo: passwordTextField.topAnchor, constant: 6)
        passwordPhTopAnchor?.isActive = false
        
        addSubview(loginButton)
        loginButton.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16).isActive = true
        loginButton.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16).isActive = true
        loginButton.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: 8).isActive = true
        loginButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        addSubview(separatorLabel)
        separatorLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        separatorLabel.topAnchor.constraint(equalTo: loginButton.bottomAnchor, constant: 12).isActive = true
        
        addSubview(leftSeparatorView)
        leftSeparatorView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 64).isActive = true
        leftSeparatorView.rightAnchor.constraint(equalTo: separatorLabel.leftAnchor, constant: -12).isActive = true
        leftSeparatorView.centerYAnchor.constraint(equalTo: separatorLabel.centerYAnchor).isActive = true
        leftSeparatorView.heightAnchor.constraint(equalToConstant: 0.25).isActive = true
        
        addSubview(rightSeparatorView)
        rightSeparatorView.leftAnchor.constraint(equalTo: separatorLabel.rightAnchor, constant: 12).isActive = true
        rightSeparatorView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -64).isActive = true
        rightSeparatorView.centerYAnchor.constraint(equalTo: separatorLabel.centerYAnchor).isActive = true
        rightSeparatorView.heightAnchor.constraint(equalToConstant: 0.25).isActive = true
        
        addSubview(facebookButton)
        facebookButton.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16).isActive = true
        facebookButton.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16).isActive = true
        facebookButton.topAnchor.constraint(equalTo: separatorLabel.bottomAnchor, constant: 12).isActive = true
        facebookButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        addSubview(signupButton)
        signupButton.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        signupButton.topAnchor.constraint(equalTo: facebookButton.bottomAnchor, constant: 24).isActive = true
        
        addSubview(bottomSeparatorView)
        bottomSeparatorView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16).isActive = true
        bottomSeparatorView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16).isActive = true
        bottomSeparatorView.topAnchor.constraint(equalTo: signupButton.bottomAnchor, constant: 8).isActive = true
        bottomSeparatorView.heightAnchor.constraint(equalToConstant: 0.25).isActive = true
        
        addSubview(resetButton)
        resetButton.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        resetButton.topAnchor.constraint(equalTo: bottomSeparatorView.bottomAnchor, constant: 8).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class LoginController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    private let loginCellId = "loginCellId"
    var passwordAuthenticationCompletion: AWSTaskCompletionSource<AWSCognitoIdentityPasswordAuthenticationDetails>?
    
    let imageView: UIImageView = {
        let image = UIImageView()
        image.contentMode = UIViewContentMode.scaleToFill
        image.translatesAutoresizingMaskIntoConstraints = false
        image.clipsToBounds = true
        image.image = UIImage(named: "stadium")
        return image
    }()
    
    let whiteView: UIView = {
        let v = UIView()
        v.backgroundColor = .white
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.delegate = self
        collectionView?.dataSource = self
        collectionView?.showsVerticalScrollIndicator = false
        collectionView?.isScrollEnabled = true
        collectionView?.alwaysBounceVertical = true
        setupCollectionView()
        configureNavigationBar()
        setupHideKeyboardOnTap()
    }
    
    func setupCollectionView() {
        collectionView?.backgroundColor = .clear
        collectionView?.register(LoginCell.self, forCellWithReuseIdentifier: loginCellId)
        
        view.insertSubview(imageView, belowSubview: collectionView!)
        view.insertSubview(whiteView, belowSubview: imageView)
        whiteView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        
        imageView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 140)
        imageView.alpha = 1
        imageView.contentMode = .scaleToFill
        imageView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        imageView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 140).isActive = true
        imageView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        
        let gradient = CAGradientLayer()
        gradient.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: imageView.frame.height)
        gradient.colors = [UIColor.clear.cgColor, UIColor.white.cgColor]
        gradient.startPoint = CGPoint(x: 0, y: 0.0)
        gradient.endPoint = CGPoint(x: 0, y: 1.0)
        imageView.layer.addSublayer(gradient)
        print(imageView.frame.size.height)
    }
    
    func setupHideKeyboardOnTap() {
        self.view.addGestureRecognizer(self.endEditingRecognizer())
        self.navigationController?.navigationBar.addGestureRecognizer(self.endEditingRecognizer())
    }
    
    /// Dismisses the keyboard from self.view
    private func endEditingRecognizer() -> UIGestureRecognizer {
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(self.view.endEditing(_:)))
        tap.cancelsTouchesInView = false
        return tap
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        configureNavigationBar()
        setupPlaceholderText()
    }
    
    func setupPlaceholderText() {
        let indexPath = IndexPath(item: 0, section: 0)
        if let cell = collectionView?.cellForItem(at: indexPath) as? LoginCell
        {
            cell.emailPhCenterAnchor?.isActive = true
            cell.emailPhTopAnchor?.isActive = false
            cell.emailPhText.textColor = placeholderGray
            cell.emailPhText.font = UIFont.systemFont(ofSize: 17)
            cell.emailTextField.contentVerticalAlignment = .center
            cell.emailTextField.bottomPaddingPoints(0)
            
            cell.passwordPhCenterAnchor?.isActive = true
            cell.passwordPhTopAnchor?.isActive = false
            cell.passwordPhText.textColor = placeholderGray
            cell.passwordPhText.font = UIFont.systemFont(ofSize: 17)
            cell.passwordTextField.contentVerticalAlignment = .center
            cell.passwordTextField.bottomPaddingPoints(0)
        }
    }
    
    func configureNavigationBar() {
        navigationController?.isNavigationBarHidden = true
        UIApplication.shared.statusBarStyle = .default
    }
    
    
    @objc func inputDidChange(_ sender:AnyObject) {
        let indexPath = IndexPath(item: 0, section: 0)
        if let cell = collectionView?.cellForItem(at: indexPath) as? LoginCell
        {
            if (cell.emailTextField.text?.count)! > 0 && cell.emailTextField.isEditing {
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    cell.emailPhCenterAnchor?.isActive = false
                    cell.emailPhTopAnchor?.isActive = true
                    cell.emailPhText.textColor = bluePrimary
                    cell.emailPhText.font = UIFont.systemFont(ofSize: 14)
                    cell.layoutIfNeeded()
                })
                cell.emailTextField.contentVerticalAlignment = .bottom
                cell.emailTextField.bottomPaddingPoints(6)
                
            } else if (cell.emailTextField.text?.count)! == 0{
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    cell.emailPhCenterAnchor?.isActive = true
                    cell.emailPhTopAnchor?.isActive = false
                    cell.emailPhText.textColor = placeholderGray
                    cell.emailPhText.font = UIFont.systemFont(ofSize: 17)
                    cell.layoutIfNeeded()
                })
                cell.emailTextField.contentVerticalAlignment = .center
                cell.emailTextField.bottomPaddingPoints(0)
            }
            
            if (cell.passwordTextField.text?.count)! > 0 && cell.passwordTextField.isEditing {
                UIView.animate(withDuration: 0.75, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    cell.passwordPhCenterAnchor?.isActive = false
                    cell.passwordPhTopAnchor?.isActive = true
                    cell.passwordPhText.textColor = bluePrimary
                    cell.passwordPhText.font = UIFont.systemFont(ofSize: 14)
                    cell.layoutIfNeeded()
                })
                cell.passwordTextField.contentVerticalAlignment = .bottom
                cell.passwordTextField.bottomPaddingPoints(6)
                
            } else if (cell.passwordTextField.text?.count)! == 0{
                UIView.animate(withDuration: 0.75, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    cell.passwordPhCenterAnchor?.isActive = true
                    cell.passwordPhTopAnchor?.isActive = false
                    cell.passwordPhText.textColor = placeholderGray
                    cell.passwordPhText.font = UIFont.systemFont(ofSize: 17)
                    cell.layoutIfNeeded()
                })
                cell.passwordTextField.contentVerticalAlignment = .center
                cell.passwordTextField.bottomPaddingPoints(0)
            }
            
            if (cell.emailTextField.text != nil && cell.passwordTextField.text != nil) {
                cell.loginButton.isEnabled = true
            } else {
                cell.loginButton.isEnabled = false
            }
        }
    }
    
    @objc func didBeginEdit() {
        let indexPath = IndexPath(item: 0, section: 0);
        if let cell = collectionView?.cellForItem(at: indexPath) as? LoginCell
        {
            if cell.emailTextField.isEditing {
                cell.emailTextField.layer.borderColor = bluePrimary.cgColor
                if (cell.emailTextField.text?.count)! > 0 {
                    cell.emailPhText.textColor = bluePrimary
                }
            } else {
                cell.passwordTextField.layer.borderColor = bluePrimary.cgColor
                if (cell.passwordTextField.text?.count)! > 0 {
                    cell.passwordPhText.textColor = bluePrimary
                }
            }
        }
    }
    
    @objc func didEndEdit() {
        let indexPath = IndexPath(item: 0, section: 0);
        if let cell = collectionView?.cellForItem(at: indexPath) as? LoginCell
        {
            cell.emailTextField.layer.borderColor = placeholderGray.cgColor
            cell.emailPhText.textColor = placeholderGray
            cell.passwordTextField.layer.borderColor = placeholderGray.cgColor
            cell.passwordPhText.textColor = placeholderGray
        }
    }
    
    @objc func loginPressed() {
        print("login pressed")
        let indexPath = IndexPath(item: 0, section: 0);
        if let cell = collectionView?.cellForItem(at: indexPath) as? LoginCell
        {
            if (cell.emailTextField.text == nil || cell.passwordTextField.text == nil) {
                return
            }
            
            let authDetails = AWSCognitoIdentityPasswordAuthenticationDetails(username: cell.emailTextField.text!.lowercased(), password: cell.passwordTextField.text!)
            self.passwordAuthenticationCompletion?.set(result: authDetails)
            AppDelegate.defaultUserPool().currentUser()?.getSession()
        }
    }
    
    @objc func facebookPressed() {
        print("Initiate Facebook login function")
    }
    
    @objc func signupFunc() {
        print("Signup func pressed")
        let signupLayout = UICollectionViewFlowLayout()
        let vc = SignupController(collectionViewLayout: signupLayout)
        let navController = UINavigationController(rootViewController: vc)
        self.present(navController, animated: true, completion: nil)
    }
    
    @objc func resetPassword() {
        print("Reset password pressed")
//        if (self.usernameInput?.text == nil || self.usernameInput!.text!.isEmpty) {
//            let alertController = UIAlertController(title: "Enter Username",
//                                                    message: "Please enter your username and then select Forgot Password if you want to reset your password.",
//                                                    preferredStyle: .alert)
//            let retryAction = UIAlertAction(title: "OK", style: .default, handler: nil)
//            alertController.addAction(retryAction)
//            self.present(alertController, animated: true, completion:  nil)
//            return
//        }
//        self.performSegue(withIdentifier: "ForgotPasswordSegue", sender: self)
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 10, 0)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: loginCellId, for: indexPath) as! LoginCell
        cell.backgroundColor = .clear
        cell.loginButton.addTarget(self, action: #selector(self.loginPressed), for: .touchUpInside)
        cell.facebookButton.addTarget(self, action: #selector(self.facebookPressed), for: .touchUpInside)
        cell.signupButton.addTarget(self, action: #selector(self.signupFunc), for: .touchUpInside)
        cell.resetButton.addTarget(self, action: #selector(self.resetPassword), for: .touchUpInside)
        
        cell.emailTextField.addTarget(self, action: #selector(inputDidChange(_:)), for: .editingChanged)
        cell.passwordTextField.addTarget(self, action: #selector(inputDidChange(_:)), for: .editingChanged)
        
        cell.emailTextField.addTarget(self, action: #selector(self.didBeginEdit), for: .editingDidBegin)
        cell.passwordTextField.addTarget(self, action: #selector(self.didBeginEdit), for: .editingDidBegin)

        cell.emailTextField.addTarget(self, action: #selector(self.didEndEdit), for: .editingDidEnd)
        cell.passwordTextField.addTarget(self, action: #selector(self.didEndEdit), for: .editingDidEnd)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 600)
    }
    
    lazy var offset_HeaderStop:CGFloat = 250 // At this offset the Header stops its transformations
    let offset_B_LabelHeader:CGFloat = 140 // At this offset the Black label reaches the Header
    let distance_W_LabelHeader:CGFloat = 100 // The distance between the bottom of the Header and the top of the White Label
    var oldContentOffset = CGPoint.zero
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let offset = scrollView.contentOffset.y + 44
        let delta = scrollView.contentOffset.y - oldContentOffset.y
        print(delta)
        var headerTransform = CATransform3DIdentity
        
        // PULL DOWN -----------------
        print(offset)
        if offset < 0 {
            let headerScaleFactor:CGFloat = -(offset) / imageView.bounds.height
            let headerSizevariation = ((imageView.bounds.height * (1.0 + headerScaleFactor)) - imageView.bounds.height)/2.0
            headerTransform = CATransform3DTranslate(headerTransform, 0, headerSizevariation, 0)
            headerTransform = CATransform3DScale(headerTransform, 1.0 + headerScaleFactor, 1.0 + headerScaleFactor, 0)
            imageView.layer.transform = headerTransform
            imageView.alpha -= delta / 105
        }
        else if offset > 0 {
            imageView.alpha -= delta / 105
        }
        print(imageView.alpha)
        imageView.layer.transform = headerTransform
        oldContentOffset = scrollView.contentOffset
    }
}

extension LoginController: AWSCognitoIdentityPasswordAuthentication {
    
    public func getDetails(_ authenticationInput: AWSCognitoIdentityPasswordAuthenticationInput, passwordAuthenticationCompletionSource: AWSTaskCompletionSource<AWSCognitoIdentityPasswordAuthenticationDetails>) {
        self.passwordAuthenticationCompletion = passwordAuthenticationCompletionSource
        DispatchQueue.main.async {
            let indexPath = IndexPath(item: 0, section: 0);
            if let cell = self.collectionView?.cellForItem(at: indexPath) as? LoginCell
            {
                if (cell.emailTextField.text == nil) {
                    cell.emailTextField.text = authenticationInput.lastKnownUsername
                }
            }
        }
    }
    
    public func didCompleteStepWithError(_ error: Error?) {
        DispatchQueue.main.async {
            if error != nil {
                let alertController = UIAlertController(title: "Cannot Login",
                                                        message: (error! as NSError).userInfo["message"] as? String,
                                                        preferredStyle: .alert)
                let retryAction = UIAlertAction(title: "Retry", style: .default, handler: nil)
                alertController.addAction(retryAction)
                
                self.present(alertController, animated: true, completion:  nil)
            } else {
                print("its chill")
                let rootViewController = UIApplication.shared.keyWindow?.rootViewController
                guard let mainNavigationController = rootViewController as? MainNavigationController else { return }
                
                mainNavigationController.viewControllers = [CustomTabBarController()]
                
                UserDefaults.standard.set(true, forKey: "isLoggedIn")
                UserDefaults.standard.synchronize()
                
                self.dismiss(animated: true, completion: {
                    let indexPath = IndexPath(item: 0, section: 0);
                    if let cell = self.collectionView?.cellForItem(at: indexPath) as? LoginCell
                    {
                        cell.emailTextField.text = nil
                        cell.passwordTextField.text = nil
                    }
                })
            }
        }
    }
    
}

