//
//  SignupController.swift
//  Fantasy App
//
//  Created by Chad on 8/7/18.
//  Copyright © 2018 Chad. All rights reserved.
//

import UIKit
import AWSCognitoIdentityProvider

class SignupCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super .init(frame: frame)
        setupViews()
    }
    
    let image: UIImageView = {
        let image = UIImageView()
        image.frame = CGRect(x: 0, y: 0, width: 200, height: 35)
        image.contentMode = UIViewContentMode.scaleAspectFit
        image.translatesAutoresizingMaskIntoConstraints = false
        image.clipsToBounds = true
        return image
    }()
    
    let welcomeLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Play Tournament Fantasy Today!"
        return label
    }()
    
    let loginInstructions: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 15)
        label.textColor = grayText
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Create an account to start playing"
        return label
    }()
    
    let firstPh: UILabel = {
        let label = UILabel()
        label.text = "First name"
        label.font = UIFont.systemFont(ofSize: 17)
        label.textColor = placeholderGray
        label.translatesAutoresizingMaskIntoConstraints = false
        label.sizeToFit()
        return label
    }()
    
    let firstNameTextField: CustomTextField = {
        let tf = CustomTextField()
        tf.bottomPaddingPoints(0)
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.layer.borderWidth = 1
        tf.layer.borderColor = placeholderGray.cgColor
        tf.layer.cornerRadius = 4
        tf.clipsToBounds = true
        tf.autocorrectionType = .no
        tf.autocapitalizationType = .none
        tf.spellCheckingType = .no
        tf.keyboardType = .asciiCapable
        tf.tintColor = bluePrimary
        return tf
    }()
    
    let lastPh: UILabel = {
        let label = UILabel()
        label.text = "Last name"
        label.font = UIFont.systemFont(ofSize: 17)
        label.textColor = placeholderGray
        label.translatesAutoresizingMaskIntoConstraints = false
        label.sizeToFit()
        return label
    }()
    
    let lastNameTextField: CustomTextField = {
        let tf = CustomTextField()
        tf.bottomPaddingPoints(0)
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.layer.borderWidth = 1
        tf.layer.borderColor = placeholderGray.cgColor
        tf.layer.cornerRadius = 4
        tf.clipsToBounds = true
        tf.autocorrectionType = .no
        tf.autocapitalizationType = .none
        tf.spellCheckingType = .no
        tf.keyboardType = .asciiCapable
        tf.tintColor = bluePrimary
        return tf
    }()
    
    let emailPh: UILabel = {
        let label = UILabel()
        label.text = "Enter your email"
        label.font = UIFont.systemFont(ofSize: 17)
        label.textColor = placeholderGray
        label.translatesAutoresizingMaskIntoConstraints = false
        label.sizeToFit()
        return label
    }()
    
    let emailTextField: CustomTextField = {
        let tf = CustomTextField()
        tf.bottomPaddingPoints(0)
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.layer.borderWidth = 1
        tf.layer.borderColor = placeholderGray.cgColor
        tf.layer.cornerRadius = 4
        tf.clipsToBounds = true
        tf.autocorrectionType = .no
        tf.autocapitalizationType = .none
        tf.spellCheckingType = .no
        tf.keyboardType = .asciiCapable
        tf.tintColor = bluePrimary
        return tf
    }()
    
    let passPh: UILabel = {
        let label = UILabel()
        label.text = "Create password"
        label.font = UIFont.systemFont(ofSize: 17)
        label.textColor = placeholderGray
        label.translatesAutoresizingMaskIntoConstraints = false
        label.sizeToFit()
        return label
    }()
    
    let passwordTextField: CustomTextField = {
        let tf = CustomTextField()
        tf.bottomPaddingPoints(0)
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.layer.borderWidth = 1
        tf.layer.borderColor = placeholderGray.cgColor
        tf.layer.cornerRadius = 4
        tf.clipsToBounds = true
        tf.autocorrectionType = .no
        tf.autocapitalizationType = .none
        tf.spellCheckingType = .no
        tf.isSecureTextEntry = true
        tf.tintColor = bluePrimary
        return tf
    }()
    
    let signupButton: UIButton = {
        let button = UIButton()
        button.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        button.setTitle("Get started", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = bluePrimary
        button.layer.cornerRadius = 4
        button.clipsToBounds = true
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let topLoginButton: UIButton = {
        let button = UIButton()
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        button.setTitle("Log in", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = bluePrimary
        button.layer.cornerRadius = 4
        button.clipsToBounds = true
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let facebookButton: UIButton = {
        let button = UIButton()
        button.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        button.setTitle("Continue with Facebook", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = UIColor(red: 67/255, green: 105/255, blue: 176/255, alpha: 1)
        button.layer.cornerRadius = 4
        button.clipsToBounds = true
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let separatorLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = .gray
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "or"
        return label
    }()
    
    let leftSeparatorView: UIView = {
        let view = UIView()
        view.backgroundColor = lightGray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let rightSeparatorView: UIView = {
        let view = UIView()
        view.backgroundColor = lightGray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let loginButton: UIButton = {
        let button = UIButton()
        let s1 = "Already have an account? "
        let s1Attributes : [NSAttributedStringKey : Any] = [NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue) : UIFont.systemFont(ofSize: 14, weight: .regular), NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue) : UIColor.gray]
        let attributedString = NSMutableAttributedString(string: s1, attributes: s1Attributes)
        
        let s2 = "Log in"
        let s2Attributes : [NSAttributedStringKey : Any] = [NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue) : UIFont.systemFont(ofSize: 14, weight: .semibold), NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue) : bluePrimary]
        let s2String = NSMutableAttributedString(string: s2, attributes: s2Attributes)
        attributedString.append(s2String)
        button.setAttributedTitle(attributedString, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let termsButton: UIButton = {
        let button = UIButton()
        
        let s1 = "By creating an account, you agree to our \n"
        let s1Attributes : [NSAttributedStringKey : Any] = [NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue) : UIFont.systemFont(ofSize: 14, weight: .regular), NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue) : UIColor.gray]
        let attributedString = NSMutableAttributedString(string: s1, attributes: s1Attributes)
        
        let s2 = "Terms of Service "
        let s2Attributes : [NSAttributedStringKey : Any] = [NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue) : UIFont.systemFont(ofSize: 14, weight: .semibold), NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue) : bluePrimary]
        let s2String = NSMutableAttributedString(string: s2, attributes: s2Attributes)
        
        let s3 = "and "
        let s3Attributes : [NSAttributedStringKey : Any] = [NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue) : UIFont.systemFont(ofSize: 14, weight: .regular), NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue) : UIColor.gray]
        let s3String = NSMutableAttributedString(string: s3, attributes: s3Attributes)
        
        let s4 = "Privacy Policy"
        let s4Attributes : [NSAttributedStringKey : Any] = [NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue) : UIFont.systemFont(ofSize: 14, weight: .semibold), NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue) : bluePrimary]
        let s4String = NSMutableAttributedString(string: s4, attributes: s4Attributes)
        
        attributedString.append(s2String)
        attributedString.append(s3String)
        attributedString.append(s4String)
        button.setAttributedTitle(attributedString, for: .normal)
        button.titleLabel?.numberOfLines = 0
        button.titleLabel?.textAlignment = .center
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let bottomSeparatorView: UIView = {
        let view = UIView()
        view.backgroundColor = lightGray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    var firstPhCenter: NSLayoutConstraint?
    var firstPhTop: NSLayoutConstraint?
    var lastPhCenter: NSLayoutConstraint?
    var lastPhTop: NSLayoutConstraint?
    var emailPhCenter: NSLayoutConstraint?
    var emailPhTop: NSLayoutConstraint?
    var passwordPhCenter: NSLayoutConstraint?
    var passwordPhTop: NSLayoutConstraint?
    
    func setupViews() {
        addSubview(image)
        image.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        image.topAnchor.constraint(equalTo: self.topAnchor, constant: 125 - UIApplication.shared.statusBarFrame.height).isActive = true
        image.heightAnchor.constraint(equalToConstant: 35).isActive = true
        image.image = UIImage(named: "instacart")
        
        addSubview(welcomeLabel)
        welcomeLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        welcomeLabel.topAnchor.constraint(equalTo: image.bottomAnchor, constant: 16).isActive = true
        
        addSubview(loginInstructions)
        loginInstructions.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        loginInstructions.topAnchor.constraint(equalTo: welcomeLabel.bottomAnchor, constant: 6).isActive = true
        
        addSubview(firstNameTextField)
        firstNameTextField.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16).isActive = true
        firstNameTextField.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -self.frame.width / 2 - 4).isActive = true
        firstNameTextField.topAnchor.constraint(equalTo: loginInstructions.bottomAnchor, constant: 12).isActive = true
        firstNameTextField.heightAnchor.constraint(equalToConstant: 54).isActive = true
        
        addSubview(firstPh)
        firstPh.leftAnchor.constraint(equalTo: firstNameTextField.leftAnchor, constant: 8).isActive = true
        firstPhCenter = firstPh.centerYAnchor.constraint(equalTo: firstNameTextField.centerYAnchor)
        firstPhCenter?.isActive = true
        firstPhTop = firstPh.topAnchor.constraint(equalTo: firstNameTextField.topAnchor, constant: 6)
        firstPhTop?.isActive = false
        
        addSubview(lastNameTextField)
        lastNameTextField.leftAnchor.constraint(equalTo: firstNameTextField.rightAnchor, constant: 8).isActive = true
        lastNameTextField.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16).isActive = true
        lastNameTextField.topAnchor.constraint(equalTo: loginInstructions.bottomAnchor, constant: 12).isActive = true
        lastNameTextField.heightAnchor.constraint(equalToConstant: 54).isActive = true
        
        addSubview(lastPh)
        lastPh.leftAnchor.constraint(equalTo: lastNameTextField.leftAnchor, constant: 8).isActive = true
        lastPhCenter = lastPh.centerYAnchor.constraint(equalTo: lastNameTextField.centerYAnchor)
        lastPhCenter?.isActive = true
        lastPhTop = lastPh.topAnchor.constraint(equalTo: lastNameTextField.topAnchor, constant: 6)
        lastPhTop?.isActive = false
        
        addSubview(emailTextField)
        emailTextField.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16).isActive = true
        emailTextField.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16).isActive = true
        emailTextField.topAnchor.constraint(equalTo: firstNameTextField.bottomAnchor, constant: 8).isActive = true
        emailTextField.heightAnchor.constraint(equalToConstant: 54).isActive = true
        
        addSubview(emailPh)
        emailPh.leftAnchor.constraint(equalTo: emailTextField.leftAnchor, constant: 8).isActive = true
        emailPhCenter = emailPh.centerYAnchor.constraint(equalTo: emailTextField.centerYAnchor)
        emailPhCenter?.isActive = true
        emailPhTop = emailPh.topAnchor.constraint(equalTo: emailTextField.topAnchor, constant: 6)
        emailPhTop?.isActive = false
        
        addSubview(passwordTextField)
        passwordTextField.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16).isActive = true
        passwordTextField.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16).isActive = true
        passwordTextField.topAnchor.constraint(equalTo: emailTextField.bottomAnchor, constant: 8).isActive = true
        passwordTextField.heightAnchor.constraint(equalToConstant: 54).isActive = true
        
        addSubview(passPh)
        passPh.leftAnchor.constraint(equalTo: passwordTextField.leftAnchor, constant: 8).isActive = true
        passwordPhCenter = passPh.centerYAnchor.constraint(equalTo: passwordTextField.centerYAnchor)
        passwordPhCenter?.isActive = true
        passwordPhTop = passPh.topAnchor.constraint(equalTo: passwordTextField.topAnchor, constant: 6)
        passwordPhTop?.isActive = false
        
        addSubview(signupButton)
        signupButton.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16).isActive = true
        signupButton.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16).isActive = true
        signupButton.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: 8).isActive = true
        signupButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        addSubview(separatorLabel)
        separatorLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        separatorLabel.topAnchor.constraint(equalTo: signupButton.bottomAnchor, constant: 12).isActive = true
        
        addSubview(leftSeparatorView)
        leftSeparatorView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 64).isActive = true
        leftSeparatorView.rightAnchor.constraint(equalTo: separatorLabel.leftAnchor, constant: -12).isActive = true
        leftSeparatorView.centerYAnchor.constraint(equalTo: separatorLabel.centerYAnchor).isActive = true
        leftSeparatorView.heightAnchor.constraint(equalToConstant: 0.25).isActive = true
        
        addSubview(rightSeparatorView)
        rightSeparatorView.leftAnchor.constraint(equalTo: separatorLabel.rightAnchor, constant: 12).isActive = true
        rightSeparatorView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -64).isActive = true
        rightSeparatorView.centerYAnchor.constraint(equalTo: separatorLabel.centerYAnchor).isActive = true
        rightSeparatorView.heightAnchor.constraint(equalToConstant: 0.25).isActive = true
        
        addSubview(facebookButton)
        facebookButton.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16).isActive = true
        facebookButton.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16).isActive = true
        facebookButton.topAnchor.constraint(equalTo: separatorLabel.bottomAnchor, constant: 12).isActive = true
        facebookButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        addSubview(loginButton)
        loginButton.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        loginButton.topAnchor.constraint(equalTo: facebookButton.bottomAnchor, constant: 24).isActive = true
        
        addSubview(bottomSeparatorView)
        bottomSeparatorView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16).isActive = true
        bottomSeparatorView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16).isActive = true
        bottomSeparatorView.topAnchor.constraint(equalTo: loginButton.bottomAnchor, constant: 8).isActive = true
        bottomSeparatorView.heightAnchor.constraint(equalToConstant: 0.25).isActive = true
        
        addSubview(termsButton)
        termsButton.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        termsButton.topAnchor.constraint(equalTo: bottomSeparatorView.bottomAnchor, constant: 8).isActive = true
        
        addSubview(topLoginButton)
        topLoginButton.topAnchor.constraint(equalTo: self.topAnchor, constant: 12).isActive = true
        topLoginButton.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16).isActive = true
        topLoginButton.widthAnchor.constraint(equalToConstant: 66).isActive = true
        topLoginButton.heightAnchor.constraint(equalToConstant: 26).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class SignupController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    private let signupCellId = "signupCellId"
    var user: AWSCognitoIdentityUser?
    var codeDeliveryDetails:AWSCognitoIdentityProviderCodeDeliveryDetailsType?
    
    let whiteView: UIView = {
        let v = UIView()
        v.backgroundColor = .white
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.delegate = self
        collectionView?.dataSource = self
        collectionView?.showsVerticalScrollIndicator = false
        collectionView?.isScrollEnabled = true
        collectionView?.alwaysBounceVertical = true
        setupCollectionView()
        configureNavigationBar()
        setupHideKeyboardOnTap()
    }
    
    func setupCollectionView() {
        collectionView?.backgroundColor = .clear
        collectionView?.register(SignupCell.self, forCellWithReuseIdentifier: signupCellId)
        
        view.insertSubview(whiteView, belowSubview: collectionView!)
        whiteView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        configureNavigationBar()
        setupPlaceholderText()
    }
    
    func setupPlaceholderText() {
        let indexPath = IndexPath(item: 0, section: 0)
        if let cell = collectionView?.cellForItem(at: indexPath) as? SignupCell
        {
            cell.firstPhCenter?.isActive = true
            cell.firstPhTop?.isActive = false
            cell.firstPh.textColor = placeholderGray
            cell.firstPh.font = UIFont.systemFont(ofSize: 17)
            cell.firstNameTextField.contentVerticalAlignment = .center
            cell.firstNameTextField.bottomPaddingPoints(0)
            
            cell.lastPhCenter?.isActive = true
            cell.lastPhTop?.isActive = false
            cell.lastPh.textColor = placeholderGray
            cell.lastPh.font = UIFont.systemFont(ofSize: 17)
            cell.lastNameTextField.contentVerticalAlignment = .center
            cell.lastNameTextField.bottomPaddingPoints(0)
            
            cell.emailPhCenter?.isActive = true
            cell.emailPhTop?.isActive = false
            cell.emailPh.textColor = placeholderGray
            cell.emailPh.font = UIFont.systemFont(ofSize: 17)
            cell.emailTextField.contentVerticalAlignment = .center
            cell.emailTextField.bottomPaddingPoints(0)
            
            cell.passwordPhCenter?.isActive = true
            cell.passwordPhTop?.isActive = false
            cell.passPh.textColor = placeholderGray
            cell.passPh.font = UIFont.systemFont(ofSize: 17)
            cell.passwordTextField.contentVerticalAlignment = .center
            cell.passwordTextField.bottomPaddingPoints(0)
        }
    }
    
    @objc func inputDidChange(_ sender:AnyObject) {
        let indexPath = IndexPath(item: 0, section: 0);
        if let cell = collectionView?.cellForItem(at: indexPath) as? SignupCell
            {
                if (cell.emailTextField.text?.count)! > 0 && cell.emailTextField.isEditing {
                    UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                        cell.emailPhCenter?.isActive = false
                        cell.emailPhTop?.isActive = true
                        cell.emailPh.textColor = bluePrimary
                        cell.emailPh.font = UIFont.systemFont(ofSize: 14)
                        cell.layoutIfNeeded()
                    })
                    cell.emailTextField.contentVerticalAlignment = .bottom
                    cell.emailTextField.bottomPaddingPoints(6)
                    
                } else if (cell.emailTextField.text?.count)! == 0{
                    UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                        cell.emailPhCenter?.isActive = true
                        cell.emailPhTop?.isActive = false
                        cell.emailPh.textColor = placeholderGray
                        cell.emailPh.font = UIFont.systemFont(ofSize: 17)
                        cell.layoutIfNeeded()
                    })
                    cell.emailTextField.contentVerticalAlignment = .center
                    cell.emailTextField.bottomPaddingPoints(0)
                }
                
                if (cell.firstNameTextField.text?.count)! > 0 && cell.firstNameTextField.isEditing {
                    UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                        cell.firstPhCenter?.isActive = false
                        cell.firstPhTop?.isActive = true
                        cell.firstPh.textColor = bluePrimary
                        cell.firstPh.font = UIFont.systemFont(ofSize: 14)
                        cell.layoutIfNeeded()
                    })
                    cell.firstNameTextField.contentVerticalAlignment = .bottom
                    cell.firstNameTextField.bottomPaddingPoints(6)
                    
                } else if (cell.firstNameTextField.text?.count)! == 0{
                    UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                        cell.firstPhCenter?.isActive = true
                        cell.firstPhTop?.isActive = false
                        cell.firstPh.textColor = placeholderGray
                        cell.firstPh.font = UIFont.systemFont(ofSize: 17)
                        cell.layoutIfNeeded()
                    })
                    cell.firstNameTextField.contentVerticalAlignment = .center
                    cell.firstNameTextField.bottomPaddingPoints(0)
                }
                
                if (cell.lastNameTextField.text?.count)! > 0 && cell.lastNameTextField.isEditing {
                    UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                        cell.lastPhCenter?.isActive = false
                        cell.lastPhTop?.isActive = true
                        cell.lastPh.textColor = bluePrimary
                        cell.lastPh.font = UIFont.systemFont(ofSize: 14)
                        cell.layoutIfNeeded()
                    })
                    cell.lastNameTextField.contentVerticalAlignment = .bottom
                    cell.lastNameTextField.bottomPaddingPoints(6)
                    
                } else if (cell.lastNameTextField.text?.count)! == 0{
                    UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                        cell.lastPhCenter?.isActive = true
                        cell.lastPhTop?.isActive = false
                        cell.lastPh.textColor = placeholderGray
                        cell.lastPh.font = UIFont.systemFont(ofSize: 17)
                        cell.layoutIfNeeded()
                    })
                    cell.lastNameTextField.contentVerticalAlignment = .center
                    cell.lastNameTextField.bottomPaddingPoints(0)
                }
                
                if (cell.passwordTextField.text?.count)! > 0 && cell.passwordTextField.isEditing {
                    UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                        cell.passwordPhCenter?.isActive = false
                        cell.passwordPhTop?.isActive = true
                        cell.passPh.textColor = bluePrimary
                        cell.passPh.font = UIFont.systemFont(ofSize: 14)
                        cell.layoutIfNeeded()
                    })
                    cell.passwordTextField.contentVerticalAlignment = .bottom
                    cell.passwordTextField.bottomPaddingPoints(6)
                    
                } else if (cell.passwordTextField.text?.count)! == 0{
                    UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                        cell.passwordPhCenter?.isActive = true
                        cell.passwordPhTop?.isActive = false
                        cell.passPh.textColor = placeholderGray
                        cell.passPh.font = UIFont.systemFont(ofSize: 17)
                        cell.layoutIfNeeded()
                    })
                    cell.passwordTextField.contentVerticalAlignment = .center
                    cell.passwordTextField.bottomPaddingPoints(0)
                }
            
            if(cell.firstNameTextField.text == nil || cell.lastNameTextField.text == nil) {
                cell.signupButton.isEnabled = false
                cell.signupButton.alpha = 0.75
                return
            }
            if(cell.emailTextField.text == nil) {
                cell.signupButton.isEnabled = false
                cell.signupButton.alpha = 0.75
                return
            }
            if((cell.passwordTextField.text?.count)! < 6) {
                cell.signupButton.isEnabled = false
                cell.signupButton.alpha = 0.75
                return
            }
                cell.signupButton.alpha = 1
                cell.signupButton.isEnabled = ((cell.passwordTextField.text?.count)! > 5)
            }
    }
    
    @objc func didBeginEdit() {
        let indexPath = IndexPath(item: 0, section: 0);
        if let cell = collectionView?.cellForItem(at: indexPath) as? SignupCell
        {
            if cell.firstNameTextField.isEditing {
                cell.firstNameTextField.layer.borderColor = bluePrimary.cgColor
                if (cell.firstNameTextField.text?.count)! > 0 {
                    cell.firstPh.textColor = bluePrimary
                }
            }
            
            if cell.lastNameTextField.isEditing {
                cell.lastNameTextField.layer.borderColor = bluePrimary.cgColor
                if (cell.lastNameTextField.text?.count)! > 0 {
                    cell.lastPh.textColor = bluePrimary
                }
            }
            
            if cell.emailTextField.isEditing {
                cell.emailTextField.layer.borderColor = bluePrimary.cgColor
                if (cell.emailTextField.text?.count)! > 0 {
                    cell.emailPh.textColor = bluePrimary
                }
            }
            
            if cell.passwordTextField.isEditing {
                cell.passwordTextField.layer.borderColor = bluePrimary.cgColor
                if (cell.passwordTextField.text?.count)! > 0 {
                    cell.passPh.textColor = bluePrimary
                }
            }
        }
    }
    
    @objc func didEndEdit() {
        let indexPath = IndexPath(item: 0, section: 0);
        if let cell = collectionView?.cellForItem(at: indexPath) as? SignupCell
        {
            cell.firstNameTextField.layer.borderColor = placeholderGray.cgColor
            cell.firstPh.textColor = placeholderGray
            cell.lastNameTextField.layer.borderColor = placeholderGray.cgColor
            cell.lastPh.textColor = placeholderGray
            cell.emailTextField.layer.borderColor = placeholderGray.cgColor
            cell.emailPh.textColor = placeholderGray
            cell.passwordTextField.layer.borderColor = placeholderGray.cgColor
            cell.passPh.textColor = placeholderGray
        }
    }
    
    func configureNavigationBar() {
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        UIApplication.shared.statusBarStyle = .default
    }
    
    func setupHideKeyboardOnTap() {
        self.view.addGestureRecognizer(self.endEditingRecognizer())
        self.navigationController?.navigationBar.addGestureRecognizer(self.endEditingRecognizer())
    }
    
    /// Dismisses the keyboard from self.view
    private func endEditingRecognizer() -> UIGestureRecognizer {
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(self.view.endEditing(_:)))
        tap.cancelsTouchesInView = false
        return tap
    }
    
    @objc func signupPressed() {
        print("Start signup process")
        let indexPath = IndexPath(item: 0, section: 0)
        if let cell = collectionView?.cellForItem(at: indexPath) as? SignupCell
        {
            let userPool = AppDelegate.defaultUserPool()
            let emailAttribute = AWSCognitoIdentityUserAttributeType(name: "email", value: cell.emailTextField.text!.lowercased())
            let firstNameAttribute = AWSCognitoIdentityUserAttributeType(name: "given_name", value: cell.firstNameTextField.text!)
            let lastNameAttribute = AWSCognitoIdentityUserAttributeType(name: "family_name", value: cell.lastNameTextField.text!)
            let attributes:[AWSCognitoIdentityUserAttributeType] = [emailAttribute, firstNameAttribute, lastNameAttribute]
            userPool.signUp(cell.emailTextField.text!.lowercased(), password: cell.passwordTextField.text!, userAttributes: attributes, validationData: nil)
                .continueWith { (response) -> Any? in
                    if response.error != nil {
                        // Error in the Signup Process
                        let alert = UIAlertController(title: "Error", message: (response.error! as NSError).userInfo["message"] as? String, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler:nil))
                        self.present(alert, animated: true, completion: nil)
                    } else {
                        self.user = response.result!.user
                        // Does user need confirmation?
                        if (response.result?.userConfirmed?.intValue != AWSCognitoIdentityUserStatus.confirmed.rawValue) {
                            // User needs confirmation, so we need to proceed to the verify view controller
                            DispatchQueue.main.async {
                                self.codeDeliveryDetails = response.result?.codeDeliveryDetails
                                print("verification needed")
                                let layout = UICollectionViewFlowLayout()
                                let verificationController = VerifyController(collectionViewLayout: layout)
                                verificationController.codeDeliveryDetails = self.codeDeliveryDetails
                                verificationController.user = self.user!
                                verificationController.u = cell.emailTextField.text!.lowercased()
                                verificationController.p = cell.passwordTextField.text!
                                self.navigationController?.pushViewController(verificationController, animated: true)
                                self.presentingViewController?.view.isHidden = true
                            }
                        } else {
                            // User signed up but does not need confirmation.  This should rarely happen (if ever).
                            DispatchQueue.main.async {
//                                self.presentingViewController?.dismiss(animated: true, completion: nil)
                            }
                        }
                    }
                    return nil
            }
        }
    }
    
    @objc func signupWithFacebook() {
        print("Signup with Facebook")
    }
    
    @objc func loginButtonFunc() {
        print("Push login controller")
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func termsAndConditionsPressed() {
        print("Show terms and conditions")
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 10, 0)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: signupCellId, for: indexPath) as! SignupCell
        cell.backgroundColor = .clear
        cell.signupButton.addTarget(self, action: #selector(self.signupPressed), for: .touchUpInside)
        cell.facebookButton.addTarget(self, action: #selector(self.signupWithFacebook), for: .touchUpInside)
        cell.topLoginButton.addTarget(self, action: #selector(self.loginButtonFunc), for: .touchUpInside)
        cell.loginButton.addTarget(self, action: #selector(self.loginButtonFunc), for: .touchUpInside)
        cell.termsButton.addTarget(self, action: #selector(self.termsAndConditionsPressed), for: .touchUpInside)
        
        cell.signupButton.isEnabled = false
        cell.signupButton.alpha = 0.75
        cell.firstNameTextField.addTarget(self, action: #selector(inputDidChange(_:)), for: .editingChanged)
        cell.lastNameTextField.addTarget(self, action: #selector(inputDidChange(_:)), for: .editingChanged)
        cell.emailTextField.addTarget(self, action: #selector(inputDidChange(_:)), for: .editingChanged)
        cell.passwordTextField.addTarget(self, action: #selector(inputDidChange(_:)), for: .editingChanged)
        
        cell.firstNameTextField.addTarget(self, action: #selector(self.didBeginEdit), for: .editingDidBegin)
        cell.lastNameTextField.addTarget(self, action: #selector(self.didBeginEdit), for: .editingDidBegin)
        cell.emailTextField.addTarget(self, action: #selector(self.didBeginEdit), for: .editingDidBegin)
        cell.passwordTextField.addTarget(self, action: #selector(self.didBeginEdit), for: .editingDidBegin)
        
        cell.firstNameTextField.addTarget(self, action: #selector(self.didEndEdit), for: .editingDidEnd)
        cell.lastNameTextField.addTarget(self, action: #selector(self.didEndEdit), for: .editingDidEnd)
        cell.emailTextField.addTarget(self, action: #selector(self.didEndEdit), for: .editingDidEnd)
        cell.passwordTextField.addTarget(self, action: #selector(self.didEndEdit), for: .editingDidEnd)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 600)
    }
}
