//
//  CustomMyLeaguesHeader.swift
//  Fantasy App
//
//  Created by Chad on 7/23/18.
//  Copyright © 2018 Chad. All rights reserved.
//

import UIKit

class CustomMyLeaguesHeader: UICollectionViewCell {
    
    let date = Date()
    let formatter = DateFormatter()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupViews()
    }
    
    let dateLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        label.textColor = .gray
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let headerLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 32, weight: .bold)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let separatorLine: UIView = {
       let view = UIView()
        view.backgroundColor = lightGray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let optionsButton: UIButton = {
        let button = UIButton()
        let image = UIImage(named: "moreFilled")?.withRenderingMode(.alwaysTemplate)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.imageView?.contentMode = .scaleAspectFit
        button.setBackgroundImage(image, for: .normal)
        button.tintColor = .darkGray
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        return button
    }()
    
    func setupViews() {
        formatter.dateFormat = "EEEE, MMMM dd"
        let result = formatter.string(from: date).uppercased()
        
        addSubview(headerLabel)
        headerLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 44).isActive = true
        headerLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 14).isActive = true
        
        addSubview(dateLabel)
        dateLabel.bottomAnchor.constraint(equalTo: headerLabel.topAnchor, constant: -2).isActive = true
        dateLabel.leftAnchor.constraint(equalTo: headerLabel.leftAnchor).isActive = true
        //dateLabel.text = result
        
        addSubview(separatorLine)
        separatorLine.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        separatorLine.leftAnchor.constraint(equalTo: headerLabel.leftAnchor).isActive = true
        separatorLine.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -14).isActive = true
        separatorLine.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
    }
    
    @objc func buttonAction(sender: UIButton!) {
        print("Button tapped")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
