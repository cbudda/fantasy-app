//
//  CustomLeagueCell.swift
//  Fantasy App
//
//  Created by Chad on 7/24/18.
//  Copyright © 2018 Chad. All rights reserved.
//

import UIKit

class LeagueCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super .init(frame: frame)
        setupViews()
    }
    
    let leagueImage: UIImageView = {
        let image = UIImageView()
        image.frame = CGRect(x: 16, y: 0, width: 50, height: 50)
        image.contentMode = UIViewContentMode.scaleAspectFit
        image.translatesAutoresizingMaskIntoConstraints = false
        image.clipsToBounds = true
        image.backgroundColor = lightGray
        return image
    }()
    
    let fantasyTeamName: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        label.translatesAutoresizingMaskIntoConstraints = false
        //label.backgroundColor = lightGray
        label.text = "Fantasy Team Name"
        return label
    }()
    
    let separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = lightGray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let bottomSeparatorView: UIView = {
        let view = UIView()
        view.backgroundColor = lightGray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let fantasyLeagueName: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = grayText
        label.text = "League Name"
        return label
    }()
    
    let rankLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        label.translatesAutoresizingMaskIntoConstraints = false
        //label.backgroundColor = lightGray
        label.text = "Rank:"
        return label
    }()
    
    let rank: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        //label.backgroundColor = lightGray
        label.textColor = grayText
        label.text = "10"
        return label
    }()
    
    let pointsLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        label.translatesAutoresizingMaskIntoConstraints = false
        //label.backgroundColor = lightGray
        label.text = "Pts:"
        return label
    }()
    
    let points: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        //label.backgroundColor = lightGray
        label.textColor = grayText
        label.text = "777"
        return label
    }()
    
    let gamesLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        label.translatesAutoresizingMaskIntoConstraints = false
        //label.backgroundColor = lightGray
        label.text = "GP:"
        return label
    }()
    
    let games: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        //label.backgroundColor = lightGray
        label.textColor = grayText
        label.text = "33"
        return label
    }()
    
    let playersLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        label.translatesAutoresizingMaskIntoConstraints = false
        //label.backgroundColor = lightGray
        label.text = "Players:"
        return label
    }()
    
    let players: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        //label.backgroundColor = lightGray
        label.textColor = grayText
        label.text = "8"
        return label
    }()
    
    let stackView: UIStackView = {
        let stack = UIStackView()
        stack.translatesAutoresizingMaskIntoConstraints = false
        
        let renameLabel: UIButton = {
            let label = UIButton()
            let image = UIImage(named: "edit")?.withRenderingMode(.alwaysTemplate)
            label.centerTextAndImage(spacing: 8)
            label.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: .medium)
            label.setTitle("Rename", for: .normal)
            label.setTitleColor(grayText, for: .normal)
            label.setTitleColor(.gray, for: .highlighted)
            label.setTitleColor(.gray, for: .selected)
            
            label.setImage(image, for: .normal)
            label.tintColor = grayText
            return label
        }()
        
        let shareLabel: UIButton = {
            let label = UIButton()
            let image = UIImage(named: "shareIcon")?.withRenderingMode(.alwaysTemplate)
            label.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: .medium)
            label.centerTextAndImage(spacing: 8)
            label.setTitle("Share", for: .normal)
            label.setTitleColor(grayText, for: .normal)
            label.setTitleColor(.gray, for: .highlighted)
            label.setTitleColor(.gray, for: .selected)
            label.imageView?.contentMode = .scaleAspectFit
            label.setImage(image, for: .normal)
            label.tintColor = grayText
            return label
        }()
        
        stack.addArrangedSubview(renameLabel)
        stack.addArrangedSubview(shareLabel)
        stack.distribution = .fillEqually
//        stack.setCustomSpacing(48, after: renameLabel)
        
        return stack
    }()
    
    let adminDot: UIImageView = {
        let image = UIImageView()
        image.frame = CGRect(x: 0, y: 0, width: 4, height: 4)
        image.contentMode = UIViewContentMode.scaleAspectFit
        image.translatesAutoresizingMaskIntoConstraints = false
        image.clipsToBounds = true
        image.image = UIImage(named: "dot")
        return image
    }()
    
    let adminShield: UIImageView = {
        let image = UIImageView()
        image.frame = CGRect(x: 0, y: 0, width: 12.5, height: 12.5)
        image.contentMode = UIViewContentMode.scaleAspectFit
        image.translatesAutoresizingMaskIntoConstraints = false
        image.clipsToBounds = true
        image.image = UIImage(named: "shield")
        return image
    }()
    
    let adminLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = grayText
        label.text = "Admin"
        return label
    }()
    
    func setupViews() {
        addSubview(leagueImage)
        leagueImage.topAnchor.constraint(equalTo: self.topAnchor, constant: 14).isActive = true
        leagueImage.widthAnchor.constraint(equalToConstant: 50).isActive = true
        leagueImage.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 12).isActive = true
        leagueImage.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        addSubview(fantasyLeagueName)
        addSubview(fantasyTeamName)
        fantasyTeamName.bottomAnchor.constraint(equalTo: fantasyLeagueName.topAnchor, constant: -2.5).isActive = true
        fantasyTeamName.leftAnchor.constraint(equalTo: leagueImage.rightAnchor, constant: 12).isActive = true
        fantasyTeamName.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -12).isActive = true
        
        fantasyLeagueName.centerYAnchor.constraint(equalTo: leagueImage.centerYAnchor, constant: 0).isActive = true
        fantasyLeagueName.leftAnchor.constraint(equalTo: fantasyTeamName.leftAnchor).isActive = true
        //fantasyLeagueName.rightAnchor.constraint(equalTo: fantasyTeamName.rightAnchor).isActive = true
        
        addSubview(separatorView)
        separatorView.topAnchor.constraint(equalTo: leagueImage.bottomAnchor, constant: 16).isActive = true
        separatorView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16).isActive = true
        separatorView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16).isActive = true
        separatorView.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
        
        addSubview(rankLabel)
        rankLabel.topAnchor.constraint(equalTo: fantasyLeagueName.bottomAnchor, constant: 3.5).isActive = true
        rankLabel.leftAnchor.constraint(equalTo: fantasyTeamName.leftAnchor).isActive = true
        
        addSubview(rank)
        rank.topAnchor.constraint(equalTo: rankLabel.topAnchor).isActive = true
        rank.leftAnchor.constraint(equalTo: rankLabel.rightAnchor, constant: 6).isActive = true
        
        addSubview(pointsLabel)
        pointsLabel.topAnchor.constraint(equalTo: rankLabel.topAnchor).isActive = true
        pointsLabel.leftAnchor.constraint(equalTo: rank.rightAnchor, constant: 10).isActive = true
        
        addSubview(points)
        points.topAnchor.constraint(equalTo: rankLabel.topAnchor).isActive = true
        points.leftAnchor.constraint(equalTo: pointsLabel.rightAnchor, constant: 6).isActive = true
        
        addSubview(gamesLabel)
        gamesLabel.topAnchor.constraint(equalTo: rankLabel.topAnchor).isActive = true
        gamesLabel.leftAnchor.constraint(equalTo: points.rightAnchor, constant: 10).isActive = true
        
        addSubview(games)
        games.topAnchor.constraint(equalTo: rankLabel.topAnchor).isActive = true
        games.leftAnchor.constraint(equalTo: gamesLabel.rightAnchor, constant: 6).isActive = true
        
        addSubview(playersLabel)
        playersLabel.topAnchor.constraint(equalTo: rankLabel.topAnchor).isActive = true
        playersLabel.leftAnchor.constraint(equalTo: games.rightAnchor, constant: 10).isActive = true
        
        addSubview(players)
        players.topAnchor.constraint(equalTo: rankLabel.topAnchor).isActive = true
        players.leftAnchor.constraint(equalTo: playersLabel.rightAnchor, constant: 6).isActive = true
        
        addSubview(stackView)
        stackView.topAnchor.constraint(equalTo: separatorView.bottomAnchor, constant: 8).isActive = true
        stackView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 12).isActive = true
        stackView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -12).isActive = true
        
        addSubview(adminDot)
        addSubview(adminLabel)
        addSubview(adminShield)
        adminDot.leftAnchor.constraint(equalTo: fantasyLeagueName.rightAnchor, constant: 6).isActive = true
        adminDot.centerYAnchor.constraint(equalTo: fantasyLeagueName.centerYAnchor).isActive = true
        adminDot.heightAnchor.constraint(equalToConstant: 4).isActive = true
        adminDot.widthAnchor.constraint(equalToConstant: 4).isActive = true
        
        adminLabel.leftAnchor.constraint(equalTo: adminShield.rightAnchor, constant: 2).isActive = true
        adminLabel.topAnchor.constraint(equalTo: fantasyLeagueName.topAnchor).isActive = true
        
        adminShield.leftAnchor.constraint(equalTo: adminDot.rightAnchor, constant: 6).isActive = true
        adminShield.centerYAnchor.constraint(equalTo: adminLabel.centerYAnchor).isActive = true
        adminShield.heightAnchor.constraint(equalToConstant: 12.5).isActive = true
        adminShield.widthAnchor.constraint(equalToConstant: 12.5).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
