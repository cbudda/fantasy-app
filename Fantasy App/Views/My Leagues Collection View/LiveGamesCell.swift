//
//  LiveGames.swift
//  Fantasy App
//
//  Created by Chad on 7/27/18.
//  Copyright © 2018 Chad. All rights reserved.
//

import UIKit

class LiveGamesCell: UICollectionViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    private let cellId = "gameCellId"
    let date = Date()
    let formatter = DateFormatter()
    
    override init(frame: CGRect) {
        super .init(frame: frame)
        setupViews()
    }
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        label.textColor = .gray
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let gamesCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.backgroundColor = .clear
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        
        return collectionView
    }()
    
    let separatorLine: UIView = {
        let view = UIView()
        view.backgroundColor = lightGray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    func setupViews() {
        backgroundColor = .clear
        formatter.dateFormat = "EEEE, MMMM d"
        let result = formatter.string(from: date).uppercased()
        
        addSubview(gamesCollectionView)
        addSubview(nameLabel)
        addSubview(separatorLine)
        
        nameLabel.text = result
        
        gamesCollectionView.dataSource = self
        gamesCollectionView.delegate = self
        
        gamesCollectionView.register(gameCell.self, forCellWithReuseIdentifier: cellId)
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-14-[v0]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["v0": nameLabel]))
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-14-[v0]-14-|", options: NSLayoutFormatOptions(), metrics: nil, views: ["v0": separatorLine]))
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[v0]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["v0": gamesCollectionView]))
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[nameLabel][v0][separatorLine(0.5)]|", options: NSLayoutFormatOptions(), metrics: nil, views: ["v0": gamesCollectionView, "nameLabel": nameLabel, "separatorLine": separatorLine]))
    
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    let awayTeam = [245, 52, 2306, 2350]
    let homeTeam = [130, 2250, 96, 2440]
    let awayScore = [72, 75, 61, 69]
    let homeScore = [99, 60, 58, 68]
    let awaySeed = [7, 9, 9, 11]
    let homeSeed = [3, 4, 5, 7]
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! gameCell
        
//        cell.teamLogo.kf.setImage(with: teamUrl)
//        cell.oppoTeamLogo.kf.setImage(with: oppUrl)
//        cell.teamSeed.text = playerGames["seed"].stringValue
//        cell.oppoTeamSeed.text = playerGames["oppoSeed"].stringValue
//        cell.teamScore.text = "\(playerGames["teamScore"].intValue)"
//        cell.oppoTeamScore.text = "\(playerGames["opponentScore"].intValue)"
//        cell.gameTime.text = playerGames["gameTime"].stringValue
        let homeURL = URL(string: "http://a.espncdn.com/combiner/i?img=/i/teamlogos/ncaa/500/\(homeTeam[indexPath.item]).png&h=150&w=150")
        let awayURL = URL(string: "http://a.espncdn.com/combiner/i?img=/i/teamlogos/ncaa/500/\(awayTeam[indexPath.item]).png&h=150&w=150")
        
        cell.teamLogo.kf.setImage(with: awayURL)
        cell.oppoTeamLogo.kf.setImage(with: homeURL)
        cell.teamSeed.text = String(awaySeed[indexPath.item])
        cell.oppoTeamSeed.text = String(homeSeed[indexPath.item])
        cell.teamScore.text = String(awayScore[indexPath.item])
        cell.oppoTeamScore.text = String(homeScore[indexPath.item])
        cell.gameTime.text = "Final"
        
        if awayScore[indexPath.row] > homeScore[indexPath.row] {
            cell.teamScore.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
            cell.oppoTeamScore.font = UIFont.systemFont(ofSize: 17)
        } else {
            cell.oppoTeamScore.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
            cell.teamScore.font = UIFont.systemFont(ofSize: 17)
        }
        
        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        cell.layer.shadowRadius = 5.0
        cell.layer.shadowOpacity = 0.15
        cell.layer.masksToBounds = false
        cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: cell.contentView.layer.cornerRadius).cgPath
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 131, height: 79)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 14, 0, 14)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class gameCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super .init(frame: frame)
        setupViews()
    }
    
    let teamLogo: UIImageView = {
        let image = UIImageView()
        image.frame = CGRect(x: 18, y: 0, width: 30, height: 30)
        image.contentMode = UIViewContentMode.scaleAspectFit
        image.translatesAutoresizingMaskIntoConstraints = false
        image.tintColor = .darkGray
        return image
    }()
    
    let oppoTeamLogo: UIImageView = {
        let oppoImage = UIImageView()
        oppoImage.frame = CGRect(x: 18, y: 0, width: 30, height: 30)
        oppoImage.contentMode = UIViewContentMode.scaleAspectFit
        oppoImage.translatesAutoresizingMaskIntoConstraints = false
        oppoImage.tintColor = .darkGray
        return oppoImage
    }()
    
    let teamSeed: UILabel = {
        let seedLabel = UILabel()
        seedLabel.font = UIFont.systemFont(ofSize: 8)
        seedLabel.translatesAutoresizingMaskIntoConstraints = false
        return seedLabel
    }()
    
    let oppoTeamSeed: UILabel = {
        let oppoSeedLabel = UILabel()
        oppoSeedLabel.font = UIFont.systemFont(ofSize: 8)
        oppoSeedLabel.translatesAutoresizingMaskIntoConstraints = false
        return oppoSeedLabel
    }()
    
    let teamScore: UILabel = {
        let teamScore = UILabel()
        teamScore.font = UIFont.systemFont(ofSize: 17)
        teamScore.translatesAutoresizingMaskIntoConstraints = false
        return teamScore
    }()
    
    let oppoTeamScore: UILabel = {
        let oppoTeamScore = UILabel()
        oppoTeamScore.font = UIFont.systemFont(ofSize: 17)
        oppoTeamScore.translatesAutoresizingMaskIntoConstraints = false
        return oppoTeamScore
    }()
    
    let gameTime: UILabel = {
        let gameTime = UILabel()
        gameTime.font = UIFont.systemFont(ofSize: 13)
        gameTime.translatesAutoresizingMaskIntoConstraints = false
        gameTime.numberOfLines = 0
        gameTime.textAlignment = .center
        return gameTime
    }()
    
    func setupViews() {
        addSubview(teamLogo)
        teamLogo.topAnchor.constraint(equalTo: self.topAnchor, constant: 8).isActive = true
        teamLogo.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 20).isActive = true
        teamLogo.widthAnchor.constraint(equalToConstant: 30).isActive = true
        teamLogo.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        addSubview(oppoTeamLogo)
        oppoTeamLogo.topAnchor.constraint(equalTo: teamLogo.bottomAnchor, constant: 3).isActive = true
        oppoTeamLogo.leftAnchor.constraint(equalTo: teamLogo.leftAnchor).isActive = true
        oppoTeamLogo.widthAnchor.constraint(equalToConstant: 30).isActive = true
        oppoTeamLogo.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        addSubview(teamSeed)
        teamSeed.topAnchor.constraint(equalTo: teamLogo.bottomAnchor, constant: -13).isActive = true
        teamSeed.rightAnchor.constraint(equalTo: teamLogo.leftAnchor, constant: -3).isActive = true
        
        addSubview(oppoTeamSeed)
        oppoTeamSeed.topAnchor.constraint(equalTo: oppoTeamLogo.bottomAnchor, constant: -14).isActive = true
        oppoTeamSeed.rightAnchor.constraint(equalTo: teamSeed.rightAnchor).isActive = true
        
        addSubview(teamScore)
        teamScore.centerYAnchor.constraint(equalTo: teamLogo.centerYAnchor, constant: 2).isActive = true
        teamScore.leftAnchor.constraint(equalTo: teamLogo.rightAnchor, constant: 8).isActive = true
        
        addSubview(oppoTeamScore)
        oppoTeamScore.centerYAnchor.constraint(equalTo: oppoTeamLogo.centerYAnchor, constant: -2).isActive = true
        oppoTeamScore.leftAnchor.constraint(equalTo: teamScore.leftAnchor).isActive = true
        
        addSubview(gameTime)
        gameTime.widthAnchor.constraint(equalToConstant: 36).isActive = true
        gameTime.leftAnchor.constraint(equalTo: teamLogo.rightAnchor, constant: 38).isActive = true
        gameTime.topAnchor.constraint(equalTo: teamLogo.topAnchor).isActive = true
        gameTime.bottomAnchor.constraint(equalTo: oppoTeamLogo.bottomAnchor).isActive = true
        
        backgroundColor = UIColor.white
        layer.borderColor = UIColor(red: 214/255, green: 214/255, blue: 214/255, alpha: 1).cgColor
        layer.borderWidth = 0.5
        layer.cornerRadius = 8
        clipsToBounds = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
