//
//  MyLeaguesVC.swift
//  Fantasy App
//
//  Created by Chad on 6/13/18.
//  Copyright © 2018 Chad. All rights reserved.
//

import UIKit

class MyLeaguesVC: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    private let leagueCellId = "LeagueCell"
    private let loadingCellId = "MyLeaguesLoadingCell"
    private let headerId = "MyLeagueHeader"
    private let liveGamesCell = "LiveGamesCell"
    
    let teamNames = ["Sweep The Leg, Grayson", "Bruce Pearl's BBQ", "Obi Wan Ginobli & Friends", "Thad Matta World Peace"]
    let leagueNames = ["Laker Legends", "#Billastrator", "One Time at Band Camp", "Aaron Craft's 10th Year"]
    let images = ["anchor", "bilas", "obiwan", "aaronCraft"]
    let ranks = [8, 1, 13, 6]
    let points = [348, 568, 331, 455]
    let games = [31, 33, 34, 36]
    let players = [4, 3, 1, 3]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.delegate = self
        collectionView?.dataSource = self
        collectionView?.showsVerticalScrollIndicator = false
        setupCollectionView()
        configureNavigationBar()
    }
    
    func setupCollectionView() {
        collectionView?.backgroundColor = .white
        collectionView?.register(LeagueCell.self, forCellWithReuseIdentifier: leagueCellId)
        collectionView?.register(MyLeaguesLoadingCell.self, forCellWithReuseIdentifier: loadingCellId)
        collectionView?.register(LiveGamesCell.self, forCellWithReuseIdentifier: liveGamesCell)
        collectionView?.register(CustomMyLeaguesHeader.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: headerId)
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ViewController")
        navigationController?.pushViewController(viewController, animated: true)
        Lambda.refreshStats()
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.item == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: liveGamesCell, for: indexPath) as! LiveGamesCell
            return cell
        } else {
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: loadingCellId, for: indexPath) as! MyLeaguesLoadingCell
//
//            cell.layer.cornerRadius = 12
//            cell.clipsToBounds = true
//            cell.backgroundColor = .white
//            cell.layer.borderColor = UIColor(red: 150/255, green: 150/255, blue: 150/255, alpha: 1).cgColor
//            cell.layer.borderWidth = 0.25
//            cell.leagueImage.layer.cornerRadius = 25
//
//            cell.layer.shadowColor = UIColor.gray.cgColor
//            cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
//            cell.layer.shadowRadius = 5.0
//            cell.layer.shadowOpacity = 0.15
//            cell.layer.masksToBounds = false
//            cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: cell.contentView.layer.cornerRadius).cgPath
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: leagueCellId, for: indexPath) as! LeagueCell
            cell.layer.cornerRadius = 12
            cell.clipsToBounds = true
            cell.backgroundColor = .white
            cell.layer.borderColor = UIColor(red: 150/255, green: 150/255, blue: 150/255, alpha: 1).cgColor
            cell.layer.borderWidth = 0.25
            cell.leagueImage.layer.cornerRadius = cell.leagueImage.frame.size.width / 2

            cell.layer.shadowColor = UIColor.gray.cgColor
            cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
            cell.layer.shadowRadius = 5.0
            cell.layer.shadowOpacity = 0.15
            cell.layer.masksToBounds = false
            cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: cell.contentView.layer.cornerRadius).cgPath

            cell.fantasyLeagueName.text = leagueNames[indexPath.row - 1]
            cell.fantasyTeamName.text = teamNames[indexPath.row - 1]
            cell.leagueImage.image = UIImage(named: images[indexPath.row - 1])
            cell.rank.text = "\(ranks[indexPath.row - 1])"
            cell.points.text = "\(points[indexPath.row - 1])"
            cell.games.text = "\(games[indexPath.row - 1])"
            cell.players.text = "\(players[indexPath.row - 1])"

            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        return CGSize(width: view.frame.width, height: 88)
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: headerId, for: indexPath) as! CustomMyLeaguesHeader
        header.backgroundColor = .white
        header.headerLabel.text = "My Leagues"
        
        return header
    }
            
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.item == 0 {
            return CGSize(width: view.frame.width, height: 124)
        } else {
            return CGSize(width: view.frame.width - 28, height: 124)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(10, 0, 10, 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 14
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 14
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        configureNavigationBar()
    }
    
    let statusBarView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.frame = UIApplication.shared.statusBarFrame
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    func configureNavigationBar() {
        view.addSubview(statusBarView)
        navigationController?.isNavigationBarHidden = true
        UIApplication.shared.statusBarStyle = .default
    }
}




