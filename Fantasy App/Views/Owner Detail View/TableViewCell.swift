//
//  TableViewCell-1.swift
//  Fantasy App
//
//  Created by Chad on 6/1/18.
//  Copyright © 2018 Chad. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate {
    
    var collectionView: UICollectionView!
    var index = Int()
    var ownerDetail = OwnerDetailVC()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = UICollectionViewScrollDirection.horizontal
        layout.sectionInset = UIEdgeInsetsMake(0, 16, 13, 16)
        layout.itemSize = CGSize(width: 131, height: 103)
        
        collectionView = UICollectionView(frame: self.bounds, collectionViewLayout: layout)
        collectionView.frame.size.width = UIScreen.main.bounds.width
        collectionView.frame.size.height = 117.5
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(GameCell.self, forCellWithReuseIdentifier: gameCellId)
        collectionView.backgroundColor = UIColor.white
        collectionView.showsHorizontalScrollIndicator = false
        
        self.addSubview(collectionView)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: UICollectionViewDataSource
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let playerIdArr = ModelController.getOwnerSortedOrder
        //print(playerIdArr)
        if playerIdArr.isEmpty {
            return 0
        } else {
            return ModelController.getOwnerPlayerGames[playerIdArr[index]].count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: gameCellId, for: indexPath) as! GameCell
        let playerIdArr = ModelController.getOwnerSortedOrder
        
        if let playerId = Int(playerIdArr[index]) {
            let playerGames = ModelController.getOwnerPlayerGames[String(playerId)][indexPath.item]

            let teamID = playerGames["teamID"].intValue
            let teamUrl = URL(string: "http://a.espncdn.com/combiner/i?img=/i/teamlogos/ncaa/500/\(playerGames["teamID"]).png&h=150&w=150")
            let oppUrl = URL(string: "http://a.espncdn.com/combiner/i?img=/i/teamlogos/ncaa/500/\(playerGames["oppoTeamID"]).png&h=150&w=150")
            
            let hexColorIndex = BackgroundColors.backgroundColors.index(where: {($0.teamID == teamID)}) as! Int
            let hexColor = BackgroundColors.backgroundColors.map {$0.color}[hexColorIndex]
            
            cell.backgroundColor = UIColor.white
            cell.layer.borderColor = UIColor(red: 214/255, green: 214/255, blue: 214/255, alpha: 1).cgColor
            cell.layer.borderWidth = 0.5
            cell.layer.cornerRadius = 8
            cell.clipsToBounds = true
            
            cell.teamLogo.kf.setImage(with: teamUrl)
            cell.oppoTeamLogo.kf.setImage(with: oppUrl)
            cell.teamSeed.text = playerGames["seed"].stringValue
            cell.oppoTeamSeed.text = playerGames["oppoSeed"].stringValue
            cell.teamScore.text = "\(playerGames["teamScore"].intValue)"
            cell.oppoTeamScore.text = "\(playerGames["opponentScore"].intValue)"
            cell.gameTime.text = playerGames["gameTime"].stringValue
            cell.roundView.backgroundColor = UIColor().colorFromHex(hexColor)
            if playerGames["round"].stringValue == "NATIONAL CHAMPIONSHIP" {
                cell.round.text = "NATIONAL CHAMP"
            } else {
                cell.round.text = playerGames["round"].stringValue
            }
            
            cell.layer.shadowColor = UIColor.gray.cgColor
            cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
            cell.layer.shadowRadius = 5.0
            cell.layer.shadowOpacity = 0.15
            cell.layer.masksToBounds = false
            cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: cell.contentView.layer.cornerRadius).cgPath
            return cell
        } else {
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        let cell = ownerDetail.ownerDetailTable.cellForRow(at: IndexPath(row: index * 2, section: 0)) as! OwnerDetailTableViewCell
        let playerIdArr = ModelController.getOwnerSortedOrder
        if let playerId = Int(playerIdArr[index]) {
            let playerGames = ModelController.getOwnerPlayerGames[String(playerId)][indexPath.item]
            
            cell.gpLabel.fadeTransition(0.5)
            cell.gpLabel.text = "MIN"
            cell.gp.fadeTransition(0.5)
            cell.gp.text = playerGames["min"].stringValue
            
            cell.mpgLabel.fadeTransition(0.5)
            cell.mpgLabel.text = "FG"
            cell.mpg.fadeTransition(0.5)
            cell.mpg.text = playerGames["fg"].stringValue
            
            cell.fgLabel.fadeTransition(0.5)
            cell.fgLabel.text = "3PT"
            cell.fg.fadeTransition(0.5)
            cell.fg.text = playerGames["3pt"].stringValue
            
            cell.threePtLabel.fadeTransition(0.5)
            cell.threePtLabel.text = "AST"
            cell.threePt.fadeTransition(0.5)
            cell.threePt.text = playerGames["ast"].stringValue
            
            cell.apgLabel.fadeTransition(0.5)
            cell.apgLabel.text = "REB"
            cell.apg.fadeTransition(0.5)
            cell.apg.text = playerGames["reb"].stringValue
            
            cell.rpgLabel.fadeTransition(0.5)
            cell.rpgLabel.text = "STL"
            cell.rpgLabelTrailConst.constant = 0
            cell.rpg.fadeTransition(0.5)
            cell.rpg.text = playerGames["stl"].stringValue
            
            cell.topgLabel.fadeTransition(0.5)
            cell.topgLabel.text = "TO"
            cell.topgLabelTrailConst.constant = 7.5
            cell.topg.fadeTransition(0.5)
            cell.topg.text = playerGames["to"].stringValue
            
            cell.ppgLabel.fadeTransition(0.5)
            cell.ppgLabel.text = "PTS"
            cell.ppgLabelConst.constant = 6.5
            cell.ppg.fadeTransition(0.5)
            cell.ppg.text = playerGames["pts"].stringValue
            
            cell.fpLabel.fadeTransition(0.5)
            cell.fpLabel.text = "FPTS"
            cell.fppg.fadeTransition(0.5)
            cell.fppg.text = playerGames["fantasyPts"].stringValue
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        cell?.contentView.backgroundColor = UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 1)
    }
    
    func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        cell?.contentView.backgroundColor = .clear
    }
    
}
