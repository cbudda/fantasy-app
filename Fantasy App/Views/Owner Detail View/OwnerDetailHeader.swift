//
//  OwnerDetailHeader.swift
//  Fantasy App
//
//  Created by Chad on 6/9/18.
//  Copyright © 2018 Chad. All rights reserved.
//

import UIKit

class OwnerDetailHeader: UITableViewCell {

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var ownerTeamName: UILabel!
    @IBOutlet weak var ownerName: UILabel!
    @IBOutlet weak var points: UILabel!
    @IBOutlet weak var games: UILabel!
    @IBOutlet weak var players: UILabel!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bottomView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        profileImage.layer.cornerRadius = profileImage.frame.size.width/2
        profileImage.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
