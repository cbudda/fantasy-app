//
//  customPlayerGameCells.swift
//  Fantasy App
//
//  Created by Chad on 6/1/18.
//  Copyright © 2018 Chad. All rights reserved.
//

import UIKit

let gameCellId = "GameCellId"

class GameCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super .init(frame: frame)
        setupViews()
    }
    
    let teamLogo: UIImageView = {
        let image = UIImageView()
        image.frame = CGRect(x: 18, y: 0, width: 30, height: 30)
        image.contentMode = UIViewContentMode.scaleAspectFit
        image.translatesAutoresizingMaskIntoConstraints = false
        image.tintColor = .darkGray
        return image
    }()
    
    let oppoTeamLogo: UIImageView = {
        let oppoImage = UIImageView()
        oppoImage.frame = CGRect(x: 18, y: 0, width: 30, height: 30)
        oppoImage.contentMode = UIViewContentMode.scaleAspectFit
        oppoImage.translatesAutoresizingMaskIntoConstraints = false
        oppoImage.tintColor = .darkGray
        return oppoImage
    }()
    
    let teamSeed: UILabel = {
        let seedLabel = UILabel()
        seedLabel.font = UIFont.systemFont(ofSize: 8)
        seedLabel.translatesAutoresizingMaskIntoConstraints = false
        return seedLabel
    }()
    
    let oppoTeamSeed: UILabel = {
        let oppoSeedLabel = UILabel()
        oppoSeedLabel.font = UIFont.systemFont(ofSize: 8)
        oppoSeedLabel.translatesAutoresizingMaskIntoConstraints = false
        return oppoSeedLabel
    }()
    
    let teamScore: UILabel = {
        let teamScore = UILabel()
        teamScore.font = UIFont.systemFont(ofSize: 17)
        teamScore.translatesAutoresizingMaskIntoConstraints = false
        return teamScore
    }()
    
    let oppoTeamScore: UILabel = {
        let oppoTeamScore = UILabel()
        oppoTeamScore.font = UIFont.systemFont(ofSize: 17)
        oppoTeamScore.translatesAutoresizingMaskIntoConstraints = false
        return oppoTeamScore
    }()
    
    let gameTime: UILabel = {
        let gameTime = UILabel()
        gameTime.font = UIFont.systemFont(ofSize: 13)
        gameTime.translatesAutoresizingMaskIntoConstraints = false
        gameTime.numberOfLines = 0
        gameTime.textAlignment = .center
        return gameTime
    }()
    
    let round: UILabel = {
        let round = UILabel()
        round.font = UIFont.systemFont(ofSize: 13, weight: .medium)
        round.translatesAutoresizingMaskIntoConstraints = false
        round.numberOfLines = 0
        round.textAlignment = .center
        return round
    }()
    
    let roundView: UIView = {
        let roundView = UIView()
        roundView.backgroundColor = UIColor(red: 0/255, green: 93/255, blue: 170/255, alpha: 1)
        roundView.translatesAutoresizingMaskIntoConstraints = false
        return roundView
    }()
    
    func setupViews() {
        addSubview(teamLogo)
        teamLogo.topAnchor.constraint(equalTo: self.topAnchor, constant: 31).isActive = true
        teamLogo.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 20).isActive = true
        teamLogo.widthAnchor.constraint(equalToConstant: 30).isActive = true
        teamLogo.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        addSubview(oppoTeamLogo)
        oppoTeamLogo.topAnchor.constraint(equalTo: teamLogo.bottomAnchor, constant: 3).isActive = true
        oppoTeamLogo.leftAnchor.constraint(equalTo: teamLogo.leftAnchor).isActive = true
        oppoTeamLogo.widthAnchor.constraint(equalToConstant: 30).isActive = true
        oppoTeamLogo.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        addSubview(teamSeed)
        teamSeed.topAnchor.constraint(equalTo: teamLogo.bottomAnchor, constant: -13).isActive = true
        teamSeed.rightAnchor.constraint(equalTo: teamLogo.leftAnchor, constant: -3).isActive = true
        
        addSubview(oppoTeamSeed)
        oppoTeamSeed.topAnchor.constraint(equalTo: oppoTeamLogo.bottomAnchor, constant: -14).isActive = true
        oppoTeamSeed.rightAnchor.constraint(equalTo: teamSeed.rightAnchor).isActive = true
        
        addSubview(teamScore)
        teamScore.centerYAnchor.constraint(equalTo: teamLogo.centerYAnchor, constant: 2).isActive = true
        teamScore.leftAnchor.constraint(equalTo: teamLogo.rightAnchor, constant: 8).isActive = true
        
        addSubview(oppoTeamScore)
        oppoTeamScore.centerYAnchor.constraint(equalTo: oppoTeamLogo.centerYAnchor, constant: -2).isActive = true
        oppoTeamScore.leftAnchor.constraint(equalTo: teamScore.leftAnchor).isActive = true
        
        addSubview(gameTime)
        gameTime.widthAnchor.constraint(equalToConstant: 36).isActive = true
        gameTime.leftAnchor.constraint(equalTo: teamLogo.rightAnchor, constant: 38).isActive = true
        gameTime.topAnchor.constraint(equalTo: teamLogo.topAnchor).isActive = true
        gameTime.bottomAnchor.constraint(equalTo: oppoTeamLogo.bottomAnchor).isActive = true
        
        addSubview(round)
        round.topAnchor.constraint(equalTo: self.topAnchor, constant: 8).isActive = true
        round.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        
        addSubview(roundView)
        roundView.topAnchor.constraint(equalTo: round.bottomAnchor).isActive = true
        roundView.leftAnchor.constraint(equalTo: round.leftAnchor).isActive = true
        roundView.rightAnchor.constraint(equalTo: round.rightAnchor).isActive = true
        roundView.heightAnchor.constraint(equalToConstant: 1).isActive = true
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
