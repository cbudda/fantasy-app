//
//  OwnerDetailVC.swift
//  Fantasy App
//
//  Created by Chad on 5/29/18.
//  Copyright © 2018 Chad. All rights reserved.
//

import UIKit

var playerIdArr = [Int]()
var playerDraftArr = [Int]()

class OwnerDetailVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIGestureRecognizerDelegate {
    //Be sure to add UICollectionViewDataSource
    
    @IBOutlet weak var ownerDetailTable: UITableView!
    @IBOutlet weak var headerImage: UIImageView!
    @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var statsLabel: UILabel!
    
    var ownerDetailRefreshControl = UIRefreshControl()
    var expand = Bool()
    let standardHeaderHeight: CGFloat = 125
    let maxHeaderHeight: CGFloat = 125
    lazy var navbarHeight = CGFloat((navigationController?.navigationBar.frame.size.height)!)
    let statusbarHeight = CGFloat(UIApplication.shared.statusBarFrame.size.height)
    lazy var topHeight = navbarHeight + statusbarHeight
	
    //(navigationController?.navigationBar.frame.size.height)!)
    var previousScrollOffset: CGFloat = 0
    
    var teamHexColor = String()
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return SendtoPlayerDetailView.sendToOwnerDetail.count * 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row%2 == 0 {
            let cell = ownerDetailTable.dequeueReusableCell(withIdentifier: "OwnerDetailTableViewCell") as! OwnerDetailTableViewCell
            let index = Int(floor(Double(indexPath.row/2)))
            
            let teamID = SendtoPlayerDetailView.sendToOwnerDetail.map {$0.teamID}[index]
            let hexColorIndex = BackgroundColors.backgroundColors.index(where: {($0.teamID == teamID)}) as! Int
            let hexColor = BackgroundColors.backgroundColors.map {$0.color}[hexColorIndex]
            teamHexColor = hexColor
            
            let url = URL(string: "http://a.espncdn.com/combiner/i?img=/i/teamlogos/ncaa/500/\(teamID).png&h=150&w=150")
            let formatter = NumberFormatter()
            formatter.minimumIntegerDigits = 0
            formatter.minimumFractionDigits = 3
            formatter.maximumFractionDigits = 3
            cell.isUserInteractionEnabled = false
            cell.teamLogo.kf.setImage(with: url)
            cell.playerName.text = SendtoPlayerDetailView.sendToOwnerDetail.map {$0.name}[index]
            cell.teamName.text = SendtoPlayerDetailView.sendToOwnerDetail.map {$0.team}[index]
            cell.stackViewTopView.backgroundColor = UIColor().colorFromHex(hexColor)
            cell.stackViewBottomView.backgroundColor = UIColor().colorFromHex(hexColor)
            cell.totalPts.text = "\(SendtoPlayerDetailView.sendToOwnerDetail.map {$0.points}[index]) PTS"
            if ModelController.getOwnerSortedOrder.count > 0 {
                let playerId = ModelController.getOwnerSortedOrder[index]
                let summary = ModelController.getOwnerPlayerSummary[String(playerId)]
                let games = summary["games"].floatValue
                cell.gp.text = String(Int(games))
                cell.mpg.text = String(format: "%.1f", summary["min"].floatValue/games)
                
                let fgPercent = summary["fgm"].floatValue/summary["fga"].floatValue
                if fgPercent.isNaN {
                    cell.fg.text = ".000"
                } else {
                cell.fg.text = formatter.string(from: NSNumber(value: summary["fgm"].floatValue/summary["fga"].floatValue))
                }
                
                let threePtPercent = summary["3pm"].floatValue/summary["3pa"].floatValue
                if threePtPercent.isNaN {
                    cell.threePt.text = ".000"
                } else {
                cell.threePt.text = formatter.string(from: NSNumber(value: summary["3pm"].floatValue/summary["3pa"].floatValue))
                }
                
                cell.apg.text = String(format: "%.1f", summary["ast"].floatValue/games)
                cell.rpg.text = String(format: "%.1f", summary["reb"].floatValue/games)
                cell.topg.text = String(format: "%.1f", summary["to"].floatValue/games)
                cell.ppg.text = String(format: "%.1f", summary["pts"].floatValue/games)
                cell.fppg.text = String(format: "%.1f", summary["fantasyPts"].floatValue/games)
            }
            return cell
        } else {
            let cell: TableViewCell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell", for: indexPath) as! TableViewCell
            cell.backgroundColor = UIColor.groupTableViewBackground
            cell.index = Int(floor(Double(indexPath.row/2)))
            cell.ownerDetail = self
//            let range = Range(uncheckedBounds: (0, cell.collectionView.numberOfSections))
//            let indexSet = IndexSet(integersIn: range)
//            cell.collectionView.reloadSections(indexSet)
            cell.collectionView.reloadData()
            
            
            if ModelController.getOwnerSortedOrder.count > 0 {
                let playerId = ModelController.getOwnerSortedOrder[Int(floor(Double(indexPath.row/2)))]
                let playerGames = ModelController.getOwnerPlayerGames[String(playerId)].count
                
                if playerGames > 0 {
                    cell.isHidden = false
                } else {
                    cell.isHidden = true
                }
            }
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row%2 == 0 {
        return 120
        } else {
            if ModelController.getOwnerSortedOrder.count > 0 {
                let playerId = ModelController.getOwnerSortedOrder[Int(floor(Double(indexPath.row/2)))]
                let playerGames = ModelController.getOwnerPlayerGames[String(playerId)].count
                
                if playerGames > 0 {
                    return 118
                } else {
                    return 0
                }
            }
            return 117.5
        }
    }
    
    var headerBlurImageView:UIImageView!
    
    override func viewDidAppear(_ animated: Bool) {
        headerBlurImageView = UIImageView(frame: headerImage.bounds)
        headerBlurImageView?.image = UIImage(named: "finalFour")?.blurredImage(withRadius: 10, iterations: 20, tintColor: UIColor.clear)
        headerBlurImageView?.contentMode = UIViewContentMode.scaleAspectFill
        headerBlurImageView?.alpha = 0.0
        headerImage.addSubview(headerBlurImageView)
    }
    
    lazy var offset_HeaderStop:CGFloat = 125 - topHeight // At this offset the Header stops its transformations
    let offset_B_LabelHeader:CGFloat = 85.0 // At this offset the Black label reaches the Header
    let distance_W_LabelHeader:CGFloat = 63.0 // The distance between the bottom of the Header and the top of the White Label
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let offset = scrollView.contentOffset.y
        var avatarTransform = CATransform3DIdentity
        var headerTransform = CATransform3DIdentity
        
        // PULL DOWN -----------------
        
        if offset < 0 {
            let headerScaleFactor:CGFloat = -(offset) / headerImage.bounds.height
            let headerSizevariation = ((headerImage.bounds.height * (1.0 + headerScaleFactor)) - headerImage.bounds.height)/2.0
            headerTransform = CATransform3DTranslate(headerTransform, 0, headerSizevariation, 0)
            headerTransform = CATransform3DScale(headerTransform, 1.0 + headerScaleFactor, 1.0 + headerScaleFactor, 0)
            headerLabel.textColor = .clear
            statsLabel.textColor = .clear
            headerImage.layer.transform = headerTransform
        }
            
            // SCROLL UP/DOWN ------------
            
        else {
            
            // Header -----------
            
            headerTransform = CATransform3DTranslate(headerTransform, 0, max(-offset_HeaderStop, -offset), 0)
            
            //  ------------ Label
            
            let labelTransform = CATransform3DMakeTranslation(0, max(-distance_W_LabelHeader, offset_B_LabelHeader - offset), 0)
            headerLabel.layer.transform = labelTransform
            statsLabel.layer.transform = labelTransform
            headerLabel.textColor = .white
            statsLabel.textColor = .white
            
            //  ------------ Blur
            
            headerBlurImageView?.alpha = min (1, (offset - offset_B_LabelHeader)/distance_W_LabelHeader)
            
            // Avatar -----------
            
//            let avatarScaleFactor = (min(offset_HeaderStop, offset)) / avatarImage.bounds.height / 1.4 // Slow down the animation
//            let avatarSizeVariation = ((avatarImage.bounds.height * (1.0 + avatarScaleFactor)) - avatarImage.bounds.height) / 2.0
//            avatarTransform = CATransform3DTranslate(avatarTransform, 0, avatarSizeVariation, 0)
//            avatarTransform = CATransform3DScale(avatarTransform, 1.0 - avatarScaleFactor, 1.0 - avatarScaleFactor, 0)
            
            if offset <= offset_HeaderStop {
                
//                if avatarImage.layer.zPosition < header.layer.zPosition{
//                    header.layer.zPosition = 0
//                }
                
            }else {
//                if avatarImage.layer.zPosition >= header.layer.zPosition{
//                    header.layer.zPosition = 2
//                }
            }
        }
        
        // Apply Transformations
        
        headerImage.layer.transform = headerTransform
//        avatarImage.layer.transform = avatarTransform
    }
    
    let headerView = Bundle.main.loadNibNamed("OwnerDetailHeader", owner: self, options: nil)?.first as! OwnerDetailHeader
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addGesture()
        self.navigationItem.title = ""
        self.navigationController?.navigationBar.tintColor = .white
        
        headerLabel.text = SendtoPlayerDetailView.fantasyTeamName
        
        headerView.ownerName.text = SendtoPlayerDetailView.sendToOwnerDetail[0].owner
        headerView.profileImage.image = UIImage(named: SendtoPlayerDetailView.sendToOwnerDetail[0].owner)
        headerView.ownerTeamName.text = SendtoPlayerDetailView.fantasyTeamName
        headerView.topView.backgroundColor = UIColor(red: 163/255, green: 161/255, blue: 159/255, alpha: 1)
        headerView.bottomView.backgroundColor = UIColor(red: 163/255, green: 161/255, blue: 159/255, alpha: 1)

        headerView.points.text = String(ModelController.getOwnerSummary["fantasyPts"].intValue)
        headerView.games.text = String(ModelController.getOwnerSummary["games"].intValue)
        headerView.players.text = String(ModelController.getOwnerSummary["players"].intValue)
        ownerDetailTable.tableHeaderView = headerView
        ownerDetailTable.delegate = self
        ownerDetailTable.dataSource = self
        ownerDetailTable.register(TableViewCell.self, forCellReuseIdentifier: "TableViewCell")
        
        headerImage.layer.masksToBounds = true
        headerImage.image = UIImage(named: "finalFour")
        headerImage.contentMode = UIViewContentMode.scaleAspectFill
        
        let gradient = CAGradientLayer()

        gradient.frame = headerImage.bounds
        gradient.colors = [UIColor.clear.cgColor, UIColor.white.cgColor]
        gradient.startPoint = CGPoint(x: 0, y: 0.4)
        gradient.endPoint = CGPoint(x: 0, y: 1.0)

        //headerImage.layer.insertSublayer(gradient, at: 0)
        
        self.ownerDetailTable.separatorColor = .clear
        
        setupStats()
        //setUpHeader()
        //print(minHeaderHeight)
        
        NotificationCenter.default.addObserver(self, selector: #selector(loadList), name: NSNotification.Name(rawValue: "loadOwnerDetail"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(endRefresh), name: NSNotification.Name(rawValue: "endOwnerDetailRefresh"), object: nil)
        
//        ownerDetailRefreshControl.addTarget(self, action: #selector(requestData), for: .valueChanged)
//        ownerDetailTable.refreshControl = ownerDetailRefreshControl
//        self.ownerDetailTable.reloadData()
    }
    
    @objc
    func requestData() {
        Lambda.getOwnerDetailStats()
    }
    
    @objc func endRefresh(){
        //end refresh
        self.ownerDetailRefreshControl.endRefreshing()
    }
    
    @objc func loadList(){
        //load data here
        UIView.transition(with: ownerDetailTable, duration: 0.5, options: .transitionCrossDissolve, animations: {self.ownerDetailTable.reloadData()}, completion: nil)
            
        headerView.points.text = String(ModelController.getOwnerSummary["fantasyPts"].intValue)
        headerView.games.text = String(ModelController.getOwnerSummary["games"].intValue)
        headerView.players.text = String(ModelController.getOwnerSummary["players"].intValue)
        
        statsLabel.text = "Points: \(ModelController.getOwnerSummary["fantasyPts"].intValue)    Games: \(ModelController.getOwnerSummary["games"].intValue)    Players: \(ModelController.getOwnerSummary["players"].intValue)"
    }
    
    func setupStats() {
        print("setup")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath)
    }
    
    
    
    
    
    //IMPLEMENT CUSTOM POP ANIMATION ----------------------------------------------------------------------------------------
    
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!
    
    func addGesture() {
        
        guard (navigationController?.viewControllers.count)! > 1 else {
            return
        }
        
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(OwnerDetailVC.handlePanGesture(_:)))
        self.view.addGestureRecognizer(panGestureRecognizer)
    }
    
    @objc func handlePanGesture(_ panGesture: UIPanGestureRecognizer) {
        
        let percent = max(panGesture.translation(in: view).x, 0) / view.frame.width
        
        switch panGesture.state {
            
        case .began:
            navigationController?.delegate = self
            _ = navigationController?.popViewController(animated: true)
            
        case .changed:
            if let percentDrivenInteractiveTransition = percentDrivenInteractiveTransition {
                percentDrivenInteractiveTransition.update(percent)
            }
            
        case .ended:
            let velocity = panGesture.velocity(in: view).x
            
            // Continue if drag more than 50% of screen width or velocity is higher than 1000
            if percent > 0.5 || velocity > 1000 {
                percentDrivenInteractiveTransition.finish()
            } else {
                percentDrivenInteractiveTransition.cancel()
            }
            
        case .cancelled, .failed:
            percentDrivenInteractiveTransition.cancel()
            
        default:
            break
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
    // END OF POP ANIMATION ---------------------------------------------------------------------------------------



