//
//  ViewController.swift
//  Fantasy App
//
//  Created by Chad on 4/15/18.
//  Copyright © 2018 Chad. All rights reserved.
//

import UIKit
import AWSMobileClient
import AWSAuthCore
import AWSCore
import AWSDynamoDB
import Kingfisher
import SwiftyJSON

extension String {
    
    func widthOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedStringKey.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.width
    }
}

extension UIImageView{
    
    func setImageFromURl(stringImageUrl url: String){
        
        if let url = NSURL(string: url) {
            if let data = NSData(contentsOf: url as URL) {
                self.image = UIImage(data: data as Data)
            }
        }
    }
}

struct postStruct {
    var rank : String!
    var owner : String!
    var players : String!
    var pointsLbl : String!
}

class SendtoPlayerDetailView {
    
    static var playerName = String()
    static var schoolName = String()
    static var schoolLogo = String()
    static var schoolID = Int()
    static var playerID = Int()
    static var draft = Int()
    static var owner = String()
    
    static var sendToOwnerDetail = [(
        name: String,
        team: String,
        games: Int,
        owner: String,
        points: Int,
        teamID: Int,
        playerID: Int,
        draft: Int
        )]()
    
    static var fantasyTeamName = String()
}

var heightofHeader : CGFloat = 20
var ownerRefreshControl = UIRefreshControl()
var playerRefreshControl = UIRefreshControl()

let labelArray = ["My Leagues", "Create a League","Join a League","Share"]
let settingsImageArray = ["home","add","join","share"]
let primeBlue = UIColor(red: 28.0/255, green: 169.0/255, blue: 233.0/255, alpha: 1.0)
let lightGray = UIColor(red: 230/250, green: 230/250, blue: 230/250, alpha: 1)

class SecondSection: UICollectionViewCell {
    override init(frame: CGRect) {
        super .init(frame: frame)
        setupViews()
    }
    let settingsLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 18)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    func setupViews() {
        addSubview(settingsLabel)
        settingsLabel.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        settingsLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        settingsLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 20).isActive = true
        settingsLabel.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

class WordCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super .init(frame: frame)
        setupViews()
    }
    
    let labelImage: UIImageView = {
        let image = UIImageView()
        image.frame = CGRect(x: 20, y: 0, width: 30, height: 30)
        image.contentMode = UIViewContentMode.scaleAspectFit
        image.translatesAutoresizingMaskIntoConstraints = false
        image.tintColor = .darkGray
        return image
    }()
    
    let wordLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 18)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    func setupViews() {
        addSubview(labelImage)
        labelImage.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        labelImage.widthAnchor.constraint(equalToConstant: 25).isActive = true
        labelImage.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 20).isActive = true
        labelImage.heightAnchor.constraint(equalToConstant: 25).isActive = true

        addSubview(wordLabel)
        wordLabel.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        wordLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        wordLabel.leftAnchor.constraint(equalTo: labelImage.rightAnchor, constant: 20).isActive = true
        wordLabel.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
  
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    var headerArray = [postStruct]()
    
    let segmentedControl = UISegmentedControl()
    let buttonBar = UIView()
    let bounds = UIScreen.main.bounds
    lazy var width = bounds.size.width
    lazy var segmentWidth = CGFloat(width/2)
    lazy var selectedSegment = Int(self.segments.selectedSegmentIndex)
    lazy var leftBound = CGFloat((segmentWidth - segmentTitles[Int(self.segments.selectedSegmentIndex)])/2)
    let titleString = "Leaderboard"
    lazy var titleWidth = titleString.widthOfString(usingFont: UIFont.systemFont(ofSize: 14)) + 16
    let playerString = "Player Stats"
    lazy var playerWidth = playerString.widthOfString(usingFont: UIFont.systemFont(ofSize: 14)) + 16
    lazy var segmentTitles = [titleWidth, playerWidth]
    lazy var sidemenuLeftBound = CGFloat(bounds.size.width * -0.75)
    var collectionView: UICollectionView!
    let loadingCell = "LoadingCell"
    
    @IBOutlet weak var segments: UISegmentedControl!
    
    @IBAction func segments(_ sender: UISegmentedControl?) {
        UIView.animate(withDuration: 0.35) {
//            self.buttonBar.frame.origin.x = (self.segments.frame.width / CGFloat(self.segments.numberOfSegments)) * CGFloat(self.segments.selectedSegmentIndex) + self.leftBound
            print(self.segments.selectedSegmentIndex)
            switch sender?.selectedSegmentIndex {
        case 0:
            self.scrollView.setContentOffset(CGPoint( x : 0, y : 0), animated: false)
        case 1:
            self.scrollView.setContentOffset(CGPoint( x : self.width, y : 0), animated: false)
        default:
            print("Scrolled")
        }
        }
    }
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var ownerTable: UITableView!
    @IBOutlet weak var textfield: UITextField!
    @IBOutlet weak var playerStatsTable: UITableView!
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var constraintsView: UIView!
    
    
    @objc
    func requestData() {
        Lambda.refreshStats()
    }
    
    let text = "Leaderboard"
    let font = UIFont.systemFont(ofSize: 16)
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    var isLoaded: Bool = false
    
    @objc func loadList(){
        //load data here
        isLoaded = true
        self.ownerTable.isUserInteractionEnabled = true
        UIView.transition(with: ownerTable, duration: 0.5, options: .transitionCrossDissolve, animations: {self.ownerTable.reloadData()}, completion: nil)
        UIView.transition(with: playerStatsTable, duration: 0.5, options: .transitionCrossDissolve, animations: {self.playerStatsTable.reloadData()}, completion: nil)
    }
    
    @objc func endRefresh(){
        //load data here
        self.ownerTable.reloadData()
        self.playerStatsTable.reloadData()
    }
    
    var blackView = UIView()
    lazy var navbarHeight = CGFloat((navigationController?.navigationBar.frame.size.height)!)
    let statusbarHeight = CGFloat(UIApplication.shared.statusBarFrame.size.height)
    lazy var topheight = navbarHeight + statusbarHeight
        //show menu
    
    @objc func sideMenu() {
        //Add side menu view
        view.addSubview(sideMenuView)
        UIApplication.shared.statusBarStyle = .default

        blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(menuDismiss)))
        UIView.animate(withDuration: 0.175, animations: {
            self.blackView.alpha = 0.5
            self.sidemenuLeftBound = 0
            self.view.frame.origin.x = CGFloat(self.bounds.size.width * 0.75)
//            self.collectionView.frame.origin.x = 0
            //self.constraintsView.frame.origin.x = self.width * 0.75
            self.blackView.frame.origin.x = CGFloat(self.bounds.size.width * 0.75)
            self.navigationController?.navigationBar.frame.origin.x = CGFloat(self.bounds.size.width * 0.75)
            })
    }
    
    @objc func menuDismiss() {
        UIView.animate(withDuration: 0.175) {
            UIApplication.shared.statusBarStyle = .lightContent
            self.blackView.alpha = 0
            self.sidemenuLeftBound = CGFloat(self.bounds.size.width * -0.75)
            self.sideMenuView.frame.origin.x = self.sidemenuLeftBound
            self.view.frame.origin.x = 0
            self.navigationController?.navigationBar.frame.origin.x = 0
            self.blackView.frame.origin.x = 0
        }
    }
    
    let sideMenuView: UIView = {
       
        let menuView = UIView()
        menuView.backgroundColor = .white
        menuView.translatesAutoresizingMaskIntoConstraints = false
        
        return menuView
        
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.ownerTable.isUserInteractionEnabled = false
        self.scrollView.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(loadList), name: NSNotification.Name(rawValue: "load"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(endRefresh), name: NSNotification.Name(rawValue: "endRefresh"), object: nil)
        setupNavigationBarItems()
        ownerTable.register(OwnerLoadingCell.self, forCellReuseIdentifier: loadingCell)
        print(isLoaded)
        segments.backgroundColor = . clear
        segments.tintColor = .clear
        print("STATUS BAR HEIGHT ---- \(statusbarHeight) -- TOP HEIGHT ---- \(topheight)")
        
        segmentedControl.translatesAutoresizingMaskIntoConstraints = false
        
        segments.setTitleTextAttributes([
            NSAttributedStringKey.font : UIFont.systemFont(ofSize: 16),
            NSAttributedStringKey.foregroundColor: UIColor.lightGray
            ], for: .normal)
        segments.setTitleTextAttributes([
            NSAttributedStringKey.font : UIFont.systemFont(ofSize: 16),
            NSAttributedStringKey.foregroundColor: UIColor(red: 0.0/255, green: 0.0/255, blue: 0.0/255, alpha: 1.0)
            ], for: .selected)
        
        buttonBar.translatesAutoresizingMaskIntoConstraints = false
        buttonBar.backgroundColor = primaryColor
        
        segments.addSubview(buttonBar)
        
        // Constrain the top of the button bar to the bottom of the segmented control
        buttonBar.topAnchor.constraint(equalTo: segments.bottomAnchor).isActive = true
        buttonBar.heightAnchor.constraint(equalToConstant: 2.0).isActive = true
        
        // Constrain the button bar to the left side of the segmented control
        buttonBar.leftAnchor.constraint(equalTo: segments.leftAnchor, constant: leftBound).isActive = true
        
        // Constrain the button bar to the width of the segmented control divided by the number of segments
        buttonBar.widthAnchor.constraint(equalToConstant: titleWidth).isActive = true
        
        //textfield.text = "View Controller Loaded"
        
        // Get the identity Id from the AWSIdentityManager
        ////let appDelegate = UIApplication.shared.delegate as! AppDelegate
        ////let credentialsProvider = AWSMobileClient.sharedInstance().getCredentialsProvider()
        ////let identityId = AWSIdentityManager.default().identityId
        
        headerArray = [postStruct.init(rank: "", owner: "", players: "", pointsLbl: "")]
        
        ownerTable.delegate = self
        ownerTable.dataSource = self
        playerStatsTable.delegate = self
        playerStatsTable.dataSource = self
        
        ownerTable.tableFooterView = UIView()
        
        // Create refresh control and add as subview in your table view.
        ownerRefreshControl.addTarget(self, action: #selector(requestData), for: .valueChanged)
        playerRefreshControl.addTarget(self, action: #selector(requestData), for: .valueChanged)
        playerStatsTable.addSubview(playerRefreshControl)
        ownerTable.addSubview(ownerRefreshControl)
        
//        view.addSubview(sideMenuView)
//        sideMenuView.topAnchor.constraint(equalTo: view.topAnchor, constant: -topheight).isActive = true
//        sideMenuView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: sidemenuLeftBound).isActive = true
//        sideMenuView.rightAnchor.constraint(equalTo: view.leftAnchor).isActive = true
//        sideMenuView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        //setupCollectionView()
        let flowLayout = UICollectionViewFlowLayout()
        let window = UIApplication.shared.keyWindow!
        flowLayout.sectionHeadersPinToVisibleBounds = true
        collectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: view.frame.width * 0.75, height: window.frame.height + topheight), collectionViewLayout: flowLayout)
        collectionView.delegate = self
        collectionView.alwaysBounceVertical = true
        collectionView.isScrollEnabled = true
        collectionView.register(WordCell.self, forCellWithReuseIdentifier: cellId)
        collectionView.register(SecondSection.self, forCellWithReuseIdentifier: secondCellId)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = .white
        collectionView.register(UICollectionViewCell.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: headerId)
        collectionView.register(UICollectionViewCell.self, forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: footerId)
        
        self.sideMenuView.addSubview(collectionView)
        addGesture()
        
        blackView = UIView(frame: CGRect(x: view.frame.origin.x, y: window.frame.origin.y, width: view.frame.width, height: window.frame.height))
        window.addSubview(blackView)
        blackView.backgroundColor = UIColor.lightGray
        blackView.translatesAutoresizingMaskIntoConstraints = false
        blackView.alpha = 0
        
        navigationController?.navigationBar.isTranslucent = false
        view.backgroundColor = primaryColor
        
        NotificationCenter.default.addObserver(self, selector:#selector(self.didEnterBackground), name:
            NSNotification.Name.UIApplicationDidEnterBackground, object: nil)

    }
    
    @objc func didEnterBackground() {
        UIApplication.shared.statusBarStyle = .lightContent
        self.blackView.alpha = 0
        self.sidemenuLeftBound = CGFloat(self.bounds.size.width * -0.75)
        self.sideMenuView.frame.origin.x = self.sidemenuLeftBound
        self.view.frame.origin.x = 0
        self.navigationController?.navigationBar.frame.origin.x = 0
        self.blackView.frame.origin.x = 0
        print(self.blackView.frame.origin.x)
    }
    
    let cellId = "cellId"
    let secondCellId = "secondCellId"
    let headerId = "headerId"
    let footerId = "footerId"
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if section == 0 {
        return labelArray.count
        } else {
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if indexPath.section == 0 {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! WordCell
        cell.wordLabel.text = labelArray[indexPath.row]
        cell.labelImage.image = UIImage(named: settingsImageArray[indexPath.row])?.withRenderingMode(.alwaysTemplate)
        return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: secondCellId, for: indexPath) as! SecondSection
            cell.settingsLabel.text = "Settings and Privacy"
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: sideMenuView.frame.width, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsets(top: 0, left: 5, bottom: 10, right: 5)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if kind == UICollectionElementKindSectionHeader {
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerId, for: indexPath)
            header.backgroundColor = .white

            //Initialize profile image
            let profileImage = UIImageView()
            profileImage.frame = CGRect(x: 0, y: 0, width: 60, height: 60)
            profileImage.contentMode = UIViewContentMode.scaleAspectFit
            profileImage.backgroundColor = lightGray
            profileImage.image = UIImage(named: "Chad Walker")
            profileImage.translatesAutoresizingMaskIntoConstraints = false
            profileImage.layer.cornerRadius = profileImage.frame.size.width/2
            profileImage.layer.masksToBounds = true
            header.addSubview(profileImage)
            profileImage.topAnchor.constraint(equalTo: header.topAnchor, constant: topheight * 0.9).isActive = true
            profileImage.widthAnchor.constraint(equalToConstant: 60).isActive = true
            profileImage.leftAnchor.constraint(equalTo: header.leftAnchor, constant: 20).isActive = true
            profileImage.heightAnchor.constraint(equalToConstant: 60).isActive = true
            
            //Initialize owner name
            let ownerName = UILabel()
            ownerName.font = UIFont.systemFont(ofSize: 20, weight: .bold)
            ownerName.text = "Chad Walker"
            header.addSubview(ownerName)
            ownerName.translatesAutoresizingMaskIntoConstraints = false
            ownerName.bottomAnchor.constraint(equalTo: profileImage.centerYAnchor).isActive = true
            ownerName.leftAnchor.constraint(equalTo: profileImage.rightAnchor, constant: 8).isActive = true
            ownerName.rightAnchor.constraint(equalTo: header.rightAnchor, constant: -8).isActive = true
            
            //Initialize owner username
            let username = UILabel()
            username.font = UIFont.systemFont(ofSize: 14)
            username.textColor = .gray
            username.text = "@igloohead9000"
            header.addSubview(username)
            username.translatesAutoresizingMaskIntoConstraints = false
            username.topAnchor.constraint(equalTo: ownerName.bottomAnchor).isActive = true
            username.leftAnchor.constraint(equalTo: profileImage.rightAnchor, constant: 8).isActive = true
            username.rightAnchor.constraint(equalTo: header.rightAnchor, constant: -8).isActive = true
            
            return header
        } else {
            let footer = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerId, for: indexPath)
            footer.backgroundColor = .white
            
            let separatorLineView = UIView()
            footer.addSubview(separatorLineView)
            separatorLineView.backgroundColor = UIColor(white: 0, alpha: 0.5)
            separatorLineView.backgroundColor = UIColor(red: 230/250, green: 230/250, blue: 230/250, alpha: 1)
            separatorLineView.translatesAutoresizingMaskIntoConstraints = false
            separatorLineView.topAnchor.constraint(equalTo: footer.topAnchor).isActive = true
            separatorLineView.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
            separatorLineView.leftAnchor.constraint(equalTo: footer.leftAnchor).isActive = true
            separatorLineView.rightAnchor.constraint(equalTo: footer.rightAnchor).isActive = true
            
            return footer
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let viewController = MyLeaguesVC(collectionViewLayout: UICollectionViewFlowLayout())
        navigationController?.popViewController(animated: false)
        navigationController?.pushViewController(viewController, animated: false)
        
        self.blackView.alpha = 0
        self.blackView.frame.origin.x = 0
        UIApplication.shared.statusBarStyle = .lightContent
        self.navigationController?.navigationBar.frame.origin.x = 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        cell?.contentView.backgroundColor = lightGray
    }
    
    func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        cell?.contentView.backgroundColor = .clear
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if section == 0 {
        return CGSize(width: sideMenuView.frame.width, height: view.frame.height * 0.21)
        } else {
        return CGSize.zero
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        if section == 0 {
        return CGSize(width: sideMenuView.frame.width, height: 10.5)
        } else {
            return CGSize.zero
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        setStatusBarBackgroundColor(color: .clear)
        UIApplication.shared.statusBarStyle = .lightContent
        self.navigationController?.view.backgroundColor = primaryColor
        navigationController?.navigationBar.barTintColor = primaryColor
        if let indexPath = ownerTable.indexPathForSelectedRow {
            ownerTable.deselectRow(at: indexPath, animated: false)
        }
    }
    
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!
    var dismissMenuPan: UIPanGestureRecognizer!
    
    func addGesture() {
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(self.handlePanGesture(_:)))
        panGestureRecognizer.delegate = self
        dismissMenuPan = UIPanGestureRecognizer(target: self, action: #selector(self.handleDismissPanGesture(_:)))
        dismissMenuPan.delegate = self
        //self.collectionView.addGestureRecognizer(dismissMenuPan)
        self.view.addGestureRecognizer(panGestureRecognizer)
    }
    
    @objc func handleDismissPanGesture(_ panGesture: UIPanGestureRecognizer) {
        let percent = min(panGesture.translation(in: view).x, 0) / view.frame.width
        
        if collectionView.isDragging == false {
            switch panGesture.state {
                
            case .began:
                print("Dismiss Began")
                
            case .changed:
                if let percentDrivenInteractiveTransition = percentDrivenInteractiveTransition {
                    percentDrivenInteractiveTransition.update(percent)
                }
                self.view.frame.origin.x = CGFloat(bounds.size.width * 0.75) - CGFloat(bounds.size.width * 0.75) * -percent
                self.blackView.frame.origin.x = CGFloat(bounds.size.width * 0.75) - CGFloat(self.bounds.size.width * 0.75) * -percent
                self.blackView.alpha = 0.5 - 0.5 * -percent
                self.navigationController?.navigationBar.frame.origin.x = CGFloat(bounds.size.width * 0.75) - CGFloat(self.bounds.size.width * 0.75) * -percent
                print("changed \(percent)")
                
            case .ended:
                let velocity = panGesture.velocity(in: view).x
                let xPoints = self.bounds.size.width * 0.75 * -percent
                let duration = TimeInterval(xPoints / velocity)
                // Continue if drag more than 50% of screen width or velocity is higher than 1000
                if velocity > 1000 {
                    UIView.animate(withDuration: duration, animations: {
                        self.view.frame.origin.x = 0
                        self.blackView.frame.origin.x = 0.0
                        self.blackView.alpha = 0.0
                        self.navigationController?.navigationBar.frame.origin.x = 0.0
                        self.blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.menuDismiss)))
                    })
                } else if percent < -0.3 {
                    UIView.animate(withDuration: 0.175, animations: {
                        self.view.frame.origin.x = 0
                        self.blackView.frame.origin.x = 0
                        self.blackView.alpha = 0.0
                        self.navigationController?.navigationBar.frame.origin.x = 0
                        self.blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.menuDismiss)))
                    })
                }
                else {
                    UIView.animate(withDuration: 0.175, animations: {
                        self.view.frame.origin.x = CGFloat(self.bounds.size.width * 0.75)
                        self.blackView.frame.origin.x = CGFloat(self.bounds.size.width * 0.75)
                        self.blackView.alpha = 0.5
                        self.navigationController?.navigationBar.frame.origin.x = CGFloat(self.bounds.size.width * 0.75)
                        print("cancel")
                    })
                }
                
            case .cancelled, .failed:
                percentDrivenInteractiveTransition?.cancel()
                
            default:
                break
            }
        }
    }
    
    @objc func handlePanGesture(_ panGesture: UIPanGestureRecognizer) {
        
        let percent = max(panGesture.translation(in: view).x, 0) / view.frame.width
        
        if segments.selectedSegmentIndex == 0 && collectionView.isDragging == false && self.view.frame.origin.x != CGFloat(self.bounds.size.width * 0.75) && ownerTable.isDragging == false && scrollView.contentOffset.x == 0.0 {
        switch panGesture.state {
            
        case .began:
            print("Began Pan Gesture")
            
        case .changed:
            if let percentDrivenInteractiveTransition = percentDrivenInteractiveTransition {
                percentDrivenInteractiveTransition.update(percent)
            }
            self.view.frame.origin.x = CGFloat(bounds.size.width * 0.75) * percent
            self.blackView.frame.origin.x = CGFloat(self.bounds.size.width * 0.75) * percent
            self.blackView.alpha = 0.5 * percent
            self.navigationController?.navigationBar.frame.origin.x = CGFloat(self.bounds.size.width * 0.75) * percent
            print("changed \(percent)")
            
        case .ended:
            let velocity = panGesture.velocity(in: view).x
            let xPoints = self.bounds.size.width * 0.75 * percent
            let duration = TimeInterval(xPoints / velocity)
            // Continue if drag more than 50% of screen width or velocity is higher than 1000
            if velocity > 1000 {
                UIView.animate(withDuration: duration, animations: {
                    self.view.frame.origin.x = CGFloat(self.bounds.size.width * 0.75)
                    self.blackView.frame.origin.x = CGFloat(self.bounds.size.width * 0.75)
                    self.blackView.alpha = 0.5
                    self.navigationController?.navigationBar.frame.origin.x = CGFloat(self.bounds.size.width * 0.75)
                    self.blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.menuDismiss)))
                    UIApplication.shared.statusBarStyle = .default
                })
                print("finish")
            } else if percent > 0.4 {
                UIView.animate(withDuration: 0.175, animations: {
                    self.view.frame.origin.x = CGFloat(self.bounds.size.width * 0.75)
                    self.blackView.frame.origin.x = CGFloat(self.bounds.size.width * 0.75)
                    self.blackView.alpha = 0.5
                    self.navigationController?.navigationBar.frame.origin.x = CGFloat(self.bounds.size.width * 0.75)
                    self.blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.menuDismiss)))
                    UIApplication.shared.statusBarStyle = .default
                })
            }
                
            else {
                UIView.animate(withDuration: 0.175, animations: {
                self.view.frame.origin.x = 0
                self.blackView.alpha = 0
                self.blackView.frame.origin.x = 0
                self.navigationController?.navigationBar.frame.origin.x = 0
                print("cancel")
                })
            }
            
        case .cancelled, .failed:
            percentDrivenInteractiveTransition?.cancel()
            
        default:
            break
        }
        }
    }
    
    let searchBar: UITextField = {
       let search = UITextField()
        search.attributedPlaceholder = NSAttributedString(string: "Laker Legends", attributes: [NSAttributedStringKey.foregroundColor : UIColor.white])
        search.font = UIFont.systemFont(ofSize: 16)
        search.textColor = .white
        search.setLeftPaddingPoints(24)
        search.backgroundColor = UIColor(red: 43/255, green: 73/255, blue: 124/255, alpha: 1.0)
        search.layer.cornerRadius = 14
        search.translatesAutoresizingMaskIntoConstraints = false
        search.clipsToBounds = true
        search.leftViewMode = UITextFieldViewMode.always
        search.leftViewMode = .always
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 43, height: 18))
        let image = UIImage(named: "search")
        imageView.image = image
        imageView.contentMode = .center
        search.leftView = imageView
        
        return search
    }()
    
    private func setupNavigationBarItems() {
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = primaryColor
        navigationItem.titleView = searchBar
        searchBar.heightAnchor.constraint(equalToConstant: 28).isActive = true
        searchBar.widthAnchor.constraint(equalToConstant: view.frame.width - 100).isActive = true
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue): UIColor.white]
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 18, weight: .bold), NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue): UIColor.white]

        let settingsButton = UIButton(type: .system)
        settingsButton.setImage(#imageLiteral(resourceName: "settingsIcon").withRenderingMode(.alwaysTemplate), for: .normal)
        settingsButton.widthAnchor.constraint(equalToConstant: 22.5).isActive = true
        settingsButton.heightAnchor.constraint(equalToConstant: 22.5).isActive = true
        settingsButton.tintColor = UIColor.white
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: settingsButton)
    }
    
    var oldContentOffset = CGPoint.zero
    lazy var topConstraints = (CGFloat((segmentWidth - segmentTitles[0])/2)..<CGFloat(segmentWidth + (segmentWidth - segmentTitles[1])/2))
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView != ownerTable && scrollView != playerStatsTable && scrollView != collectionView {
            var previousPage : NSInteger = 0
            let pageWidth : CGFloat = scrollView.frame.size.width
            let fractionalPage = Float(scrollView.contentOffset.x / pageWidth)
            let page = lroundf(fractionalPage)
            let delta = (scrollView.contentOffset.x - oldContentOffset.x) * ((topConstraints.upperBound - topConstraints.lowerBound)/width)
            let widthDelta = (scrollView.contentOffset.x - oldContentOffset.x) * ((segmentTitles[1] - segmentTitles[0])/width)
            if delta > 0 && leftBound < topConstraints.upperBound {
                leftBound += delta
                buttonBar.frame.size.width += widthDelta
            }
            if delta < 0 && leftBound >= topConstraints.lowerBound {
                leftBound += delta
                buttonBar.frame.size.width += widthDelta
            }
            if previousPage != page {
                previousPage = page
                }
        self.buttonBar.frame.origin.x = leftBound
        oldContentOffset = scrollView.contentOffset
        segments.selectedSegmentIndex = previousPage
//        buttonBar.frame.size.width = segmentTitles[segments.selectedSegmentIndex]
        print(buttonBar.frame.size.width)
        segments(nil)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.ownerTable {
            if isLoaded == false {
                return 12
            } else {
                return ModelController.ownerPointsArray.count
            }
        } else {
            return ModelController.draftedPlayerStats.count
        }
        
    }
    
    // To setup the TableView cells for each table
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.ownerTable {
            if isLoaded == false {
                let cell = ownerTable.dequeueReusableCell(withIdentifier: loadingCell) as! OwnerLoadingCell
                return cell
            } else {
                let cell = ownerTable.dequeueReusableCell(withIdentifier: "OwnerTableViewCell") as! OwnerCellTableViewCell

                let ownerPointsArray = ModelController.ownerPointsArray
                
                if ownerPointsArray.isEmpty == false {
                cell.ownerCell.text = "\(String(indexPath.row + 1))"
                cell.owner.text = String(ownerPointsArray.map {$0.name}[indexPath.row])
                cell.games.text = "\(ownerPointsArray.map {$0.games}[indexPath.row])"
                cell.points.text = "\(ownerPointsArray.map {$0.points}[indexPath.row])"
                cell.players.text = "\(ownerPointsArray.map {$0.players}[indexPath.row])"
                cell.teamName.text = "\(ownerPointsArray.map {$0.teamName}[indexPath.row])"
                cell.ownerImage.image = UIImage(named: "\(ownerPointsArray.map {$0.name}[indexPath.row])")
                cell.ownerImage.layer.cornerRadius = cell.ownerImage.frame.size.width/2
                cell.ownerImage.layer.masksToBounds = true
                cell.rightArrow.image = UIImage(named: "RightArrow")
                cell.rightArrow.image = cell.rightArrow.image!.withRenderingMode(.alwaysTemplate)
                cell.rightArrow.tintColor = UIColor.lightGray
                }
                return cell
            }
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PlayerStatsTableViewCell") as! PlayerStatsTableViewCell
            
            let playerArray = ModelController.draftedPlayerStats
            //let imgURL = "http://a.espncdn.com/combiner/i?img=/i/teamlogos/ncaa/500/2390.png&h=150&w=150"
            
            if playerArray.isEmpty == false {
                let teamID = playerArray.map {$0.teamID}[indexPath.row]
                let url = URL(string: "http://a.espncdn.com/combiner/i?img=/i/teamlogos/ncaa/500/\(teamID).png&h=150&w=150")
                cell.rankCell.text = String(indexPath.row + 1)
                cell.playerName.text = String(playerArray.map {$0.name}[indexPath.row])
                cell.gamesPlayed.text = String(playerArray.map {$0.games}[indexPath.row])
                cell.pointsScored.text = "\(playerArray.map {$0.points}[indexPath.row])"
                cell.playerSchool.text = "\(playerArray.map {$0.owner}[indexPath.row])"
                cell.teamLogo.kf.setImage(with: url)
                cell.rightArrow.image = UIImage(named: "RightArrow")
                cell.rightArrow.image = cell.rightArrow.image!.withRenderingMode(.alwaysTemplate)
                cell.rightArrow.tintColor = UIColor.lightGray
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == self.ownerTable {
        return 0
        } else {
        return heightofHeader
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if tableView == self.ownerTable {
//        let headerView = Bundle.main.loadNibNamed("HeaderView", owner: self, options: nil)?.first as! HeaderView
//
//        headerView.headerLabel.text = headerArray[section].rank
//        headerView.ownerLabel.text = headerArray[section].owner
//        headerView.playersLabel.text = headerArray[section].players
//        headerView.pointsLabel.text = headerArray[section].pointsLbl
            
        return nil
        
        } else {
            let headerView = Bundle.main.loadNibNamed("PlayerStatsHeader", owner: self, options: nil)?.first as! PlayerStatsHeader
            
            headerView.playerRank.text = "Rank"
            headerView.playerNameHeader.text = "Player Name"
            headerView.GP.text = "GP"
            headerView.Pts.text = "Pts"

        return headerView
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == self.ownerTable {
            
            let viewController = storyboard?.instantiateViewController(withIdentifier: "OwnerDetailVC")
            
            let foundItems = ModelController.draftedPlayerStats.filter { $0.owner == ModelController.ownerPointsArray.map {$0.name}[indexPath.row] }
            
            playerIdArr = foundItems.map {$0.playerID}
            playerDraftArr = foundItems.map {$0.draft}
            
            ModelController.getOwnerPlayerGames = JSON()
            ModelController.getOwnerSummary = JSON()
            ModelController.getOwnerPlayerSummary = JSON()
            ModelController.getOwnerSortedOrder = [String]()
            
            Lambda.getOwnerDetailStats()
            
            SendtoPlayerDetailView.sendToOwnerDetail = foundItems
            SendtoPlayerDetailView.fantasyTeamName = ModelController.ownerPointsArray[indexPath.row].teamName
            
            navigationController?.pushViewController(viewController!, animated: true)
            
        } else {
            
            let playerArray = ModelController.draftedPlayerStats
            let viewController = storyboard?.instantiateViewController(withIdentifier: "PlayerDetailVC")
            ModelController.detailedViewPlayerTableValues.removeAll()
            navigationController?.pushViewController(viewController!, animated: true)
            
            SendtoPlayerDetailView.playerName = playerArray.map {$0.name}[indexPath.row]
            SendtoPlayerDetailView.schoolName = playerArray.map {$0.team}[indexPath.row]
            SendtoPlayerDetailView.schoolLogo = "http://a.espncdn.com/combiner/i?img=/i/teamlogos/ncaa/500/\(playerArray.map {$0.teamID}[indexPath.row]).png&h=150&w=150"
            SendtoPlayerDetailView.schoolID = playerArray.map {$0.teamID}[indexPath.row]
            SendtoPlayerDetailView.playerID = playerArray.map {$0.playerID}[indexPath.row]
            SendtoPlayerDetailView.draft = playerArray.map {$0.draft}[indexPath.row]
            SendtoPlayerDetailView.owner = playerArray.map {$0.owner}[indexPath.row]
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

