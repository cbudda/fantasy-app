//
//  OwnerLoadingCell.swift
//  Fantasy App
//
//  Created by Chad on 8/19/18.
//  Copyright © 2018 Chad. All rights reserved.
//

import UIKit

class OwnerLoadingCell: UITableViewCell {
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    let leagueImage: UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = shimmerLight
        return v
    }()
    
    let darkLeagueImage: UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = shimmerDark
        return v
    }()
    
    let fantasyTeamName: UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = shimmerLight
        return v
    }()
    
    let darkFantasyTeamName: UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = shimmerDark
        //        v.alpha = 0.5
        return v
    }()
    
    let fantasyLeagueName: UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = shimmerLight
        return v
    }()
    
    let darkFantasyLeagueName: UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = shimmerDark
        return v
    }()
    
    let statsLabel: UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = shimmerLight
        return v
    }()
    
    let darkStatsLabel: UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = shimmerDark
        return v
    }()
    
    let gradientLayer: CAGradientLayer = {
        let gradient = CAGradientLayer()
        gradient.colors = [UIColor.clear.cgColor, UIColor.white.cgColor, UIColor.clear.cgColor]
        gradient.locations = [0, 0.5, 1]
        return gradient
    }()
    
    let containerView: UIView = {
        let v = UIView()
        v.backgroundColor = .clear
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    func setupViews() {
        addSubview(darkFantasyTeamName)
        addSubview(darkLeagueImage)
        addSubview(darkFantasyLeagueName)
        addSubview(darkStatsLabel)
        
        addSubview(containerView)
        containerView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        containerView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        containerView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 77.67).isActive = true
        containerView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -24).isActive = true
        
        containerView.addSubview(leagueImage)
        leagueImage.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true
        leagueImage.widthAnchor.constraint(equalToConstant: 45).isActive = true
        leagueImage.leftAnchor.constraint(equalTo: containerView.leftAnchor).isActive = true
        leagueImage.heightAnchor.constraint(equalToConstant: 45).isActive = true
        leagueImage.layer.cornerRadius = 22.5
        
        containerView.addSubview(fantasyLeagueName)
        containerView.addSubview(fantasyTeamName)
        fantasyTeamName.bottomAnchor.constraint(equalTo: fantasyLeagueName.topAnchor, constant: -10.5).isActive = true
        fantasyTeamName.leftAnchor.constraint(equalTo: leagueImage.rightAnchor, constant: 12).isActive = true
        fantasyTeamName.rightAnchor.constraint(equalTo: containerView.rightAnchor).isActive = true
        fantasyTeamName.heightAnchor.constraint(equalToConstant: 8).isActive = true
        
        fantasyLeagueName.centerYAnchor.constraint(equalTo: leagueImage.centerYAnchor, constant: 0).isActive = true
        fantasyLeagueName.leftAnchor.constraint(equalTo: fantasyTeamName.leftAnchor).isActive = true
        fantasyLeagueName.rightAnchor.constraint(equalTo: fantasyTeamName.rightAnchor, constant: -175).isActive = true
        fantasyLeagueName.heightAnchor.constraint(equalToConstant: 8).isActive = true
        
        containerView.addSubview(statsLabel)
        statsLabel.topAnchor.constraint(equalTo: fantasyLeagueName.bottomAnchor, constant: 10.5).isActive = true
        statsLabel.leftAnchor.constraint(equalTo: fantasyTeamName.leftAnchor).isActive = true
        statsLabel.rightAnchor.constraint(equalTo: fantasyTeamName.rightAnchor, constant: -75).isActive = true
        statsLabel.heightAnchor.constraint(equalToConstant: 8).isActive = true
        
        darkLeagueImage.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true
        darkLeagueImage.widthAnchor.constraint(equalToConstant: 45).isActive = true
        darkLeagueImage.leftAnchor.constraint(equalTo: containerView.leftAnchor).isActive = true
        darkLeagueImage.heightAnchor.constraint(equalToConstant: 45).isActive = true
        darkLeagueImage.layer.cornerRadius = 22.5
        
        darkFantasyTeamName.bottomAnchor.constraint(equalTo: fantasyLeagueName.topAnchor, constant: -10.5).isActive = true
        darkFantasyTeamName.leftAnchor.constraint(equalTo: leagueImage.rightAnchor, constant: 12).isActive = true
        darkFantasyTeamName.rightAnchor.constraint(equalTo: containerView.rightAnchor).isActive = true
        darkFantasyTeamName.heightAnchor.constraint(equalToConstant: 8).isActive = true
        
        darkFantasyLeagueName.centerYAnchor.constraint(equalTo: leagueImage.centerYAnchor, constant: 0).isActive = true
        darkFantasyLeagueName.leftAnchor.constraint(equalTo: fantasyTeamName.leftAnchor).isActive = true
        darkFantasyLeagueName.rightAnchor.constraint(equalTo: fantasyTeamName.rightAnchor, constant: -175).isActive = true
        darkFantasyLeagueName.heightAnchor.constraint(equalToConstant: 8).isActive = true
        
        darkStatsLabel.topAnchor.constraint(equalTo: fantasyLeagueName.bottomAnchor, constant: 10.5).isActive = true
        darkStatsLabel.leftAnchor.constraint(equalTo: fantasyTeamName.leftAnchor).isActive = true
        darkStatsLabel.rightAnchor.constraint(equalTo: fantasyTeamName.rightAnchor, constant: -75).isActive = true
        darkStatsLabel.heightAnchor.constraint(equalToConstant: 8).isActive = true
        
        containerView.layer.mask = gradientLayer
        gradientLayer.frame = self.bounds
        let angle = -45 * CGFloat.pi / 180
        gradientLayer.transform = CATransform3DMakeRotation(angle, 0, 0, 1)
        
        let animation = CABasicAnimation(keyPath: "transform.translation.x")
        animation.duration = 2
        animation.fromValue = -self.frame.width
        animation.toValue = self.frame.width
        animation.repeatCount = Float.infinity
        
        gradientLayer.add(animation, forKey: "gradientKey")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
