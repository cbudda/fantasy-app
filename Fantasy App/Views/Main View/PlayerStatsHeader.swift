//
//  PlayerStatsHeader.swift
//  Fantasy App
//
//  Created by Chad on 4/29/18.
//  Copyright © 2018 Chad. All rights reserved.
//

import UIKit

class PlayerStatsHeader: UITableViewCell {

    @IBOutlet weak var playerRank: UILabel!
    @IBOutlet weak var playerNameHeader: UILabel!
    @IBOutlet weak var GP: UILabel!
    @IBOutlet weak var Pts: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
