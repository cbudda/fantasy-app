//
//  PlayerStatsTableViewCell.swift
//  Fantasy App
//
//  Created by Chad on 4/22/18.
//  Copyright © 2018 Chad. All rights reserved.
//

import UIKit

class PlayerStatsTableViewCell: UITableViewCell {

    @IBOutlet weak var rankCell: UILabel!
    @IBOutlet weak var playerName: UILabel!
    @IBOutlet weak var gamesPlayed: UILabel!
    @IBOutlet weak var pointsScored: UILabel!
    @IBOutlet weak var playerSchool: UILabel!
    @IBOutlet weak var teamLogo: UIImageView!
    @IBOutlet weak var rightArrow: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
