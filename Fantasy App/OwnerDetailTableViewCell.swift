//
//  OwnerDetailTableViewCell.swift
//  
//
//  Created by Chad on 5/29/18.
//

import UIKit

class OwnerDetailTableViewCell: UITableViewCell {

    //Values
    @IBOutlet weak var teamLogo: UIImageView!
    @IBOutlet weak var playerName: UILabel!
    @IBOutlet weak var teamName: UILabel!
    @IBOutlet weak var gp: UILabel!
    @IBOutlet weak var mpg: UILabel!
    @IBOutlet weak var fg: UILabel!
    @IBOutlet weak var threePt: UILabel!
    @IBOutlet weak var apg: UILabel!
    @IBOutlet weak var rpg: UILabel!
    @IBOutlet weak var topg: UILabel!
    @IBOutlet weak var ppg: UILabel!
    @IBOutlet weak var fppg: UILabel!
    @IBOutlet weak var stackViewTopView: UIView!
    @IBOutlet weak var stackViewBottomView: UIView!
    @IBOutlet weak var playerStatus: UILabel!
    @IBOutlet weak var totalPts: UILabel!
    
    //Labels
    @IBOutlet weak var gpLabel: UILabel!
    @IBOutlet weak var mpgLabel: UILabel!
    @IBOutlet weak var fgLabel: UILabel!
    @IBOutlet weak var threePtLabel: UILabel!
    @IBOutlet weak var apgLabel: UILabel!
    @IBOutlet weak var rpgLabel: UILabel!
    @IBOutlet weak var topgLabel: UILabel!
    @IBOutlet weak var ppgLabel: UILabel!
    @IBOutlet weak var fpLabel: UILabel!
    @IBOutlet weak var rpgLabelTrailConst: NSLayoutConstraint!
    @IBOutlet weak var topgLabelTrailConst: NSLayoutConstraint!
    @IBOutlet weak var ppgLabelConst: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
