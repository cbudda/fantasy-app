//
//  OwnerCellTableViewCell.swift
//  
//
//  Created by Chad on 4/18/18.
//

import UIKit

class OwnerCellTableViewCell: UITableViewCell {

    @IBOutlet weak var ownerCell: UILabel!
    @IBOutlet weak var owner: UILabel!
    @IBOutlet weak var players: UILabel!
    @IBOutlet weak var teamName: UILabel!
    @IBOutlet weak var points: UILabel!
    @IBOutlet weak var games: UILabel!
    @IBOutlet weak var ownerImage: UIImageView!
    @IBOutlet weak var rightArrow: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
