//
//  Stats Data Model.swift
//  Fantasy App
//
//  Created by Chad on 4/15/18.
//  Copyright © 2018 Chad. All rights reserved.
//

import Foundation
import UIKit
import AWSDynamoDB

class DynamoDBManager: AWSDynamoDBObjectModel, AWSDynamoDBModeling {

        var playerID:String?
        var round:String?
        var name:String?
        var fantasyPts:String?

    class func dynamoDBTableName() -> String {
            return "tournamentStats"
        }

        class func hashKeyAttribute() -> String {
            return "playerID"
        }

        class func ignoreAttributes() -> Array<AnyObject>! {
            return nil
        }

//        override func isEqual(object: AnyObject?) -> Bool {
//            return super.isEqual(object)
//        }

        override func `self`() -> Self {
            return self
        }
    
    class func describeTable() -> AWSTask<AnyObject> {
        let dynamoDB = AWSDynamoDB.default()
        
        // See if the test table exists.
        let describeTableInput = AWSDynamoDBDescribeTableInput()
        describeTableInput?.tableName = "tournamentStats"
        return dynamoDB.describeTable(describeTableInput!) as! AWSTask<AnyObject>
    }
}


