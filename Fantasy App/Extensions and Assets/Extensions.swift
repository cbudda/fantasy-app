//
//  Extensions.swift
//  Fantasy App
//
//  Created by Chad on 5/2/18.
//  Copyright © 2018 Chad. All rights reserved.
//

import Foundation
import UIKit

let grayText = UIColor(red: 50/255, green: 50/255, blue: 50/255, alpha: 1)
let primaryColor = UIColor(red: 67/255, green: 105/255, blue: 176/255, alpha: 1)//UIColor(red: 20/255, green: 110/255, blue: 180/255, alpha: 1.0) //Amazon Dark Blue
let bluePrimary = UIColor(red: 66/255, green: 120/255, blue: 207/255, alpha: 1)
let placeholderGray = UIColor(red: 199/255, green: 199/255, blue: 205/255, alpha: 1)
let shimmerLight = UIColor(red: 235/255, green: 235/255, blue: 238/255, alpha: 1)
let shimmerDark = UIColor(red: 242/255, green: 242/255, blue: 245/255, alpha: 1)

extension UITextField {
    func setBottomBorder(color: UIColor) {
        self.borderStyle = .none
        self.layer.backgroundColor = UIColor.white.cgColor
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
    }
}

extension UITabBar {
    
    override open func sizeThatFits(_ size: CGSize) -> CGSize {
        var sizeThatFits = super.sizeThatFits(size)
        let deviceType = UIDevice().type
        if UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height == 2436 {
            sizeThatFits.height = 77.5 // adjust your size here
        } else {
            sizeThatFits.height = 50
        }
        return sizeThatFits
    }
}

extension UIImage {
    class func colorForNavBar(color: UIColor) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: 1, height: 0.5)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        
        context?.setFillColor(color.cgColor)
        context?.fill(rect)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image!
    }
}

extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}

extension NSMutableAttributedString {
    
    func setColor(color: UIColor, forText stringValue: String) {
        let range: NSRange = self.mutableString.range(of: stringValue, options: .caseInsensitive)
        self.addAttribute(NSAttributedStringKey.foregroundColor, value: color, range: range)
    }
    
}

extension UIColor {
    
    func colorFromHex(_ hex : String) -> UIColor {
        
        var hexString = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if hexString.hasPrefix("#") {
            hexString.remove(at: hexString.startIndex)
        }
        
        if hexString.count != 6 {
            return UIColor.black
        }
        
        var rgb : UInt32 = 0
        Scanner(string: hexString).scanHexInt32(&rgb)
        
        return UIColor.init(red: CGFloat((rgb & 0xFF0000) >> 16) / 255.0,
                            green: CGFloat((rgb & 0x00FF00) >> 8) / 255.0,
                            blue: CGFloat(rgb & 0x0000FF) / 255.0,
                            alpha: 1.0)
    }
}

public enum PanDirection: Int {
    case up, down, left, right
    public var isVertical: Bool { return [.up, .down].contains(self) }
    public var isHorizontal: Bool { return !isVertical }
}

public extension UIPanGestureRecognizer {
    
    public var direction: PanDirection? {
        let velocity = self.velocity(in: view)
        let isVertical = fabs(velocity.y) > fabs(velocity.x)
        switch (isVertical, velocity.x, velocity.y) {
        case (true, _, let y) where y < 0: return .up
        case (true, _, let y) where y > 0: return .down
        case (false, let x, _) where x > 0: return .right
        case (false, let x, _) where x < 0: return .left
        default: return nil
        }
        
    }
}

class SlideAnimatedTransitioning: NSObject {
    
}

extension SlideAnimatedTransitioning: UIViewControllerAnimatedTransitioning {
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        let containerView = transitionContext.containerView
        let fromView = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!.view
        let toView = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!.view
        
        let width = containerView.frame.width
        
        var offsetLeft = fromView?.frame
        offsetLeft?.origin.x = width
        
        var offscreenRight = toView?.frame
        offscreenRight?.origin.x = -width / 3.33;
        
        toView?.frame = offscreenRight!;
        
        fromView?.layer.shadowRadius = 5.0
        fromView?.layer.shadowOpacity = 1.0
        toView?.layer.opacity = 0.9
        
        containerView.insertSubview(toView!, belowSubview: fromView!)
        
        UIView.animate(withDuration: transitionDuration(using: transitionContext), delay:0, options:.curveLinear, animations:{
            
            toView?.frame = (fromView?.frame)!
            fromView?.frame = offsetLeft!
            
            toView?.layer.opacity = 1.0
            fromView?.layer.shadowOpacity = 0.1
            
        }, completion: { finished in
            toView?.layer.opacity = 1.0
            toView?.layer.shadowOpacity = 0
            fromView?.layer.opacity = 1.0
            fromView?.layer.shadowOpacity = 0
            
            // when cancelling or completing the animation, ios simulator seems to sometimes flash black backgrounds during the animation. on devices, this doesn't seem to happen though.
            // containerView.backgroundColor = [UIColor whiteColor];
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        })
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        
        return 0.3
    }
    
}

class MyView: UIView {
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        if clipsToBounds || isHidden || alpha == 0 {
            return nil
        }
        for subview in subviews.reversed() {
            let subPoint = subview.convert(point, from: self)
            if let result = subview.hitTest(subPoint, with: event) {
                return result
            }
        }
        return nil
    }
}

extension UIView {
    func fadeTransition(_ duration:CFTimeInterval) {
        let animation = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name:
            kCAMediaTimingFunctionEaseInEaseOut)
        animation.type = kCATransitionFade
        animation.duration = duration
        layer.add(animation, forKey: kCATransitionFade)
    }
}

class AHBaseViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureNavigationBar()
        // Configure Common status bar if you want too.
    }
    
    func configureNavigationBar() {
        let someLeftButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.refresh, target: self, action: "someAction")
        let menuButton = UIButton(type: .system)
        menuButton.setImage(#imageLiteral(resourceName: "menu").withRenderingMode(.alwaysTemplate), for: .normal)
        menuButton.widthAnchor.constraint(equalToConstant: 30).isActive = true
        menuButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        menuButton.tintColor = UIColor.white
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)
        navigationItem.leftBarButtonItem?.customView?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ViewController.sideMenu)))
        navigationItem.leftBarButtonItem = someLeftButton
        navigationItem.leftItemsSupplementBackButton = true
    }
}

extension UIButton {
    func centerTextAndImage(spacing: CGFloat) {
        let insetAmount = spacing / 2
        imageEdgeInsets = UIEdgeInsets(top: 0, left: -insetAmount, bottom: 0, right: insetAmount)
        titleEdgeInsets = UIEdgeInsets(top: 0, left: insetAmount, bottom: 0, right: -insetAmount)
        contentEdgeInsets = UIEdgeInsets(top: 0, left: insetAmount, bottom: 0, right: insetAmount)
    }
}

func setStatusBarBackgroundColor(color: UIColor) {
    
    guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return }
    
    statusBar.backgroundColor = color
}

public enum Model : String {
    case simulator     = "simulator/sandbox",
    //iPod
    iPod1              = "iPod 1",
    iPod2              = "iPod 2",
    iPod3              = "iPod 3",
    iPod4              = "iPod 4",
    iPod5              = "iPod 5",
    //iPad
    iPad2              = "iPad 2",
    iPad3              = "iPad 3",
    iPad4              = "iPad 4",
    iPadAir            = "iPad Air ",
    iPadAir2           = "iPad Air 2",
    iPad5              = "iPad 5", //aka iPad 2017
    iPad6              = "iPad 6", //aka iPad 2018
    //iPad mini
    iPadMini           = "iPad Mini",
    iPadMini2          = "iPad Mini 2",
    iPadMini3          = "iPad Mini 3",
    iPadMini4          = "iPad Mini 4",
    //iPad pro
    iPadPro9_7         = "iPad Pro 9.7\"",
    iPadPro10_5        = "iPad Pro 10.5\"",
    iPadPro12_9        = "iPad Pro 12.9\"",
    iPadPro2_12_9      = "iPad Pro 2 12.9\"",
    //iPhone
    iPhone4            = "iPhone 4",
    iPhone4S           = "iPhone 4S",
    iPhone5            = "iPhone 5",
    iPhone5S           = "iPhone 5S",
    iPhone5C           = "iPhone 5C",
    iPhone6            = "iPhone 6",
    iPhone6plus        = "iPhone 6 Plus",
    iPhone6S           = "iPhone 6S",
    iPhone6Splus       = "iPhone 6S Plus",
    iPhoneSE           = "iPhone SE",
    iPhone7            = "iPhone 7",
    iPhone7plus        = "iPhone 7 Plus",
    iPhone8            = "iPhone 8",
    iPhone8plus        = "iPhone 8 Plus",
    iPhoneX            = "iPhone X",
    //Apple TV
    AppleTV            = "Apple TV",
    AppleTV_4K         = "Apple TV 4K",
    unrecognized       = "?unrecognized?"
}

// #-#-#-#-#-#-#-#-#-#-#-#-#-#-#
//MARK: UIDevice extensions
// #-#-#-#-#-#-#-#-#-#-#-#-#-#-#

public extension UIDevice {
    public var type: Model {
        var systemInfo = utsname()
        uname(&systemInfo)
        let modelCode = withUnsafePointer(to: &systemInfo.machine) {
            $0.withMemoryRebound(to: CChar.self, capacity: 1) {
                ptr in String.init(validatingUTF8: ptr)
                
            }
        }
        var modelMap : [ String : Model ] = [
            "i386"      : .simulator,
            "x86_64"    : .simulator,
            //iPod
            "iPod1,1"   : .iPod1,
            "iPod2,1"   : .iPod2,
            "iPod3,1"   : .iPod3,
            "iPod4,1"   : .iPod4,
            "iPod5,1"   : .iPod5,
            //iPad
            "iPad2,1"   : .iPad2,
            "iPad2,2"   : .iPad2,
            "iPad2,3"   : .iPad2,
            "iPad2,4"   : .iPad2,
            "iPad3,1"   : .iPad3,
            "iPad3,2"   : .iPad3,
            "iPad3,3"   : .iPad3,
            "iPad3,4"   : .iPad4,
            "iPad3,5"   : .iPad4,
            "iPad3,6"   : .iPad4,
            "iPad4,1"   : .iPadAir,
            "iPad4,2"   : .iPadAir,
            "iPad4,3"   : .iPadAir,
            "iPad5,3"   : .iPadAir2,
            "iPad5,4"   : .iPadAir2,
            "iPad6,11"  : .iPad5, //aka iPad 2017
            "iPad6,12"  : .iPad5,
            "iPad7,5"   : .iPad6, //aka iPad 2018
            "iPad7,6"   : .iPad6,
            //iPad mini
            "iPad2,5"   : .iPadMini,
            "iPad2,6"   : .iPadMini,
            "iPad2,7"   : .iPadMini,
            "iPad4,4"   : .iPadMini2,
            "iPad4,5"   : .iPadMini2,
            "iPad4,6"   : .iPadMini2,
            "iPad4,7"   : .iPadMini3,
            "iPad4,8"   : .iPadMini3,
            "iPad4,9"   : .iPadMini3,
            "iPad5,1"   : .iPadMini4,
            "iPad5,2"   : .iPadMini4,
            //iPad pro
            "iPad6,3"   : .iPadPro9_7,
            "iPad6,4"   : .iPadPro9_7,
            "iPad7,3"   : .iPadPro10_5,
            "iPad7,4"   : .iPadPro10_5,
            "iPad6,7"   : .iPadPro12_9,
            "iPad6,8"   : .iPadPro12_9,
            "iPad7,1"   : .iPadPro2_12_9,
            "iPad7,2"   : .iPadPro2_12_9,
            //iPhone
            "iPhone3,1" : .iPhone4,
            "iPhone3,2" : .iPhone4,
            "iPhone3,3" : .iPhone4,
            "iPhone4,1" : .iPhone4S,
            "iPhone5,1" : .iPhone5,
            "iPhone5,2" : .iPhone5,
            "iPhone5,3" : .iPhone5C,
            "iPhone5,4" : .iPhone5C,
            "iPhone6,1" : .iPhone5S,
            "iPhone6,2" : .iPhone5S,
            "iPhone7,1" : .iPhone6plus,
            "iPhone7,2" : .iPhone6,
            "iPhone8,1" : .iPhone6S,
            "iPhone8,2" : .iPhone6Splus,
            "iPhone8,4" : .iPhoneSE,
            "iPhone9,1" : .iPhone7,
            "iPhone9,3" : .iPhone7,
            "iPhone9,2" : .iPhone7plus,
            "iPhone9,4" : .iPhone7plus,
            "iPhone10,1" : .iPhone8,
            "iPhone10,4" : .iPhone8,
            "iPhone10,2" : .iPhone8plus,
            "iPhone10,5" : .iPhone8plus,
            "iPhone10,3" : .iPhoneX,
            "iPhone10,6" : .iPhoneX,
            //AppleTV
            "AppleTV5,3" : .AppleTV,
            "AppleTV6,2" : .AppleTV_4K
        ]
        
        if let model = modelMap[String.init(validatingUTF8: modelCode!)!] {
            if model == .simulator {
                if let simModelCode = ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] {
                    if let simModel = modelMap[String.init(validatingUTF8: simModelCode)!] {
                        return simModel
                    }
                }
            }
            return model
        }
        return Model.unrecognized
    }
}

class BackgroundColors {
    static var backgroundColors : [(teamID: Int, color: String)] = [(teamID: 399, color: "#512D6D"),(teamID: 2066, color: "#0A6649"),(teamID: 42, color: "#C02427"),(teamID: 311, color: "#09305C"),(teamID: 160, color: "#071F42"),(teamID: 2619, color: "#A7262F"),(teamID: 2349, color: "#E51747"),(teamID: 2378, color: "#E5B217"),(teamID: 261, color: "#09593C"),(teamID: 2132, color: "#C71432"),(teamID: 151, color: "#592A8A"),(teamID: 248, color: "#D61717"),(teamID: 235, color: "#0A2769"),(teamID: 2567, color: "#C71432"),(teamID: 58, color: "#0A6649"),(teamID: 218, color: "#7E0427"),(teamID: 2655, color: "#05331D"),(teamID: 202, color: "#C5B783"),(teamID: 2116, color: "#B4985A"),(teamID: 41, color: "#05112E"),(teamID: 2724, color: "#E5C717"),(teamID: 2166, color: "#960F1F"),(teamID: 2168, color: "#CF1543"),(teamID: 2184, color: "#BA1334"),(teamID: 2230, color: "#9D2235"),(teamID: 2244, color: "#0A6638"),(teamID: 45, color: "#0A4466"),(teamID: 2325, color: "#082954"),(teamID: 113, color: "#881C1C"),(teamID: 227, color: "#09265C"),(teamID: 257, color: "#C32032"),(teamID: 2603, color: "#9C1029"),(teamID: 139, color: "#0E3887"),(teamID: 179, color: "#623B2A"),(teamID: 2670, color: "#E5AE17"),(teamID: 103, color: "#4C0808"),(teamID: 228, color: "#E56A17"),(teamID: 150, color: "#0C2D83"),(teamID: 52, color: "#862633"),(teamID: 59, color: "#E6AE27"),(teamID: 97, color: "#CB333B"),(teamID: 2390, color: "#064024"),(teamID: 152, color: "#E5171E"),(teamID: 153, color: "#7BAFD4"),(teamID: 87, color: "#0C2340"),(teamID: 221, color: "#0C2340"),(teamID: 183, color: "#E57A32"),(teamID: 258, color: "#08084C"),(teamID: 259, color: "#4C0808"),(teamID: 154, color: "#807750"),(teamID: 526, color: "#0A2869"),(teamID: 294, color: "#084F44"),(teamID: 338, color: "#E5B122"),(teamID: 288, color: "#260959"),(teamID: 2885, color: "#CC1414"),(teamID: 2454, color: "#0E518C"),(teamID: 2908, color: "#0B7041"),(teamID: 56, color: "#0A6649"),(teamID: 239, color: "#0C4B1C"),(teamID: 66, color: "#C71432"),(teamID: 2305, color: "#1230B5"),(teamID: 2306, color: "#482783"),(teamID: 201, color: "#80110D"),(teamID: 197, color: "#E56A17"),(teamID: 2628, color: "#5D1E91"),(teamID: 251, color: "#CB6015"),(teamID: 2641, color: "#DE2316"),(teamID: 277, color: "#0C4A7D"),(teamID: 2086, color: "#104D9E"),(teamID: 156, color: "#0E5A8C"),(teamID: 305, color: "#135BBA"),(teamID: 46, color: "#062040"),(teamID: 269, color: "#0A3866"),(teamID: 2507, color: "#1A1717"),(teamID: 2550, color: "#0E4A87"),(teamID: 2599, color: "#D31145"),(teamID: 222, color: "#0A2C63"),(teamID: 2752, color: "#092D57"),(teamID: 331, color: "#C91426"),(teamID: 70, color: "#8B6F4E"),(teamID: 304, color: "#E66A17"),(teamID: 149, color: "#861F41"),(teamID: 147, color: "#0C457D"),(teamID: 155, color: "#11A665"),(teamID: 2464, color: "#09265C"),(teamID: 2458, color: "#082C54"),(teamID: 2502, color: "#004A2A"),(teamID: 16, color: "#063825"),(teamID: 253, color: "#D41818"),(teamID: 2692, color: "#492365"),(teamID: 2097, color: "#1A1717"),(teamID: 2127, color: "#082C54"),(teamID: 2241, color: "#C71432"),(teamID: 2272, color: "#413C87"),(teamID: 2335, color: "#A6192E"),(teamID: 2344, color: "#0A1E3F"),(teamID: 2506, color: "#9D2235"),(teamID: 2515, color: "#C8102E"),(teamID: 2427, color: "#0B346B"),(teamID: 2737, color: "#A8113C"),(teamID: 356, color: "#E04E39"),(teamID: 84, color: "#A90533"),(teamID: 2294, color: "#E5BC17"),(teamID: 120, color: "#C71432"),(teamID: 130, color: "#071F42"),(teamID: 127, color: "#084F3B"),(teamID: 135, color: "#8C0E27"),(teamID: 158, color: "#C71432"),(teamID: 77, color: "#5B1A89"),(teamID: 194, color: "#CD1446"),(teamID: 213, color: "#071F42"),(teamID: 2509, color: "#B1946C"),(teamID: 164, color: "#C71432"),(teamID: 275, color: "#CD1543"),(teamID: 13, color: "#063831"),(teamID: 2239, color: "#0B4470"),(teamID: 2463, color: "#991B1E"),(teamID: 62, color: "#095936"),(teamID: 299, color: "#1A1717"),(teamID: 302, color: "#0A3E66"),(teamID: 300, color: "#106BA3"),(teamID: 27, color: "#2D6CC0"),(teamID: 2540, color: "#103EA1"),(teamID: 232, color: "#76232F"),(teamID: 48, color: "#0F5396"),(teamID: 2182, color: "#08294C"),(teamID: 2210, color: "#990F39"),(teamID: 2275, color: "#182E83"),(teamID: 256, color: "#423393"),(teamID: 111, color: "#CC1414"),(teamID: 119, color: "#1A1717"),(teamID: 350, color: "#0A6666"),(teamID: 2729, color: "#243926"),(teamID: 2429, color: "#084C2A"),(teamID: 2226, color: "#0A3261"),(teamID: 2229, color: "#0A3261"),(teamID: 2348, color: "#1143A8"),(teamID: 276, color: "#186E41"),(teamID: 2393, color: "#1470CC"),(teamID: 249, color: "#0C7A3A"),(teamID: 295, color: "#093757"),(teamID: 242, color: "#09265C"),(teamID: 2572, color: "#E5BC17"),(teamID: 5, color: "#1E6B52"),(teamID: 2636, color: "#072645"),(teamID: 2638, color: "#E57717"),(teamID: 98, color: "#E5171E"),(teamID: 325, color: "#0B6B51"),(teamID: 2174, color: "#E6174B"),(teamID: 2739, color: "#0B6B51"),(teamID: 85, color: "#B3123F"),(teamID: 270, color: "#1A1717"),(teamID: 94, color: "#E5B120"),(teamID: 2473, color: "#1A1717"),(teamID: 82, color: "#050533"),(teamID: 2750, color: "#0B7055"),(teamID: 2754, color: "#E51732"),(teamID: 225, color: "#4E3629"),(teamID: 171, color: "#97C6E5"),(teamID: 172, color: "#B31B1B"),(teamID: 159, color: "#0B6B3D"),(teamID: 108, color: "#A41034"),(teamID: 219, color: "#940F25"),(teamID: 163, color: "#E57722"),(teamID: 43, color: "#0B396B"),(teamID: 2099, color: "#0E2756"),(teamID: 2217, color: "#C72127"),(teamID: 314, color: "#910F31"),(teamID: 2363, color: "#0B7041"),(teamID: 2368, color: "#CF152A"),(teamID: 2405, color: "#071F42"),(teamID: 315, color: "#4F2170"),(teamID: 2514, color: "#072A4A"),(teamID: 2520, color: "#981E32"),(teamID: 2612, color: "#0B4170"),(teamID: 2561, color: "#0B6B58"),(teamID: 2006, color: "#092E5E"),(teamID: 2050, color: "#C9141A"),(teamID: 189, color: "#E57017"),(teamID: 2084, color: "#10259C"),(teamID: 2117, color: "#6F263D"),(teamID: 2199, color: "#084F2A"),(teamID: 2309, color: "#0C3D78"),(teamID: 193, color: "#E31937"),(teamID: 2459, color: "#B01212"),(teamID: 195, color: "#0B6E53"),(teamID: 2649, color: "#0A2B69"),(teamID: 2711, color: "#66350A"),(teamID: 2065, color: "#6F263D"),(teamID: 2154, color: "#115EA6"),(teamID: 2169, color: "#70C2E5"),(teamID: 50, color: "#E58217"),(teamID: 2261, color: "#092557"),(teamID: 47, color: "#092C5C"),(teamID: 2379, color: "#822433"),(teamID: 2415, color: "#103D7B"),(teamID: 2450, color: "#0C7A58"),(teamID: 2448, color: "#0D4D85"),(teamID: 2428, color: "#870E2C"),(teamID: 2542, color: "#E54417"),(teamID: 2569, color: "#940F30"),(teamID: 71, color: "#A61111"),(teamID: 2181, color: "#0B346B"),(teamID: 339, color: "#532380"),(teamID: 2287, color: "#CF152A"),(teamID: 282, color: "#134EBD"),(teamID: 2350, color: "#6A1638"),(teamID: 2623, color: "#5E0912"),(teamID: 2460, color: "#4B116D"),(teamID: 79, color: "#872C46"),(teamID: 2674, color: "#693F23"),(teamID: 2005, color: "#0E398F"),(teamID: 68, color: "#0C367A"),(teamID: 36, color: "#0A664F"),(teamID: 278, color: "#E5174B"),(teamID: 2440, color: "#071F42"),(teamID: 167, color: "#BA1334"),(teamID: 21, color: "#A81933"),(teamID: 23, color: "#105CA3"),(teamID: 2439, color: "#B01212"),(teamID: 328, color: "#093659"),(teamID: 2751, color: "#E6B122"),(teamID: 2803, color: "#1A1717"),(teamID: 2115, color: "#0E3887"),(teamID: 161, color: "#0E4687"),(teamID: 2341, color: "#1A1717"),(teamID: 116, color: "#0F6099"),(teamID: 2523, color: "#29375A"),(teamID: 2529, color: "#9E1036"),(teamID: 2597, color: "#A6192E"),(teamID: 2598, color: "#AF272F"),(teamID: 2681, color: "#06382E"),(teamID: 2046, color: "#BA1334"),(teamID: 2057, color: "#09265C"),(teamID: 2197, color: "#0F5396"),(teamID: 2198, color: "#6F263D"),(teamID: 55, color: "#D91734"),(teamID: 2413, color: "#0F4299"),(teamID: 93, color: "#072545"),(teamID: 2565, color: "#872C46"),(teamID: 2546, color: "#CC1414"),(teamID: 2630, color: "#072A4A"),(teamID: 2634, color: "#10599E"),(teamID: 2635, color: "#753BBD"),(teamID: 12, color: "#BA1334"),(teamID: 9, color: "#7D2248"),(teamID: 25, color: "#071F42"),(teamID: 38, color: "#CFB87C"),(teamID: 2483, color: "#173F35"),(teamID: 204, color: "#DE5C16"),(teamID: 24, color: "#8C1515"),(teamID: 26, color: "#1382C2"),(teamID: 30, color: "#990F0F"),(teamID: 254, color: "#BA1334"),(teamID: 264, color: "#363C74"),(teamID: 265, color: "#981E32"),(teamID: 44, color: "#0E418C"),(teamID: 349, color: "#1A1717"),(teamID: 104, color: "#CC1414"),(teamID: 2083, color: "#0D3B80"),(teamID: 2142, color: "#700B26"),(teamID: 107, color: "#3F0A61"),(teamID: 322, color: "#A8996E"),(teamID: 2329, color: "#653819"),(teamID: 2352, color: "#0A6959"),(teamID: 2426, color: "#09305C"),(teamID: 333, color: "#A32638"),(teamID: 8, color: "#A32638"),(teamID: 2, color: "#08274C"),(teamID: 57, color: "#112EA6"),(teamID: 61, color: "#CC1414"),(teamID: 96, color: "#1166AB"),(teamID: 99, color: "#461D7C"),(teamID: 344, color: "#762123"),(teamID: 142, color: "#1A1717"),(teamID: 145, color: "#0C457D"),(teamID: 2579, color: "#730B14"),(teamID: 2633, color: "#F78C19"),(teamID: 245, color: "#4F0808"),(teamID: 238, color: "#1A1717"),(teamID: 236, color: "#0C4A7D"),(teamID: 2193, color: "#002C64"),(teamID: 231, color: "#393995"),(teamID: 2382, color: "#E55F17"),(teamID: 2535, color: "#0A3866"),(teamID: 2643, color: "#8AB6E5"),(teamID: 2430, color: "#0A3866"),(teamID: 2678, color: "#E52E3A"),(teamID: 2717, color: "#592C88"),(teamID: 2747, color: "#74603F"),(teamID: 2000, color: "#502D7F"),(teamID: 2110, color: "#4F2683"),(teamID: 2277, color: "#0F3999"),(teamID: 2916, color: "#CB333B"),(teamID: 2320, color: "#E31837"),(teamID: 2377, color: "#0E3887"),(teamID: 2443, color: "#0A2C4A"),(teamID: 2447, color: "#B81237"),(teamID: 2466, color: "#492F92"),(teamID: 2534, color: "#E5841C"),(teamID: 2545, color: "#0A694B"),(teamID: 2617, color: "#61279E"),(teamID: 357, color: "#146FC4"),(teamID: 2010, color: "#700B26"),(teamID: 2011, color: "#1A1717"),(teamID: 2016, color: "#CF9415"),(teamID: 2029, color: "#1A1717"),(teamID: 2755, color: "#1A1717"),(teamID: 2296, color: "#0A3261"),(teamID: 2400, color: "#084F2A"),(teamID: 2504, color: "#9B26B6"),(teamID: 2582, color: "#67B1E5"),(teamID: 2640, color: "#75263D"),(teamID: 2172, color: "#990F39"),(teamID: 2870, color: "#19109E"),(teamID: 2449, color: "#E5BC17"),(teamID: 2437, color: "#1A1717"),(teamID: 198, color: "#071F42"),(teamID: 233, color: "#960A0A"),(teamID: 2571, color: "#115FA8"),(teamID: 2710, color: "#663399"),(teamID: 2026, color: "#1A1717"),(teamID: 2032, color: "#BA1623"),(teamID: 324, color: "#0C7478"),(teamID: 290, color: "#091C5A"),(teamID: 2247, color: "#1145A6"),(teamID: 2031, color: "#662A2C"),(teamID: 309, color: "#CF202F"),(teamID: 6, color: "#BF1344"),(teamID: 326, color: "#B4985A"),(teamID: 2653, color: "#862633"),(teamID: 2433, color: "#660A0A"),(teamID: 250, color: "#146DC7"),(teamID: 252, color: "#09325C"),(teamID: 2250, color: "#0A2F66"),(teamID: 2351, color: "#8F0E33"),(teamID: 279, color: "#E5711E"),(teamID: 2492, color: "#0B4173"),(teamID: 2501, color: "#25205E"),(teamID: 2608, color: "#D91636"),(teamID: 301, color: "#071F42"),(teamID: 2539, color: "#E6AB2C"),(teamID: 2541, color: "#910F34"),(teamID: 2130, color: "#0B6E53"),(teamID: 2934, color: "#103EA1"),(teamID: 2253, color: "#5F259F"),(teamID: 166, color: "#800D0D"),(teamID: 2547, color: "#CE153F"),(teamID: 140, color: "#148AC9"),(teamID: 292, color: "#0B7041"),(teamID: 3084, color: "#06402F")]
}
