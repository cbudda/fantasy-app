//
//  CustomTabBarController.swift
//  Fantasy App
//
//  Created by Chad on 7/21/18.
//  Copyright © 2018 Chad. All rights reserved.
//

import UIKit

class CustomTabBarController: UITabBarController {
    
    let amazonBlue = UIColor(red: 20/255, green: 110/255, blue: 180/255, alpha: 1.0)
    
    let tabsContainerView: UIView = {
        let containerView = UIView()
        containerView.backgroundColor = bluePrimary
        return containerView
    }()
    
    let tabsLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 15, weight: .bold)
        label.textColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        
        //setup our custom view controllers
        let leaguesLayout = UICollectionViewFlowLayout()
        let createLayout = UICollectionViewFlowLayout()
        let joinLayout = UICollectionViewFlowLayout()
        let settingsLayout = UICollectionViewFlowLayout()
        UITabBar.appearance().tintColor = UIColor(red: 66/255, green: 120/255, blue: 207/255, alpha: 1)
        UITabBar.appearance().backgroundImage = UIImage.colorForNavBar(color: .white)
        UITabBar.appearance().shadowImage = UIImage.colorForNavBar(color: lightGray)
        self.tabBar.isTranslucent = false
        self.tabBar.barTintColor = .white
        
        let initialViewController = MyLeaguesVC(collectionViewLayout: leaguesLayout)
        let navigationController = UINavigationController(rootViewController: initialViewController)
        navigationController.tabBarItem.image = UIImage(named: "home")?.withRenderingMode(.alwaysOriginal)
        navigationController.tabBarItem.selectedImage = UIImage(named: "homeIcon")?.withRenderingMode(.alwaysOriginal)
        
        let createController = CreateLeagueVC(collectionViewLayout: createLayout)
//        let createController = LoginController(collectionViewLayout: createLayout)
        let createLeagueController = UINavigationController(rootViewController: createController)
        createLeagueController.tabBarItem.image = UIImage(named: "createEmpty")?.withRenderingMode(.alwaysOriginal)
        createLeagueController.tabBarItem.selectedImage = UIImage(named: "createIconBlue")?.withRenderingMode(.alwaysOriginal)
        
        let secondController = JoinLeagueVC(collectionViewLayout: joinLayout)
//        let secondController = SignupController(collectionViewLayout: joinLayout)
        let leaguesController = UINavigationController(rootViewController: secondController)
        leaguesController.tabBarItem.image = UIImage(named: "joinEmpty")?.withRenderingMode(.alwaysOriginal)
        leaguesController.tabBarItem.selectedImage = UIImage(named: "joinIcon")?.withRenderingMode(.alwaysOriginal)
        
        let thirdController = MenuVC(collectionViewLayout: settingsLayout)
        let settingsController = UINavigationController(rootViewController: thirdController)
        settingsController.tabBarItem.image = UIImage(named: "menuEmpty")?.withRenderingMode(.alwaysOriginal)
        settingsController.tabBarItem.selectedImage = UIImage(named: "menu")?.withRenderingMode(.alwaysOriginal)
        
        viewControllers = [navigationController, createLeagueController, leaguesController, settingsController]
        
        for controller in viewControllers! {
            controller.tabBarItem.imageInsets = UIEdgeInsetsMake(5.5, 0, -5.5, 0)
            controller.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: 5.5)
        }
    }
    
    var prevIndex: CGFloat = 0
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem!) {
        let arr = (self.tabBar.items as! [UITabBarItem])
        let index: CGFloat = CGFloat(arr.index(of: item)!)
        print(index)
        
        let tabSize = tabBar.frame.size.width / CGFloat((viewControllers?.count)!)
        let tabBarStart = tabBar.frame.origin.x

        let targetX = tabBarStart + (tabSize * index) + (tabSize / 2)
        
        if prevIndex != index {
            prevIndex = index
            showTabName(targetX: targetX, index: index)
        }
    }
    
    func showTabName(targetX: CGFloat, index: CGFloat) {

        let viewNames = ["My Leagues", "Create League", "Join League", "Settings"]
        let window = UIApplication.shared.keyWindow!
        
        tabsContainerView.layer.removeAllAnimations()
        tabsLabel.layer.removeAllAnimations()
        
        window.addSubview(tabsContainerView)
        window.addSubview(tabsLabel)
        
        tabsLabel.text = viewNames[Int(index)]
        tabsLabel.alpha = 0
        
        let padding: CGFloat = 6
        let width = tabsLabel.intrinsicContentSize.width + padding * 2
        let leftPadding = 6 - (targetX - width/2)
        var centerX = targetX
        
        if leftPadding > 0 {
            centerX = centerX + leftPadding
        } else {
            centerX = targetX
        }
        
        tabsLabel.topAnchor.constraint(equalTo: tabsContainerView.topAnchor, constant: 2.25).isActive = true
        tabsLabel.centerXAnchor.constraint(equalTo: tabsContainerView.centerXAnchor).isActive = true
        
        tabsContainerView.frame = CGRect(x: centerX - 2.5, y: window.frame.height - 103, width: 5, height: 5)
        tabsContainerView.layer.cornerRadius = tabsContainerView.frame.height / 2
        tabsContainerView.alpha = 0
        
        print(targetX - width/2)
        
        fadeInBackground(targetX: centerX, width: width)
        fadeOutBackground(targetX: centerX)
    }
    
    fileprivate func fadeInBackground(targetX: CGFloat, width: CGFloat) {
        UIView.animate(withDuration: 0.1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.tabsContainerView.frame = CGRect(x: targetX - 11.25, y: UIApplication.shared.keyWindow!.frame.height - 103, width: 22.5, height: 22.5)
            self.tabsContainerView.layer.cornerRadius = self.tabsContainerView.frame.height / 2
            self.tabsContainerView.alpha = 1
        }) { (finished: Bool) in
            self.fadeInText()
        }
        
        UIView.animate(withDuration: 0.25, delay: 0.05, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.tabsContainerView.frame = CGRect(x: targetX - width/2, y: UIApplication.shared.keyWindow!.frame.height - 103, width: width, height: 22.5)
        })
    }
    
    fileprivate func fadeOutBackground(targetX: CGFloat) {
        UIView.animate(withDuration: 0.25, delay: 1.1, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.tabsContainerView.frame = CGRect(x: targetX - 11.25, y: UIApplication.shared.keyWindow!.frame.height - 103, width: 22.5, height: 22.5)
        })
        
        UIView.animate(withDuration: 0.15, delay: 1.2, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.tabsContainerView.frame = CGRect(x: targetX - 2.5, y: UIApplication.shared.keyWindow!.frame.height - 103, width: 5, height: 5)
            self.tabsContainerView.layer.cornerRadius = self.tabsContainerView.frame.height / 2
            self.tabsContainerView.alpha = 0
        })
    }
    
    fileprivate func fadeInText() {
        UIView.animate(withDuration: 0.3, delay: 0.1, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.tabsLabel.alpha = 1
        }) { (finished: Bool) in
            self.fadeOutText()
        }
    }
    
    fileprivate func fadeOutText() {
        UIView.animate(withDuration: 0.3, delay: 0.4, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.tabsLabel.alpha = 0
        })
    }
}

extension CustomTabBarController: UITabBarControllerDelegate  {
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        guard let tabViewControllers = tabBarController.viewControllers, let toIndex = tabViewControllers.index(of: viewController) else {
            return false
        }
        animateToTab(toIndex: toIndex)
        return true
    }
    
    func animateToTab(toIndex: Int) {
        guard let tabViewControllers = viewControllers,
            let selectedVC = selectedViewController else { return }
        
        guard let fromView = selectedVC.view,
            let toView = tabViewControllers[toIndex].view,
            let fromIndex = tabViewControllers.index(of: selectedVC),
            fromIndex != toIndex else { return }
        
        // Add the toView to the tab bar view
        fromView.superview?.addSubview(toView)
        toView.alpha = 0.15
        fromView.alpha = 0.8
        // Position toView off screen (to the left/right of fromView)
        let screenWidth : CGFloat = 100 //UIScreen.main.bounds.size.width
        let scrollRight = toIndex > fromIndex
        let offset = (scrollRight ? screenWidth : -screenWidth)
        toView.center = CGPoint(x: fromView.center.x + offset, y: toView.center.y)
        
        // Disable interaction during animation
        view.isUserInteractionEnabled = false
        
        UIView.animate(withDuration: 0.2,
                       delay: 0.0,
                       usingSpringWithDamping: 1,
                       initialSpringVelocity: 0,
                       options: .curveEaseOut,
                       animations: {
                        // Fade out the Views
                        fromView.alpha = 0
                        toView.alpha = 1
                        
                        // Slide the views by -offset
                        fromView.center = CGPoint(x: fromView.center.x - offset, y: fromView.center.y)
                        toView.center = CGPoint(x: toView.center.x - offset, y: toView.center.y)
                        
        }, completion: { finished in
            // Remove the old view from the tabbar view.
            fromView.removeFromSuperview()
            self.selectedIndex = toIndex
            self.view.isUserInteractionEnabled = true
        })
    }
}
